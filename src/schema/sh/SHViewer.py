# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
GTK Viewer for SHPipe.

(c)Daniel Ramage 2005
"""

import gtk
import pango

from decorators import *

import Emulator
from TerminalDisplay import TerminalDisplay
from sh import SHPipe

from ui.gtk.SegmentedTextView import Segment

import Runtime.Viewers

class SHViewer(TerminalDisplay):
	""" Embeds a TerminalDisplay in a Segment of a SegmentedTextView widget. """

	@takes("SHViewer", tuple, SHPipe)
	def __init__(self, context, pipe):
		TerminalDisplay.__init__(self)
		
		# get the ui 
		self.segment, self.indicator = context
		self.indicator.setImage(gtk.STOCK_CLOSE)
		
		self.pipe = pipe
		self.emulator = Emulator.XTermEmulator(self)

		# intercept key presses in the segment
		self.segment.connect("key-press-event", self.key_press_event)

		# connect incoming pipe to emulator
		self.pipe.onIncoming = self.emulator.interpret
		self.pipe.onClosed = lambda: self.indicator.setImage(gtk.STOCK_EXECUTE)
		
		self._needsCR = False
		self._tags = {}		# attribute tuple -> gtk.TextTag
		self._tag = None	# current tag
		
		# line start offsets relative to segment start
		self._lines = [0]

		# the 1-based index of the last "real" line
		self.lastRealLine = 1
		
		# default attribute
		self.tdISetAttributes(tuple())

	##
	## Line utility functions
	##
	## All are 0-indexed.
	##
	
	def getLineIter(self, line):
		"""
		Returns a gtk.TextIter for the start of the given line.
		"""
		assert line >= 0
		
		while line >= len(self._lines):
			self.segment.append("\n")
			self._lines.append(len(self.segment))

		return self.segment.buffer.get_iter_at_offset(
			self.segment.start.get_offset() + self._lines[line])

	def getLineLength(self, line):
		"""
		Returns the length of the given line.
		"""
		assert line >= 0

		if line >= len(self._lines):
			return 0
		
		if line+1 == len(self._lines):
			end = len(self.segment)
		else:
			end = self._lines[line+1]-1
		
		return end - self._lines[line]
	
	def insertLine(self, line):
		"""
		Inserts a line before the given line.
		"""
		assert line >= 0
		
		pos = self.getLineIter(line)
		self.segment.insert("\n", pos)
		
		# update all following line offsets by +1
		for i in range(line, len(self._lines)):
			self._lines[i] += 1
			
		# insert a new line index for the new line
		self._lines.insert(line, self._lines[line]-1)
	
	def deleteLine(self, line):
		"""
		Deletes the given line.
		"""
		assert line >= 0
		
		if line >= len(self._lines): return
			
		start = self.getLineIter(line)
		end = self.getLineIter(line)

		end.forward_chars(self.getLineLength(line))

		if end.get_offset() != self.segment.end.get_offset():
			end.forward_chars(1)
		elif start.get_offset() != self.segment.start.get_offset():
			start.backward_chars(1)
		size = end.get_offset() - start.get_offset()
		self.segment.buffer.delete(start, end)
		
		# remove the line index
		del self._lines[line]
		
		# update all following line offsets by -size
		for i in range(line, len(self._lines)):
			self._lines[i] -= size

	def insertInLine(self, line, char, text, overwrite=False):
		"""
		Inserts the given text at line,char in the given line.
		
		line and char should both be 0-indexed.
		"""
		assert line >= 0
		assert char >= 0
		
		textStart = char
		textEnd = char+len(text)
		
		if overwrite:
			self.deleteInLine(line, char, char+len(text))
		
		if char > self.getLineLength(line):
			padlength = char - self.getLineLength(line)
			text = (" "*padlength)+text
			char = char - padlength
		
		pos = self.getLineIter(line)
		pos.forward_chars(char)
		self.segment.insert(text, pos)
		
		# update all following line offsets by length
		for i in range(line+1, len(self._lines)):
			self._lines[i] += len(text)
		
		# apply tag
		start = self.getLineIter(line)
		start.forward_chars(textStart)
		end = self.getLineIter(line)
		end.forward_chars(textEnd)
		if self._tag is not None:
			self.segment.buffer.apply_tag(self._tag,start,end)
	
	def deleteInLine(self, line, start, end):
		"""
		Deletes a range of characters in the give line.
		"""
		assert line >= 0
		assert start >= 0 and end >= start
		
		length = self.getLineLength(line)
		startIter = self.getLineIter(line)
		startIter.forward_chars(min(start,length))
		endIter = self.getLineIter(line)
		endIter.forward_chars(min(end,length))
		deleteLength = endIter.get_offset() - startIter.get_offset()
		self.segment.delete(startIter, endIter)
		
		# update all following lines offsets by -deleteLength
		for i in range(line+1, len(self._lines)):
			self._lines[i] -= deleteLength
		
	##
	## Event handlers
	##

	def key_press_event(self, segment, event):
		""" Feed key presses to the pipe's stdin. """
		self.pipe.send(event.string)
		self.segment.stop_emission("key-press-event")

	##
	## TerminalDisplay implementation
	##

	# emulator tag -> gtk.TextTag attributes
	TAGS = {
	  Emulator.ATTR_BOLD:		("weight",pango.WEIGHT_BOLD),
	  Emulator.ATTR_UNDERLINE:	("underline",pango.UNDERLINE_SINGLE),
	  Emulator.ATTR_FG_BLACK:	("foreground","black"),
	  Emulator.ATTR_BG_BLACK:	("background","black"),
	  Emulator.ATTR_FG_RED:		("foreground","red"),
	  Emulator.ATTR_BG_RED:		("background","red"),
	  Emulator.ATTR_FG_GREEN:	("foreground","green"),
	  Emulator.ATTR_BG_GREEN:	("background","green"),
	  Emulator.ATTR_FG_YELLOW:	("foreground","orange"),
	  Emulator.ATTR_BG_YELLOW:	("background","orange"),
	  Emulator.ATTR_FG_BLUE:		("foreground","blue"),
	  Emulator.ATTR_BG_BLUE:		("background","blue"),
	  Emulator.ATTR_FG_MAGENTA:	("foreground","magenta"),
	  Emulator.ATTR_BG_MAGENTA:	("background","magenta"),
	  Emulator.ATTR_FG_CYAN:		("foreground","cyan"),
	  Emulator.ATTR_BG_CYAN:		("background","cyan"),
	  Emulator.ATTR_FG_WHITE:	("foreground","white"),
	  Emulator.ATTR_BG_WHITE:	("background","white"),
	  Emulator.ATTR_FG_DEFAULT:	("foreground-set", False),
	  Emulator.ATTR_BG_DEFAULT:	("background-set", False),
	}
			
	def tdISetAttributes(self, attributes):
		if attributes not in self._tags:
			tag = self.segment.buffer.create_tag(wrap_mode=gtk.WRAP_NONE)
			for attribute in attributes:
				if attribute in self.TAGS:
					tag.set_property(*self.TAGS[attribute])
			
			if Emulator.ATTR_INVERSE in attributes:
				if tag.get_property("foreground-set"):
					foreground = tag.get_property("foreground")
				else:
					foreground = "black"
				if tag.get_property("background-set"):
					background = tag.get_property("background")
				else:
					background = "white"
				
				tag.set_property("foreground", background)
				tag.set_property("background", foreground)
			
#			def handler(texttag,widget,event,textiter):
#				if event.type == gtk.gdk.BUTTON_PRESS:
#					print "MOUSE EVENT IN",texttag
#				elif event.type == gtk.gdk.KEY_PRESS:
#					print "KEY EVENT IN", texttag
#			tag.connect("event",handler)
			
			self._tags[attributes] = tag
		self._tag = self._tags[attributes]
	
	def tdIInsertAtCursor(self, bytes, overwrite=False):
		(line,col) = self.tdCursor
	
		if line > self.lastRealLine:
			self.lastRealLine = line 
		
		self.insertInLine(line-1, col-1, bytes, overwrite)
		self.segment.scrollToCursor()

	def tdIDeleteInLine(self, line, start, end):
		if line > self.lastRealLine:
			self.lastRealLine = line 
	
		self.deleteInLine(line-1,start-1,end)

	def tdIInsertLine(self, line):
		if line < self.lastRealLine:
			self.lastRealLine += 1
			self.insertLine(line)

	def tdIDeleteLine(self, line):
		if line < self.lastRealLine:
			self.lastRealLine -= 1
			self.deleteLine(line-1)

	def tdIToHost(self, bytes):
		self.pipe.send(bytes)

	def tdICursorMoved(self):
		self.insertInLine(self.tdCursor[0]-1,self.tdCursor[1]-1,"")
		cursor = self.getLineIter(self.tdCursor[0]-1)
		cursor.forward_chars(self.tdCursor[1]-1)
		self.segment.cursor = cursor
	
	def tdISoundBell(self):
		print unichr(7)
		
	##
	## TerminalDisplayTestHook implementation
	##
	
	def tdTEST_getCursorLine(self):
		return self.cursor.get_line() - self.start.get_line() + 1
	def tdTEST_getCursorCol(self):
		return self.cursor.get_line_offset() + 1
	def tdTEST_getLine(self,line):
		return self.getLineText(line-1)
	def tdTEST_getLineCount(self):
		return self.end.get_line()-self.start.get_line()+1
	def tdTEST_reset(self):
		self.text = ""
		self._lines = [0]


##
## register the viewer with schema.viewers
##

Runtime.Viewers.register("gtk", SHPipe, SHViewer)

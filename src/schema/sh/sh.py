# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
sh schema

(c) Daniel Ramage 2005
"""

from decorators import *
from interface import *

import sys
import os
import os.path
import dircache
import pty
import select

from OPEP.AST import Status
from OPEP.EBNF import EBNFParser
from OPEP.Grammar import *

import Runtime.Parser
import Runtime.Types

##
## sh-magic data structures
##

class Path(list):
	''' A Path represents an executable path (the standard $PATH
		environmental variable, with niceties). '''
	def __init__(self, elements=None):
		''' Initializes this path using the given path elements array.
			If none is provided, use the os module to build it. 	'''
		if elements is None:
			path = os.environ['PATH']
			self.extend([p for p in path.split(':') if len(p)>0])
		else:
			self.extend(elements)

	@takes("Path", str)
	def getExecutable(self, name):
		''' Returns the full path to the executable with the given name. '''
		if name[0]=='/':
			return os.access(name,os.X_OK)
		
		for path in self:
			if name in dircache.listdir(path):
				if os.access(os.path.join(path,name),os.X_OK):
					return os.path.join(path,name)
		else:
			return None
	
	@takes("Path", str)
	def hasExecutable(self, name):
		''' Returns true if an executable with the given name exists
			in the path and we have execute access to it. '''
		return self.getExecutable(name) is not None

	def __str__(self):
		return ':'.join(self)

class Environment(dict):
	'''
	Shell execution environment, with the current directory,
	shell variables, etc.
	'''
	def __init__(self, environment = None):
		if environment is None:
			self.initFromOS()
		else:
			for key,value in environment:
				self[key] = value

	def abspath(self, path):
		'''
		Returns the absolute path to the given path by using
		os.path.normpath(os.path.join(self['cwd'], path))
		'''
		path = os.path.expanduser(path)
		path = os.path.normpath(os.path.join(self['cwd'], path))
		return path

	def initFromOS(self):
		'''
		Initializes this Environment from what we can
		glean using the os module.
		'''
		self['cwd'] = os.getcwd()
		self['path'] = Path()

class PIDSet(object):
	''' Global list of all child PIDs forked by the sh schema.  We need to
		os.waitpid as part of the sh-schema's idle polling in order to prevent
		defunct processess. '''
	@takes("PIDSet")
	def __init__(self):
		self._pids = {}
		
	@takes("PIDSet", int, "SHInvokable", callable)
	def add(self, pid, job, callback=None):
		self._pids[pid] = (job, callback)
	
	@takes("PIDSet", int)
	def __contains__(self, pid):
		return pid in self._pids
	
	@takes("PIDSet")
	def poll(self):
		if len(self._pids) == 0:
			return
		else:
			(pid, status) = os.waitpid(-1, os.WNOHANG)
			
			if pid != 0:
				(job,callback) = self._pids[pid]
				if callback is not None:
					callback(job)
				del self._pids[pid]

class FDSet(object):
	"""	Collection of live operating system file descriptors.
	
		A single global instance of FDSet exists in gsh.py.  Forked
		processes need a way to close ALL open file descriptors so
		we don't hang on pipe reads.
	
		Each FD can be marked with a callback that should be called
		with the pending read contents if they become available. """
	def __init__(self):
		self._fds = []
		self._reads = {}
		self._poll = select.poll()

	def poll(self):
		""" Poll all callback-registered descriptors, dispatching as
			necessary. """
		try:
			for (fd, event) in (self._poll.poll(20)):
				try:
					self._reads[fd](os.read(fd, 2048))

				except OSError:
					self.unregister(fd)
		except select.error:
			pass

	def register(self, fd, read=None):
		""" Add the fd to the set, optionally registering callbacks.
		
			If read is not None, it must be a callable accepting a
			single string argument (waiting contents on the descriptor). """
		self._fds.append(fd)
		if read is not None:
			assert callable(read)
			self._poll.register(fd, 
				select.POLLIN|select.POLLPRI|
				select.POLLERR|select.POLLHUP)
			self._reads[fd] = read

	def unregister(self, fd):
		""" Removes the fd from the set, unregistering the callback
			if necessary.  Does NOT close the descriptor. """
		assert fd in self._fds
		self._fds.remove(fd)
		if fd in self._reads:
			self._poll.unregister(fd)
			del self._reads[fd]

	def close(self, fd):
		""" Closes and removes a fd. """
		if fd not in self._fds:
			return
		
		self.drain(fd)
		self.unregister(fd)
		os.close(fd)

	def drain(self, fd):
		""" Drains all remaining input from the given fd. """
		if fd not in self._reads: return
		
		while True:
			try:
				self._reads[fd](os.read(fd, 2048))
			except OSError:
				break

	def __contains__(self, fd):
		return fd in self._fds

	def close_all(self):
		""" Calls os.close() on all descriptors in the set after
			removing each one. """
		for fd in self._fds:
			self.unregister(fd)
			os.close(fd)


##
## global instances of magic data structures
##

pids = PIDSet()
descriptors = FDSet()

import callback

def poll():
	pids.poll()
	descriptors.poll()
callback.register_idle(poll)


##
## Shell Invocations
##

class SHInvokable(Runtime.Types.Invokable):
	""" An invocation of a unix process.  These are NOT reusable for a 
		second invocation. """
	
	@takes("SHInvokable", str, "SHSchema")
	def __init__(self, name, schema):
		Runtime.Types.Invokable.__init__(self, SHParser(name,schema.context), self.invoke)
		
		self.name = name
		self.environment = schema.env
		
		# unix fd numbers for dup2
		self.stdin = None
		self.stdout = None
		self.stderr = None
	
	@takes("SHInvokable", AST, list)
	def invoke(self, ast, args):
		""" Forks to run the given process. """
		assert "pipe" not in dir(self), \
			"Attempt to re-invoke() an SHInvokable.  SHInvokables may be invoked only once."

		# output return value
		self.pipe = SHPipe(self)
		
		# open a pty pre-fork if any pipe fd is None
		if None in (self.stdin, self.stdout, self.stderr):
			def onIncoming(bytes):
				""" Dispatches bytes to current self.pipe.onIncoming.  We
					cannot set the descriptor read callback to s.p.onIncoming
					directly, because it might not yet have is final value. """
				self.pipe.onIncoming(bytes)
		
			pty_master, pty_slave = pty.openpty()
			descriptors.register(pty_master, read=onIncoming)
			descriptors.register(pty_slave)
		
		pid = os.fork()
		if pid == 0:
			# child process
			try:
				os.chdir(self.environment['cwd'])
				
				# dup the pipes for exec'd process
				os.dup2(self.stdin or pty_slave, 0)
				os.dup2(self.stdout or pty_slave, 1)
				os.dup2(self.stderr or pty_slave, 2)
		
				# close all outstanding file descriptors (including pty_master)
				# so the parent or chained invocations don't hang on open pipes
				descriptors.close_all()
			except Exception, e:
				print e
				sys.exit(-1)
			
			argv = [self.name]
			for arg in args:
				if type(arg) == str: argv.append(arg)
				elif type(arg) == list: argv.extend(arg)
				
			os.execv(self.name, argv)
			## END OF EXECUTION
		else:
			@takes("SHInvokable")
			def onChildTermination(job):
				""" callback for when process ends """
				descriptors.close(self.stdin)
				descriptors.close(self.stdout)
				descriptors.close(self.stderr)
				self.pipe.closed = True
				self.pipe.onClosed()

			pids.add(pid, self, onChildTermination)

			# take stdin,stdout,stderr from PTY master if necessary
			self.stdin = self.stdin or pty_master
			self.stdout = self.stdout or pty_master
			self.stderr = self.stderr or pty_master
			
			# close the slave half of the PTY if we opened one
			if pty_slave: descriptors.close(pty_slave)
			
			# return value is the SHPipe
			return self.pipe

##
## sh value type
##

class SHPipe(object):
	""" Pipe for communication to system. """

	@takes("SHPipe", "SHInvokable")
	def __init__(self, invokable):
		self.invokable = invokable
		self.closed = False
	
	@takes("SHPipe", str)
	def onIncoming(self, bytes):
		""" This method is called with bytes read from a forked SHInvokable. """
	
	@takes("SHPipe")
	def onClosed(self):
		""" This method is called when the process connected to this pipe finishes. """

	@takes("SHPipe", object)
	def send(self, bytes):
		""" Sends bytes to the pipe's stdin. """
		if not self.closed:
			os.write(self.invokable.stdin, bytes)

##
## sh parser
##

@memoized
def DefaultSHParser(context):
	""" Returns the default sh command parser for the given context. """
	grammar = "@cmd := [arg]@sh.generic_argument*"
	return Runtime.Parser.CustomParser(grammar, context)

@memoized
@takes(str, "Context")
def SHParser(cmd, context):
	""" Returns the parser specific for the given command, or the default
		sh parser otherwise. """
	return DefaultSHParser(context)
	

##
## sh builtin functions
##

@takes("SHSchema")
def SHBuiltins(schema):
	""" List of builtin Invokables. """

	env = schema.env
	context = schema.context

	@singleton
	def cd():
		def execute(ast, args):
			if len(args):
				directory = args[0]
			else:
				directory = "~"
		
			path = env.abspath(directory)
			if os.path.exists(path) and os.path.isdir(path):
				env['cwd'] = path
			else:
				raise "Invalid directory '%s'" % path
		
		parser = Runtime.Parser.CustomParser("@cd := [arg]@sh.directory?", context)
		return Runtime.Types.Invokable(parser, execute)
	
	builtins = {}
	for k,v in locals().iteritems():
		if Runtime.Types.IsA(v, Runtime.Types.Invokable):
			builtins[k] = v
	return builtins


def glob(basedir, pattern):
	"""	Returns a list of files matching the given pattern, starting in the
		given basedir. """
	# add a trailing slash if necessary
	basedir = os.path.expanduser(os.path.join(basedir, ''))

	def dirify(f):
		if os.path.isdir(f):
			f = os.path.join(f,'')
		
		if f.startswith(basedir):
			f = f[len(basedir):]
		
		return f

	import glob
	pattern = os.path.join(basedir, os.path.expanduser(pattern))
	oldcwd = os.getcwd()
	os.chdir(basedir)
	files = [dirify(f) for f in glob.glob(pattern)]
	os.chdir(oldcwd)
	return files

##
## schema-exported functions
##

import schema
class SHSchema(schema.Schema):
	"""	sh Schema object. """

	def __init__(self, context):
		schema.Schema.__init__(self, context)
		
		self.env = Environment()

	def UpdateSymbolLibrary(self, library):
		""" Updates the symbol library to include sh-related symbols. """
		from OPEP.Grammar import Rule, SQuoteStringTokenSymbol
	
		# all sh symbols come from one of these underlying symbols
		SHSymbolBase = ["@gci.literal.word", "@gci.value.string"]

		@SemanticRule("@sh.directory", library, *SHSymbolBase)
		def DirectorySymbol():
			def transformAST(ast):
				if ast.status == Status.Reject: return ast
				
				if ast.children[0].value is None:
					ast.addTag(Tag(Status.Reject))
					return ast
				
				path = self.env.abspath(ast.children[0].value)
				if os.path.exists(path) and os.path.isdir(path):
					ast.value = path
					ast.addTag(Tag(Status.Accept))
				else:
					ast.addTag(Tag(Status.Reject, "Directory not found"))
				return ast
			
			def getASTCompleter(ast):
				def completions(pos):
					base = ""
					if len(ast.children): base = ast.children[0].value[:pos]
					for path in glob(self.env['cwd'], base+'*'):
						if os.path.isdir(path):
							yield path
				return Completer(ast.match, completions)
			
		@SemanticRule("@sh.file", library, *SHSymbolBase)
		def FileSymbol():
			def transformAST(ast):
				if ast.status == Status.Reject: return ast
				
				path = self.env.abspath(ast.children[0].value)
				if os.path.exists(path) and os.path.isfile(path):
					ast.value = path
					ast.addTag(Tag(Status.Accept))
				else:
					ast.addTag(Tag(Status.Reject, "File not found"))
				return ast
		
		@SemanticRule("@sh.wildcard", library, "@gci.literal.word")
		def WildcardSymbol():
			def transformAST(ast):
				if ast.status == Status.Reject: return ast
				
				import glob
				pattern = os.path.expanduser(ast.children[0].value)
				basedir = os.getcwd()
				os.chdir(self.env['cwd'])
				ast.value = [self.env.abspath(path) for path in glob.glob(pattern)]
				os.chdir(basedir)
				if len(ast.value) > 0:
					ast.addTag(Tag(Status.Accept))
				else:
					ast.addTag(Tag(Status.Warning,
						"No files or folders match pattern '%s'" % ast.children[0].value))
				return ast
		
		@SemanticRule("@sh.generic_argument", library, *SHSymbolBase)
		def GenericArgument():
			def transformAST(ast):
				if ast.status == Status.Reject: return ast
		
				if ast.children[0].type == "@gci.literal.word" and \
				   ast.children[0].value[0] != '-':
					# if it is not a switch, try treating it like a wildcard
					ast = library['@sh.wildcard'].rules[0].transformAST(ast)
				
				if not ast.value:
					# no files were found or it was a switch, take literal
					ast.value = ast.children[0].value
			
				return ast
			
	@implements(Runtime.Types.ItemProvider.HasItem)
	def HasItem(self, name):
		return name in self.builtins or self.env['path'].hasExecutable(name)
	
	@implements(Runtime.Types.ItemProvider.GetItem)
	def GetItem(self, name):
		assert self.HasItem(name)
		if name in self.builtins:
			return self.builtins[name]
		else:
			return SHInvokable(self.env['path'].getExecutable(name), self)

	@implements(Runtime.Types.ItemProvider.ListItems)
	def ListItems(self, name):
		return self.builtins.keys()

	@property
	def builtins():
		def fget(self):
			if not hasattr(self, '_builtins'):
				self._builtins = SHBuiltins(self)
			return self._builtins

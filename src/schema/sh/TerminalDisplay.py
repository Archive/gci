# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
TerminalDisplay interface and test hooks.

The primitive used by Emulator.

(c)Daniel Ramage 2005
"""

from Emulator import VERBOSE

PURE_VIRTUAL = "Pure virtual function."

class TerminalDisplay(object):
	"""
	A handle to a terminal emulator assigned for a particular job.
	
	May be the Terminal itself in some implementations.
	"""
	def __init__(self):
		self.__cursor = [1,1]
		self.__size = [24,80]
		self.__margin = [1,24]

		# mode flags
		self.__tdOriginMode = False
		self.__tdInsertMode = False
		self.__tdEchoMode = True
		self.__tdApplicationKeyMode = False
		
		# convience pointers to virtual functions
		self.tdSetAttributes = self.tdISetAttributes
		self.tdToHost = self.tdIToHost
		self.tdSoundBell = self.tdISoundBell
		
	##
	##  Virtual functions (I == Interface)
	##
	
	def tdISetAttributes(self, attribute, active=True):
		"""
		Set an attribute active for subsequently inserted text.
		"""
		raise PURE_VIRTUAL

	def tdIInsertAtCursor(self, text, overwrite=False):
		"""
		Insert text at the cursor (potentially overwriting).
		Updates cursor position.
		"""
		raise PURE_VIRTUAL

	def tdIInsertLine(self, line):
		"""
		Insert a new line following the given line.
		"""
		raise PURE_VIRTUAL

	def tdIDeleteInLine(self, line, start, end):
		"""
		Deletes characters in the row, inclusive, shifting following
		chars left.
		"""
		raise PURE_VIRTUAL
	
	def tdIDeleteLine(self, line):
		"""
		Deletes rows in the display, shifting following lines up.
		"""
		raise PURE_VIRTUAL
	
	def tdIToHost(self, bytes):
		"Sends the given bytes to the host (client program)."
		raise PURE_VIRTUAL

	def tdICursorMoved(self):
		"Notification that the cursor position has moved."
		raise PURE_VIRTUAL
	
	def tdISoundBell(self):
		"Called when the application prints the BEL character."
		raise PURE_VIRTUAL
	
	##
	## Utility functions
	##
	
	def tdInsertAtCursor(self, text, overwrite=None):
		"""
		Inserts at the current cursor position, and updates the
		tdCursor accordingly.
		"""
		if overwrite is None:
			self.tdIInsertAtCursor(text, overwrite=not self.tdInsertMode)
		else:
			self.tdIInsertAtCursor(text, overwrite=overwrite)
		self.tdCursor = (self.tdCursor[0], self.tdCursor[1]+len(text))

	def tdDeleteInLine(self, line, start, end):
		if VERBOSE: print "DELETE IN LINE: line %s, start %s, end %s" \
			%(line, start, end)
		line = max(0,line)
		start = max(1,start)
		end = max(start,end)
		self.tdIDeleteInLine(line,start,end)

	def tdDeleteLine(self, line):
		if VERBOSE: print "DELETE LINE: line %s" % line
		self.tdIDeleteLine(max(1,line))

	def tdInsertLine(self, line):
		if VERBOSE: print "INSERTING LINE: line %s" % line
		self.tdIInsertLine(max(0,line))

	def tdMakeRoomForChars(self, length):
		"""
		Removes up to length characters from the end of the current line
		if we do not have enough room for them all.
		"""
		self.tdDeleteInLine(self.tdCursor[0], 
			max(self.tdCursor[1],self.tdSize[1]-length),
			self.tdSize[1])

	def tdMakeRoomForLines(self, lines):
		"""
		Removes the last 'lines' lines from the end of the screen.
		"""
		for i in range(min(lines,self.tdMargin[1]-self.tdMargin[0]+1)):
			self.tdDeleteLine(self.tdMargin[1]-i)

	def tdMoveCursor(self,
		rowOffset=0, colOffset=0,
		rowAbsolute=None, colAbsolute=None,
		margin=True):
		"""
		Moves the current tdCursor position by the specified offset
		relative to the specified absolute position.  If the absolute
		position is None, uses the current position.
		
		If margin is True, the cursor will not move beyond the extent
		of the scrolling region.
		
		Coordinate indexing is affected by the state of tdOriginMode.
		"""
		row = rowAbsolute
		if row is None: row = self.tdCursor[0]
		row += self.tdOrigin[0]-1
		row += rowOffset
		
		col = colAbsolute
		if col is None: col = self.tdCursor[1]
		col += self.tdOrigin[1]-1
		col += colOffset
		
		if margin:
			row = max(self.tdMargin[0],min(row,self.tdMargin[1]))
			col = max(1,min(col,self.tdSize[1]))
		
		self.tdCursor = (row, col)
	
	def tdMoveCursorRelative(self, rowOffset, colOffset, margin=True):
		"""
		Moves the cursor relative to the current position.
		
		If margin is True, the cursor will not move beyond the extent
		of the scrolling region.
		"""
		self.tdMoveCursor(
			rowOffset=rowOffset, colOffset=colOffset,
			margin=margin)
		
	def tdMoveCursorAbsolute(self, rowAbsolute, colAbsolute, margin=True):
		"""
		Moves the cursor to the absolute position.
		
		If margin is True, the cursor will not move beyond the extent
		of the scrolling region.
		"""
		self.tdMoveCursor(
			rowAbsolute=rowAbsolute, colAbsolute=colAbsolute,
			margin=margin)

	def tdIsCursorInScrollingRegion(self):
		"""
		Returns true when the cursor is in the scrolling region.
		"""
		return self.tdCursor[0] >= self.tdMargin[0] and \
		       self.tdCursor[0] <= self.tdMargin[1]
		
	##
	## Properties
	##
	
	def tdCursor():
		doc="[row,column] tuple defining the current cursor position."
		def fget(self):
			return tuple(self.__cursor)
		def fset(self, position):
			self.__cursor = [position[0], position[1]]
			self.tdICursorMoved()
		return locals()
	tdCursor = property(**tdCursor())

	def tdCursorAdjusted():
		doc="[row,column] tuple defining the current cursor position, " +\
			"adjusted for the current margin."
		def fget(self):
			if self.tdOriginMode:
				return (self.tdCursor[0]+self.tdMargin[0]-1, \
					self.tdCursor[1])
			else:
				return self.tdCursor
		return locals()
	tdCursorAdjusted = property(**tdCursorAdjusted())
	
	def tdSize():
		doc="[rows,columns] tuple defining the size of full window."
		def fget(self):
			return tuple(self.__size)
		def fset(self, size):
			if (size[1] > self.__size[1]):
				for line in range(self.__size[0]):
					self.tdDeleteInLine(line+1,
						self.__size[1],size[1])
			self.__size = (size[0],size[1])
		return locals()
	tdSize = property(**tdSize())

	def tdMargin():
		doc="[top_line,bottom_line] inclusive defining the scrollable area."
		def fget(self):
			return tuple(self.__margin)
#			if self.tdOriginMode:
#				return tuple(self.__margin)
#			else:
#				return (1,self.tdSize[0])
		def fset(self, margin):
			oldcursor = self.tdCursorAdjusted
			self.__margin = list(margin)
			self.tdMoveCursor(rowAbsolute=oldcursor[0], \
				colAbsolute=oldcursor[1])
		return locals()
	tdMargin = property(**tdMargin())
	
	def tdOrigin():
		doc="[line,column] of the origin."
		def fget(self):
			if self.tdOriginMode:
				return(self.__margin[0],1)
			else:
				return(1,1)
		return locals()
	tdOrigin = property(**tdOrigin())

	##
	## mode flags
	##

	def tdOriginMode():
		doc="True/False for whether we are in origin mode (see DECOM)."
		def fget(self):
			return self.__tdOriginMode
		def fset(self, value):
			self.__tdOriginMode = value
			self.tdCursor = self.tdOrigin
		return locals()
	tdOriginMode = property(**tdOriginMode())
	
	def tdInsertMode():
		doc="True/False for whether new characters are default insert/overwrite."
		def fget(self):
			return self.__tdInsertMode
		def fset(self, value):
			self.__tdInsertMode = value
		return locals()
	tdInsertMode = property(**tdInsertMode())

	def tdEchoMode():
		doc="True when typed characters should be echo'd by default."
		def fget(self):
			return self.__tdEchoMode
		def fset(self, value):
			self.__tdEchoMode = value
		return locals()
	tdEchoMode = property(**tdEchoMode())
	
	def tdApplicationKeyMode():
		doc="True when keypresses should send application control functions, versus ANSI keycodes."
		def fget(self):
			return self.__tdApplicationKeyMode
		def fset(self, value):
			self.__tdApplicationKeyMode = value
		return locals()
	tdApplicationKeyMode = property(**tdApplicationKeyMode())

class TerminalDisplayTestHook(object):
	"""
	Test hook interface for a TerminalDisplay in order to implement
	the automated unit testing in this class.
	"""
	
	def tdTEST_getCursorLine(self):
		""" Returns the line number containing the cursor. """
		raise PURE_VIRTUAL
	
	def tdTEST_getCursorCol(self):
		""" Returns the column number of the current cursor. """
		raise PURE_VIRTUAL

	def tdTEST_getLine(self, line):
		""" Returns the line of text at the given line. """
		raise PURE_VIRTUAL
	
	def tdTEST_getLineCount(self):
		""" Returns the number of lines in there. """
		raise PURE_VIRTUAL
	
	def tdTEST_reset(self):
		""" Clears the buffer and resets the cursor to 1,1 """
		raise PURE_VIRTUAL

class BufferTerminalDisplay(TerminalDisplay,TerminalDisplayTestHook):
	"""
	A default reference implementation of TerminalDisplay that applies
	all commands to an offscreen buffer.
	"""
	def __init__(self):
		TerminalDisplay.__init__(self)
		self.tdTEST_reset()
	
	def tdISetAttributes(self, attribute, active=True):
		pass

	def tdIInsertAtCursor(self, text, overwrite=False):
		if overwrite:
			self.lines[self.cursor_line] = \
				self.lines[self.cursor_line][:self.cursor_char] + \
				text + \
				self.lines[self.cursor_line][self.cursor_char+len(text):]
		else:
			self.lines[self.cursor_line] = \
				self.lines[self.cursor_line][:self.cursor_char] + \
				text + \
				self.lines[self.cursor_line][self.cursor_char:]

	def tdIInsertLine(self, line):
		self.lines.insert(line, "")
		self.tdICursorMoved()

	def tdIDeleteInLine(self, line, start, end):
		self.lines[line-1] = \
			self.lines[line-1][:start-1] + self.lines[line-1][end:]
		self.tdICursorMoved()
	
	def tdIDeleteLine(self, line):
		if (len(self.lines) > line-1):
			self.lines.pop(line-1)
		self.tdICursorMoved()

	def tdICursorMoved(self):
		self.cursor_line = self.tdCursor[0]-1
		self.cursor_char = self.tdCursor[1]-1
		
		while self.cursor_line >= len(self.lines):
			self.lines.append("")
		while self.cursor_char > len(self.lines[self.cursor_line]):
			self.lines[self.cursor_line] += " "
	
	def tdISoundBell(self):
		print chr(7)
	
	def tdTEST_getCursorLine(self):
		return self.cursor_line+1
	
	def tdTEST_getCursorCol(self):
		return self.cursor_char+1

	def tdTEST_getLine(self, line):
		return self.lines[line-1]

	def tdTEST_getLineCount(self):
		return len(self.lines)

	def tdTEST_reset(self):
		self.lines = [""]
		self.cursor_line = 0
		self.cursor_char = 0


if __debug__:
	import unittest

	##
	## UNITTEST SETUP
	##

	terminal = None
	def CreateGTKTerminalDisplay():
		global terminal
		if terminal is None:
			import ui.gtk.ui
			terminal = ui.gtk.ui.Window().terminals[0].ITerminal_jobStarted("job")
		terminal.tdTEST_reset()
		return terminal
		
	TerminalDisplayTestSubject = CreateGTKTerminalDisplay
		
	##
	## EXERCISE SETUP
	##
	
	InsertLine = "tdInsertLine"
	DeleteLine = "tdDeleteLine"
	MoveCursorRelative = "tdMoveCursorRelative"
	MoveCursorAbsolute = "tdMoveCursorAbsolute"
	InsertAtCursor = "tdInsertAtCursor"
	DeleteInLine = "tdDeleteInLine"
	MakeRoomForChars = "tdMakeRoomForChars"
	MakeRoomForLines = "tdMakeRoomForLines"
	
	class TestTerminalDisplay(unittest.TestCase):
		"""
		Tests the display in TerminalDisplayTestSubject by running
		all exercises on it and on a BufferTerminalDisplay as a control.
		Complains if the TerminalDisplayTestSubject gets out of sync.
		"""
		def setUp(self):
			self.control = BufferTerminalDisplay()
			self.control.tdCursor = (1,1)
			self.subject = TerminalDisplayTestSubject()
			self.subject.tdCursor = (1,1)
			
			# assert isinstance(self.subject, TerminalDisplay)
			# assert isinstance(self.subject, TerminalDisplayTestHook)
				
		def dump(self, hook=None):
			"""
			Prints the contents of the given doowop.
			"""
			if hook is None:
				hook = self.control
			
			rv = []
			for line in range(hook.tdTEST_getLineCount()):
				rv.append("%s:'%s'" % (line+1,hook.tdTEST_getLine(line+1)))
			return "\n".join(rv)

		
		def assertSync(self):
			"""
			Asserts that the control and subject TerminalDisplayTestHooks
			contain the same contents.
			"""
			def assertEqual(a,b,message):
				if a != b:
					raise AssertionError("%s\n  Expected %s, got %s\nCONTROL CONTENTS\n%s\n\nSUBJECT CONTENTS\n%s" % (message, repr(a), repr(b), self.dump(self.control), self.dump(self.subject)))
			
			assertEqual(
				self.control.tdTEST_getLineCount(),
				self.subject.tdTEST_getLineCount(),
				"Wrong number of lines")

			assertEqual(
				self.control.tdTEST_getCursorLine(),
				self.subject.tdTEST_getCursorLine(),
				"Wrong cursor line")
			
			assertEqual(
				self.control.tdTEST_getCursorCol(),
				self.subject.tdTEST_getCursorCol(),
				"Wrong cursor column")
			
			for line in range(self.subject.tdTEST_getLineCount()):
				assertEqual(
					self.control.tdTEST_getLine(line+1),
					self.subject.tdTEST_getLine(line+1),
					"Line %s doesn't match" % (line+1))
		
		def assertContents(self, contents):
			"""
			Asserts that the contents of the control TerminalDisplay
			match the given string.
			"""
			if type(contents) == str:
				contents = contents.split("\n")
				
			self.assertEqual(
				len(contents),
				self.control.tdTEST_getLineCount())
			
			for i in range(len(contents)):
				self.assertEqual(
					self.control.tdTEST_getLine(i+1).rstrip(),
					contents[i].rstrip())
		
		def command(self, *command):
			"""
			Runs a command on both self.control and self.subject,
			assertSync'ing after.
			"""
			getattr(self.control,command[0])(*command[1:])
			getattr(self.subject,command[0])(*command[1:])
			self.assertSync()
		
		def testInsertAtCursor(self):
			self.command(InsertAtCursor, "hi there")
			self.command(MoveCursorRelative, 1, 0)
			self.command(InsertAtCursor, "yo")
			self.command(MoveCursorAbsolute, 2, 1)
			self.command(InsertAtCursor, "meep!", True)
			self.command(MoveCursorAbsolute, 5, 5)
			self.command(InsertAtCursor, "eep", True)
			self.command(MoveCursorRelative, 0, -3)
			self.command(InsertAtCursor, "b", False)
			self.command(MoveCursorRelative, -1, -1)
			self.command(InsertAtCursor, "beep", True)
			self.assertContents([
				"hi there",
				"meep!   yo",
				"",
				"    beep",
				"    beep"])

		def testInsertLine(self):
			self.command(InsertAtCursor, "IL: line 1")
			self.command(InsertLine, 0)
			self.command(InsertLine, 0)
			self.command(InsertAtCursor, "IL: line 2")
			self.command(InsertLine, 1)
			self.command(InsertLine, 0)
			self.assertContents([
				"",
				"          IL: line 2",
				"",
				"",
				"IL: line 1"])

		def testDeleteLine(self):
			self.command(InsertAtCursor, "DL: line 1")
			self.command(MoveCursorRelative, 1, -6)
			self.command(InsertAtCursor, "DL: line 2")
			self.command(DeleteLine, 2)
			self.command(DeleteLine, 2)
			self.assertContents([
				"DL: line 1",
				"    "])
		
		def testDeleteInLine(self):
			self.command(InsertAtCursor, "DELETE IN LINE: line 1")
			self.command(MoveCursorAbsolute, 2, 1)
			self.command(InsertAtCursor, "DELETE IN LINE: line 2")
			self.command(DeleteInLine, 1, 1, 6)
			self.command(DeleteInLine, 2, 2, 2)
			self.assertContents([
				" IN LINE: line 1",
				"DLETE IN LINE: line 2"])

	if __name__ == '__main__':
		unittest.main()

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Terminal emulation package: implements most of xterm, vt102, vt52.

(c)Daniel Ramage 2005
"""

import string
import re

VERBOSE = False

from TerminalDisplay import TerminalDisplay

##
## Character attributes supported by the emulator
##

ATTR_BOLD = 0
ATTR_UNDERLINE = 1
ATTR_BLINK = 2
ATTR_INVERSE = 3
ATTR_FG_BLACK = 30
ATTR_BG_BLACK = 40
ATTR_FG_RED = 31
ATTR_BG_RED = 41
ATTR_FG_GREEN = 32
ATTR_BG_GREEN = 42
ATTR_FG_YELLOW = 33
ATTR_BG_YELLOW = 43
ATTR_FG_BLUE = 34
ATTR_BG_BLUE = 44
ATTR_FG_MAGENTA = 35
ATTR_BG_MAGENTA = 45
ATTR_FG_CYAN = 36
ATTR_BG_CYAN = 46
ATTR_FG_WHITE = 37
ATTR_BG_WHITE = 47
ATTR_FG_DEFAULT = 39
ATTR_BG_DEFAULT = 49

def IS_ATTR_FG(attribute):
	""" Returns true when the given attribute is for a foreground color """
	return attribute >= 30 and attribute < 40
def IS_ATTR_BG(attribute):
	""" Returns true when the given attribute is for a background color """
	return attribute >= 40 and attribute < 50

##
## Special characters
##

CHR_ESC = chr(27)

##
## Exceptions
##

class UnknownControlSequence(Exception):
	def __init__(self, code=None, subfunction=None, args=None):
		self.code = code
		self.subfunction = subfunction
		self.args = args
	def __str__(self):
		return "Unknown control sequence: code %s; subfunction %s; args %s" % \
			(repr(self.code), repr(self.subfunction), repr(self.args))

##
## CommandFormats
##

class CommandFormat(object):
	"""
	Abstract base class of all command formats supported in the package.
	
	Defines four return codes for recognize():
	  REJECT  - Does not accept the input string
	  PARTIAL - Recognizes an incomplete input command
	  ACCEPT  - Recognizes a complete, known input
	  UNKNOWN - Recognizes a complete, but unknown, input
	"""
	REJECT  = 0
	PARTIAL = 1
	ACCEPT  = 2
	UNKNOWN = 3
	
	def recognize(self, bytes):
		"""
		Tests if the given byte sequence begins with a command in
		this CommandFormat.
		
		Returns a (status, length, arguments) tuple, where:
		- status is one of REJECT, PARTIAL, ACCEPT, UNKNOWN
		- length is the number of characters that an ACCEPT or
		  UNKNOWN recognition consumed.
		- arguments is a list of arguments to be passed to
		  interpret for an ACCEPT.
		"""
		raise "PURE VIRTUAL"
		
	def interpret(self, *arguments):
		"""
		Interpret the command represented by the given *arguments.
		These arguments are as returned by an ACCEPTed recognize.
		"""
		raise "PURE VIRTUAL"

class CTRL(CommandFormat):
	"""
	CTRL, CTRL-Letter commands.
	
	CTRL commands are the most basic commands given to a terminal.
	These are non-printable characters in the range 0..26, 28..31, 127.
	
	The CTRL format is special because it can legally be embedded
	within ESC or CSI format expressions.
	"""
	def __init__(self):
		self.calls = {}
	def register(self, code, function):
		self.calls[chr(ord(code)-ord('@'))] = function
	def recognize(self, bytes):
		value = ord(bytes[0])
		if (value>31 or value==27) and value!=127:
			return (CommandFormat.REJECT, 0)
		elif bytes[0] not in self.calls:
			return (CommandFormat.UNKNOWN, 1,
				chr(ord(bytes[0])+ord('@')))
		
		return (CommandFormat.ACCEPT, 1, bytes[0])
	def accepts(self, byte):
		return self.recognize((byte,))[0] == CommandFormat.ACCEPT
	def interpret(self, command):
		if VERBOSE: print "CTRL %s"%(chr(ord(command)+ord('@')))
		self.calls[command]()
			
class ESC(CommandFormat):
	"""
	ESC: ESC-KEY or ESC-KEY-KEY command format.
	
	CTRL format may be embedded within.
	"""
	def __init__(self, ctrlInstance):
		self.calls = {}
		self.ctrlInstance = ctrlInstance
		
		# list of possible prefix bytes of multi-byte sequences
		self.prefix = []
	
	def register(self, code, function):
		self.calls[code] = function
		if len(code) == 2 and code[0] not in self.prefix:
			self.prefix.append(code[0])
	
	def recognize(self, bytes):
		if bytes[0] != CHR_ESC:
			return (CommandFormat.REJECT, 0)
		
		# queue of embedded CTRL commands
		ctrlQueue = []
		
		# the command
		cmd = ''
		
		# ignore <ESC>
		index = 1
		
		while index < len(bytes):
			if bytes[index] == '[':
				# <ESC>[ is reserved for CSI 
				return (CommandFormat.REJECT, 0)
			elif self.ctrlInstance.accepts(bytes[index]):
				# embedded CTRL command
				ctrlQueue.append(bytes[index])
			elif cmd == '' and bytes[index] in self.prefix:
				# take one prefix byte for two-byte sequences
				cmd += bytes[index]
			else:
				# first non-prefix regular byte
				cmd += bytes[index]
				break
			
			index += 1

		# loop fell through
		if index == len(bytes):
			return (CommandFormat.PARTIAL, len(bytes))

		# execute enqueued embedded CTRL commands
		for ctrlCmd in ctrlQueue:
			self.ctrlInstance.interpret(ctrlCmd)
		
		if cmd in self.calls:
			return (CommandFormat.ACCEPT, index+1, cmd)
		else:
			return (CommandFormat.UNKNOWN, index+1, cmd)

	def interpret(self, command):
		if VERBOSE: print "ESC %s"%(command)
		self.calls[command]()

class CSI(CommandFormat):
	"""
	CSI, Control Sequence Introducer: A VT100 command format.
	
	CSI command sequences are introduced by "<ESC>[".  One or more
	numerical parameters (ascii numbers base 10) follows, separated
	by semicolons.  A final alpha-character specifies the command
	and closes the sequence.
	
	The numeric parameter can be either a numeric argument to the
	function, such as how many characters to delete for DCH (CSI code
	'P', delete characters), or a sub-function selector, as for ED (CSI
	code 'J', clear in screen).
	
	Numeric arguments usually have a default value, allowing arguments
	to go unspecified.  Sub-function selectors can be chained as multiple
	arguments to a single invocation.  So <ESC>[0;4m is equivalent to:
	  <ESC>[0m<ESC>[4m
	  
	An obnoctious complication is that CTRL sequences can be legally
	embedded in a CSI.  These commands are to be interpreted immediately
	as they are encountered.
	"""
	def __init__(self, ctrlInstance):
		self.calls = {}
		self.ctrlInstance = ctrlInstance
		
		# list of possible prefix bytes of multi-byte sequences
		self.prefix = []
		
	def register(self, code, function):
		self.calls[code] = function
		if len(code) == 2 and code[0] not in self.prefix:
			self.prefix.append(code[0])

	def recognize(self, bytes):
		if bytes[0] == ord('[')+0x40:
			print "ARRRRG!"
	
		if len(bytes) == 1 and bytes[0] == CHR_ESC:
			return (CommandFormat.PARTIAL, 1)
		elif bytes[0] != CHR_ESC:
			return (CommandFormat.REJECT, 0)
		
		# queue of embedded CTRL commands to execute
		ctrlQueue = []
		
		# the command
		cmd = ""
		
		# numerical arguments (each possibly None, meaning default
		# from policy)
		args = []
		
		# ignore <ESC>
		index = 1
		
		# look for [
		while index < len(bytes):
			byte = bytes[index]
			index += 1
		
			if self.ctrlInstance.accepts(byte):
				# embedded CTRL command
				ctrlQueue.append(byte)
			elif byte != '[':
				# first non-CTRL is not '['
				return (CommandFormat.REJECT, 0)
			else:
				break
		
		# loop fell through
		if index == len(bytes):
			return (CommandFormat.PARTIAL, len(bytes))
		
		# parse body of CSI sequence - everything after <ESC>[
		while index < len(bytes):
			byte = bytes[index]
		
			if self.ctrlInstance.accepts(byte):
				# embedded CTRL command
				ctrlQueue.append(byte)
			elif cmd == '' and byte in self.prefix:
				# prefix byte, such as '?' in <ESC>[?..l
				cmd += byte
			elif byte >= '0' and byte <= '9':
				# digit, append to last args spot
				if len(args) == 0: args.append(None)
				if args[-1] is None:
					args[-1] = int(byte)
				else:
					args[-1] *= 10
					args[-1] += int(byte)
			elif byte == ';':
				# digit separator, make way for new digit
				args.append(None)
			else:
				# first genuine character
				cmd += byte
				break
			
			index += 1
		
		# loop fell through
		if index == len(bytes):
			return (CommandFormat.PARTIAL, len(bytes))
		
		# execute enqueued embedded CTRL commands
		for ctrlCmd in ctrlQueue:
			self.ctrlInstance.interpret(ctrlCmd)

		if cmd in self.calls:
			return (CommandFormat.ACCEPT, index+1, cmd, args)
		else:
			return (CommandFormat.UNKNOWN, index+1, cmd)

	def interpret(self, cmd, args):
		try:
			self.calls[cmd](*args)
			if VERBOSE: print "CSI %s %s"%(repr(cmd),repr(args))
		except UnknownControlSequence, s:
			if VERBOSE:
				print "UNKNOWN SEQUENCE",args,"FOR CSI",cmd

##
## Parameter Policies
##

class ParameterPolicy(object):
	"""
	Base class of parameter type policies used to describe CSI codes.
	"""

class Numeric(ParameterPolicy):
	"""
	{Pn;Pn;...}: A set of numeric parameters, with defaults.
	"""
	def __init__(self, function, *defaults):
		self.function = function
		self.defaults = defaults
	def __call__(self, *args):
		if len(args) > len(self.defaults):
			args = args[:len(self.defaults)]
			if VERBOSE: print "WARNING: Too many arguments."
		elif len(args) < len(self.defaults):
			args = [arg for arg in args]
			args.extend(self.defaults[len(args):])
		
		argv = []
		for arg,default in zip(args,self.defaults):
			if arg is None:
				argv.append(default)
			else:
				argv.append(arg)
		return self.function(*argv)

class Ignore(ParameterPolicy):
	"""
	Ignores any passed arguments.
	"""
	def __init__(self, function):
		self.function = function
	def __call__(self, *args):
		self.function()

class Subfunction(ParameterPolicy):
	"""
	{Ps}: A single subfunction selector
	
	A callable object that uses a numerical parameter to dispatch
	the request to one of several registered function.
	"""
	def __init__(self, subfunctions):
		self.subfunctions = subfunctions
	
	def __call__(self, *selectors):
		if len(selectors) == 0:
			self.callSubfunction(0)
		else:
			for selector in selectors:
				self.callSubfunction(selector)
	
	def callSubfunction(self, selector):
		if selector not in self.subfunctions:
			raise UnknownControlSequence(subfunction=selector)
		self.subfunctions[selector]()

class SubfunctionWithArguments(ParameterPolicy):
	"""
	{Ps;Pn;Pn;...}: A subfunction selector with (default) arguments.
	"""
	def __init__(self, subfunctions, *defaults):
		self.subfunctions = subfunctions
		self.defaults = defaults
	def __call__(self, *args):
		if len(args) == 0:
			if VERBOSE: print "WARNING: Broken control sequence."
			args = [0]
		args = list(args)
		selector = args.pop(0)
		if selector is None:
			selector = 0
		if selector not in self.subfunctions:
			raise UnknownControlSequence(subfunction=selector)
		if len(args) > len(self.defaults):
			args = args[:len(self.defaults)]
			if VERBOSE: print "WARNING: Too many arguments."
		elif len(args) < len(self.defaults):
			args.extend(self.defaults[len(args):])
		for i in range(len(args)):
			if args[i] is None:
				args[i] = self.defaults[i]
		self.subfunctions[selector](*args)


##
## Emulator Command Sequence Database
##

def emulatorDB(terminal):
	"""
	Constructs a database of all supported escape sequences.
	"""
	class Attributes:
		"""
		Text attributes (from ATTR_* at top of file) currently
		in play.
		"""
		def __init__(self):
			self.attributes = []
			
		def set(self, attribute):
			"""
			Sets the given attribute.  If the attribute is a FG or
			BG, any existing FG or BG color is cleared.
			"""
			if IS_ATTR_FG(attribute):
				self.attributes = [attr for attr in self.attributes if not IS_ATTR_FG(attr)]
			elif IS_ATTR_BG(attribute):
				self.attributes = [attr for attr in self.attributes if not IS_ATTR_BG(attr)]
			
			if attribute not in self.attributes:
				self.attributes.append(attribute)
			self.attributes.sort()
			self.__update()
			
		def clr(self, attribute):
			"""
			Clears the given attribute from the attributes list.
			"""
			if attribute in self.attributes:
				self.attributes.remove(attribute)
			self.__update()
		
		def reset(self):
			"""
			Resets all attributes to default.
			"""
			self.attributes = []
			self.__update()
		
		def __update(self):
			"""
			Updates the attributes as visible in the terminal.
			"""
			terminal.tdSetAttributes(tuple(self.attributes))
			
	attributes = Attributes()

	##
	## CTRL functions
	##

	def BEL():
		"""
		Bell; VT100;
		
		Sounds a bell.
		"""
		def interpret():
			terminal.tdSoundBell()
		return (CTRL, 'G', interpret)

	def BS():
		"""
		Back space; VT100;
		
		Moves the cursor back one character.
		"""
		def interpret():
			terminal.tdMoveCursorRelative(0,-1)
		return (CTRL, 'H', interpret)
	
	def HT():
		"""
		Horizontal tab
		
		Moves the cursor to the next tab or to the right margin
		if no more tabs are set.
		"""
		def interpret():
			terminal.tdInsertAtCursor("\t")
#			terminal.tdMoveCursorRelative(0,1)
		return (CTRL, 'I', interpret)
	
	def LF():
		"""
		Line feed; VT100;
		
		Moves the cursor down to the next line (same column).
		"""
		def interpret():
			if terminal.tdCursor[0] == terminal.tdMargin[1]:
				terminal.tdDeleteLine(terminal.tdMargin[0])
				terminal.tdInsertLine(terminal.tdMargin[1]-1)
			else:
				terminal.tdMoveCursorRelative(1,0)
#			terminal.tdMoveCursorRelative(1,0,margin=False)
		return (CTRL, 'J', interpret)
	
	def VT():
		"""
		Vertical tab; VT100;
		
		Same as LF.
		"""
		return (CTRL, 'K', LF()[2])
	
	def FF():
		"""
		Form feed; VT100;
		
		Same as LF.
		"""
		return (CTRL, 'L', LF()[2])
		
	def CR():
		"""
		Carriage Return; VT100;
		
		Moves the cursor to the start of the current line.
		"""
		def interpret():
			terminal.tdMoveCursor(colAbsolute=1,margin=False)
		return (CTRL, 'M', interpret)

	def CAN():
		"""
		Cancels the current multi-byte escape sequence.
		"""
		def interpret():
			raise CAN_Exception
		return (CTRL, 'X', interpret)

	##
	## CSI functions
	##	
			
	def ICH():
		"""
		Insert (blank) character; VT100;
		
		Inserts a number of blank characters at the current cursor pos.
		Parameter value of 0 or 1 means insert 1 character.
		"""
		def interpret(chars):
			if chars < 1: chars = 1
			terminal.tdMakeRoomForChars(chars)
			terminal.tdInsertAtCursor(' '*chars, overwrite=False)
			terminal.tdMoveCursorRelative(0,-chars)
		
		return (CSI, '@', Numeric(interpret, 1))
	
	def CUU():
		"""
		Cursor Up; VT100;
		
		Moves the cursor up the specified number of lines.  Does
		not move past the top margin.  Always moves at least one line.
		"""
		def interpret(lines):
			if lines < 1: lines = 1
			terminal.tdMoveCursorRelative(-lines,0)
		return (CSI, 'A', Numeric(interpret, 1))

	def CUD():
		"""
		Cursor Down; VT100;
		
		Moves the cursor down the specified number of lines.  Does
		not move past the bottom margin.  Always moves at least one line.
		"""
		def interpret(lines):
			if lines < 1: lines = 1
			terminal.tdMoveCursorRelative(lines,0)
		return (CSI, 'B', Numeric(interpret, 1))
		
	def CUF():
		"""
		Cursor Forward; VT100;
		
		Moves the cursor right the specified number of chars. Does
		not move past the right margin.  Always moves at least one char.
		"""
		def interpret(chars):
			if chars < 1: chars = 1
			terminal.tdMoveCursorRelative(0,chars)
		return (CSI, 'C', Numeric(interpret, 1))
	
	def CUB():
		"""
		Cursor Backward; VT100;
		
		Moves the cursor left the specified number of chars. Does
		not move past the left margin.  Always moves at least one char.
		"""
		def interpret(chars):
			if chars < 1: chars = 1
			terminal.tdMoveCursorRelative(0,-chars)
		return (CSI, 'D', Numeric(interpret, 1))

	def CHA():
		"""
		Cursor Horizontal Absolute; VT100;
		
		Moves the cursor to the given column.
		"""
		def interpret(pos):
			terminal.tdMoveCursor(colAbsolute=pos)
		return (CSI, 'G', Numeric(interpret, 1))

	def CUP():
		"""
		Cursor Position; ECMA VT100;
		
		Moves the cursor to the specified position.
		"""
		def interpret(line,column):
			terminal.tdMoveCursorAbsolute(line,column,margin=False)
		return (CSI, 'H', Numeric(interpret, 1, 1))
	
	def ED():
		"""
		Erase in display; ECMA VT100;
		
		Erase in display.  Does not affect cursor.
		
		0: clear to end of screen
		1: clear to start of screen
		2: clear entire screen
		"""
		def clear_to_end_of_screen():
			terminal.tdDeleteInLine(terminal.tdCursor[0], \
				terminal.tdCursor[1], terminal.tdSize[1])
			for i in range(terminal.tdSize[0],terminal.tdCursor[0],-1):
				terminal.tdDeleteInLine(i,1,terminal.tdSize[1])
		def clear_to_start_of_screen():
			col = terminal.tdCursor[1]
			terminal.tdMoveCursor(colAbsolute=1)
			terminal.tdInsertAtCursor(" "*col, overwrite=True)
			for i in range(1,terminal.tdCursor[0]):
				terminal.tdDeleteInLine(i,1,terminal.tdSize[1])
			terminal.tdMoveCursor(colAbsolute=col)
		def clear_entire_screen():
			for i in range(1,terminal.tdSize[0]+1):
				terminal.tdDeleteInLine(i,1,terminal.tdSize[1])
		
		subfunctions = {
			0: clear_to_end_of_screen,
			1: clear_to_start_of_screen,
			2: clear_entire_screen }
		
		return (CSI, 'J', Subfunction(subfunctions))

	def EL():
		"""
		Erase in Line; ECMA VT100;
		
		Erases characters in the current line.  Does not affect cursor.
		"""
		def clear_to_end_of_line():
			terminal.tdDeleteInLine(terminal.tdCursor[0], \
				terminal.tdCursor[1], terminal.tdSize[1])
		def clear_to_start_of_line():
			col = terminal.tdCursor[1]
			terminal.tdMoveCursor(colAbsolute=1)
			terminal.tdInsertAtCursor(" "*col, overwrite=True)
			terminal.tdMoveCursor(colAbsolute=col)
		def clear_entire_line():
			terminal.tdDeleteInLine(terminal.tdCursor[0],
				1, terminal.tdSize[1])
		
		subfunctions = {
			0: clear_to_end_of_line,
			1: clear_to_start_of_line,
			2: clear_entire_line }
		
		return (CSI, 'K', Subfunction(subfunctions))

	def IL():
		"""
		Insert Line; VT102;
		
		Inserts a number of lines before the line.  Shifts contents
		down as necessary.  Resets cursor to column 1.
		
		Ignored outside of scrolling region.  When inside scrolling
		region, removes lines that would scroll past the bottom of
		the scrolling region.
		
		Always inserts at least one line.
		"""
		def interpret(count):
			if not terminal.tdIsCursorInScrollingRegion(): return
			
			count = max(1, count)
			count = min(count,terminal.tdMargin[1]-terminal.tdCursor[0]+1)
			
			for i in range(count):
				terminal.tdDeleteLine(terminal.tdMargin[1])
				terminal.tdInsertLine(terminal.tdCursor[0]-1)
			terminal.tdMoveCursor(colAbsolute=1)
		return (CSI, 'L', Numeric(interpret, 1))
	
	def DL():
		"""
		Delete line; VT102;
		
		Deletes a number of lines starting with the current.  Shifts
		contents up as necessary.  Resets cursor to column 1.

		Ignored outside of scrolling regoin.  When inside scrolling
		region, inserts new blank lines at bottom of scrolling region.		
						
		Always deletes at least one line.
		"""
		def interpret(count):
			if not terminal.tdIsCursorInScrollingRegion(): return

			count = max(1, count)
			count = min(count, terminal.tdMargin[1]-terminal.tdCursor[0]+1)

			for i in range(count):
				terminal.tdDeleteLine(terminal.tdCursor[0])
				terminal.tdInsertLine(terminal.tdMargin[1]-1)
			terminal.tdMoveCursor(colAbsolute=1)
		return (CSI, 'M', Numeric(interpret, 1))

	def DCH():
		"""
		Delete characters; ECMA;
		
		Delete the specified number of characters.  Does not affect cursor.
		
		Always deletes at least one character.
		"""
		def interpret(chars):
			if chars < 1: chars = 1
			terminal.tdDeleteInLine(terminal.tdCursor[0], \
				terminal.tdCursor[1], terminal.tdCursor[1]+chars)
		return (CSI, 'P', Numeric(interpret, 1))

	def ECH():
		"""
		Erase characters;
		
		Erases the specified number of characters starting at the
		current cursor position.  Character attributes are reset.
		Always erases at least the current cursor position.
		"""
		def interpret(chars):
			if chars < 1: chars = 1
			col = terminal.tdCursor[1]
			terminal.tdInsertAtCursor(" "*chars, overwrite=True)
			terminal.tdMoveCursor(colAbsolute=col)
			
		return (CSI, 'X', Numeric(interpret, 1))

	def DA():
		"""
		Device Attributes Request; ECMA VT100;
		
		Responds with advanced video options code (2): <ESC>[?1;2c
		"""
		def interpret():
			terminal.tdToHost(CHR_ESC+"[?1;2c")
		return (CSI, 'c', Ignore(interpret))

	def VPA():
		"""
		Vertical Position Absolute; ECMA;
		
		Moves the cursor to the given line.
		"""
		def interpret(line):
			terminal.tdMoveCursor(rowAbsolute=line,margin=False)
		return (CSI, 'd', Numeric(interpret, 1))

	def HVP():
		"""
		Horizontal and Vertical position; ECMA VT100;
		
		Identical to CUP.
		"""
		return (CSI, 'f', CUP()[2])

	def DECSTBM():
		"""
		DEC Set Top and Bottom Margins; VT100;
		
		Sets the top and bottom lines of the region where cursor
		scrolling is allowed.  Affects relative cursor position
		depending on state of origin mode flag.
		"""
		def interpret(top,bottom):
			if bottom == -1:
				bottom = terminal.tdSize[0]
			top = max(1, min(top, terminal.tdSize[0]))
			bottom = max(1, min(bottom, terminal.tdSize[0]))
			terminal.tdMargin = (top,bottom)
		return (CSI, 'r', Numeric(interpret,1,-1))
		

	def SGR():
		"""
		Select graphic rendition; ECMA VT100;
		"""
		subfunctions = {
			0: 		 lambda: attributes.reset(),
			1: 		 lambda: attributes.set(ATTR_BOLD),
			22: 		 lambda: attributes.clr(ATTR_BOLD),
			4: 		 lambda: attributes.set(ATTR_UNDERLINE),
			24: 		 lambda: attributes.clr(ATTR_UNDERLINE),
			5: 		 lambda: attributes.set(ATTR_BLINK),
			25: 		 lambda: attributes.clr(ATTR_BLINK),
			7: 		 lambda: attributes.set(ATTR_INVERSE),
			27: 		 lambda: attributes.clr(ATTR_INVERSE),
			ATTR_FG_BLACK:	 lambda: attributes.set(ATTR_FG_BLACK),
			ATTR_BG_BLACK:	 lambda: attributes.set(ATTR_BG_BLACK),
			ATTR_FG_RED:	 lambda: attributes.set(ATTR_FG_RED),
			ATTR_BG_RED:	 lambda: attributes.set(ATTR_BG_RED),
			ATTR_FG_GREEN:	 lambda: attributes.set(ATTR_FG_GREEN),
			ATTR_BG_GREEN:	 lambda: attributes.set(ATTR_BG_GREEN),
			ATTR_FG_YELLOW:	 lambda: attributes.set(ATTR_FG_YELLOW),
			ATTR_BG_YELLOW:	 lambda: attributes.set(ATTR_BG_YELLOW),
			ATTR_FG_BLUE:	 lambda: attributes.set(ATTR_FG_BLUE),
			ATTR_BG_BLUE:	 lambda: attributes.set(ATTR_BG_BLUE),
			ATTR_FG_MAGENTA: lambda: attributes.set(ATTR_FG_MAGENTA),
			ATTR_BG_MAGENTA: lambda: attributes.set(ATTR_BG_MAGENTA),
			ATTR_FG_CYAN:	 lambda: attributes.set(ATTR_FG_CYAN),
			ATTR_BG_CYAN:	 lambda: attributes.set(ATTR_BG_CYAN),
			ATTR_FG_WHITE:	 lambda: attributes.set(ATTR_FG_WHITE),
			ATTR_BG_WHITE:	 lambda: attributes.set(ATTR_BG_WHITE),
			ATTR_FG_DEFAULT: lambda: attributes.set(ATTR_FG_DEFAULT),
			ATTR_BG_DEFAULT: lambda: attributes.set(ATTR_BG_DEFAULT)
			}
		
		return (CSI, 'm', Subfunction(subfunctions))

	def SM():
		"""
		Set Mode
		
		Sets the given mode flag.
		"""
		def IRM():
			"""
			Insert/Replace Mode.
			
			When set, insert mode means that newly inserted
			characters will shift over the line contents to
			the right (chopping off all extra characters pushed
			past the right margin).
			"""
			terminal.tdInsertMode = True
		
		subfunctions = {
			4: IRM
		}
		return (CSI, 'h', Subfunction(subfunctions))
	
	def RM():
		"""
		Reset Mode
		
		Resets the given mode flag.
		"""
		def IRM():
			termina.tdInsertMode = False
		
		subfunctions = {
			4: IRM
		}
		return (CSI, 'l', Subfunction(subfunctions))


	def DECSET():
		"""
		DEC private set mode; VT100 xterm;
		
		Saves or sets state.  Can be restored with DECRST.
		"""
		def DECCOLM():
			"""
			Sets the terminal to 132 column mode.
			"""
			terminal.tdSize = (terminal.tdSize[0], 132)
		def DECOM():
			"""
			Sets Origin mode: the cursor is now locked within
			the margin region, and all line,col numbers are
			relative to the margin's origin.
			"""
			terminal.tdOriginMode = True
			
		subfunctions = {
			3: DECCOLM,
			6: DECOM
		}
		return (CSI, '?h', Subfunction(subfunctions))
	
	def DECRST():
		"""
		DEC private restore mode; VT100 xterm;
		
		Restores a set state.  See state descriptions in DECSET.
		"""
		def DECCOLM():
			terminal.tdSize = (terminal.tdSize[0], 80)
		def DECOM():
			terminal.tdOriginMode = False
		
		subfunctions = {
			3: DECCOLM,
			6: DECOM
		}
		return (CSI, '?l', Subfunction(subfunctions))


	##
	## ESC functions
	##
	
	def VT52CUU():
		"""
		VT52 Cursur Up; VT52;
		
		Same as CUU.
		"""
		return (ESC, 'A', CUU()[2])
	def VT52CUD():
		return (ESC, 'B', CUD()[2])
	def VT52CUF():
		return (ESC, 'C', CUF()[2])
#	def VT52CUB():
#		return (ESC, 'D', CUB()[2])

	def IND():
		"""
		Index
		
		Moves the cursor down one line in the same column, scrolling
		as necessary.  Same as CUD.
		"""
		return (ESC, 'D', CUD()[2])

	def NEL():
		"""
		Move to start of next line; ECMA VT100;
		
		Moves the cursor to the first charcter of the line one down.
		Performs a scroll if necessary.  (Uses active scroll area.)
		"""
		def interpret():
			if terminal.tdCursor[0] == terminal.tdMargin[1]:
				terminal.tdDeleteLine(terminal.tdMargin[0])
				terminal.tdInsertLine(terminal.tdMargin[1]-1)
			else:
				terminal.tdMoveCursor(1,colAbsolute=1)
		return (ESC, 'E', interpret)

	def RI():
		"""
		Reverse Index; ECMA VT100;
		
		Move the cursor up 1 line (same column).  If at the top
		of the scrollable area, inserts a blank line to shift
		the contents down.
		"""
		def interpret():
			if terminal.tdCursor[0] == terminal.tdMargin[0]:
				terminal.tdDeleteLine(terminal.tdMargin[1])
				terminal.tdInsertLine(terminal.tdMargin[0]-1)
			else:
				terminal.tdMoveCursor(-1,0)
		return (ESC, 'M', interpret)

	def DECALN():
		"""
		DEC Screen alignment test
		
		Fills the screen with capital E's.  I don't know why this is
		implemented on non-physical devices, which consequently don't
		need alignment, but for some reason it is.
		"""
		def interpret():
			for i in range(terminal.tdMargin[0],terminal.tdMargin[1]+1):
				terminal.tdMoveCursorAbsolute(i,1,margin=False)
				terminal.tdInsertAtCursor("E"*terminal.tdSize[1],
					overwrite=True)
		return (ESC, '#8', interpret)

	return [item[1]() for item in locals().items() if item[0].isupper()]


##
## Emulator interface
##

class Emulator(object):
	"""
	A basic (dumb) emulator that does not parse escape sequences.
	"""
	def __init__(self, terminal):
		assert isinstance(terminal,TerminalDisplay)
		self.terminal = terminal

	def interpret(self, job, string):
		self.terminal.feedTerminal(job, string)

class XTermEmulator(Emulator):
	"""
	An emulator that parses all escape sequences in the emulatorDB().
	
	Intelligently handles escape sequences that cross read boundaries;
	identifies itself as xterm to fool programs that care.
	"""
	def __init__(self, terminal):
		Emulator.__init__(self, terminal)
		
		# format class -> instance
		formatMap = {}
		formatMap[CTRL] = CTRL()
		formatMap[ESC]  = ESC(formatMap[CTRL])
		formatMap[CSI]  = CSI(formatMap[CTRL])
		
		for command in emulatorDB(terminal):
			if command[0] not in formatMap:
				raise "Unknown format %s"%(command[0])
			formatMap[command[0]].register(*command[1:])
		
		self.formats = formatMap.values()
		
		# cached prefix string from partial escape sequence
		self.prefix = None

		# queue of characters to send to the terminal
		class queue:
			queue = []
			def enqueue(me,char):
				if VERBOSE and (ord(char)>=128 or ord(char)<32):
					print "WARNING: INSERTING chr(%s)"%ord(char)
				me.queue.append(char)
			def flush(me):
				if len(me.queue):
					if VERBOSE:
						print "INSERTING %s" % \
							repr("".join(me.queue))
					self.terminal.tdInsertAtCursor(
						"".join(me.queue))
					me.queue = []
		self.queue = queue()

	def interpret(self, bytes):
		# remember saved characters from the previous call
		# (partial escape sequences)
		if self.prefix is not None:
			bytes = buffer(str(self.prefix)+str(bytes))
			self.prefix = None

		while len(bytes):
			for format in self.formats:
				recog = format.recognize(bytes)
				status = recog[0]
				
				if status == CommandFormat.REJECT:
					# short circuit: this is the most
					# common case, so move ahead
					continue
				if status == CommandFormat.PARTIAL:
					# partially recognized, save it up
					# and wait for more input
					self.prefix = bytes
					return
				if status == CommandFormat.UNKNOWN:
					# unknown sequence in a correct
					# format: ignore
					if VERBOSE:
						print "UNKNOWN COMMAND: %s,%s"\
							% (format,recog[2])
					bytes = bytes[recog[1]:]
					break
				if status == CommandFormat.ACCEPT:
					# accepted: call interpret with args
					self.queue.flush()
					format.interpret(*recog[2:])
					bytes = bytes[recog[1]:]
					break
			else:
				# no format picked up a byte block starting 
				# here, so enque one byte for a terminal feed
				self.queue.enqueue(bytes[0])
				bytes = bytes[1:]

		self.queue.flush()


if __name__ == '__main__':
	import unittest
	from TerminalDisplay import TestTerminalDisplay
	
	class TestEmulator(TestTerminalDisplay):
		"""
		Tests on each emulator command and mode, etc.
		"""
		def testIRM(self):
			"""
			Insert/Replace Mode test
			"""
			pass
	
	unittest.main()

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
GCI Schema for communicating with the terminal object.

(c) Daniel Ramage 2005
"""

from decorators import *

import schema
import Runtime.Types

def GCILibrary(context):
	""" GCI Commands. """
	
	@singleton
	def exit():
		def execute(ast, args):
			context['ui'].terminal.closeTerminal()
		return Runtime.Types.Invokable(None, execute)

	@singleton
	def newtab():
		def execute(ast, args):
			context['ui'].terminal.newTerminalTab()
		return Runtime.Types.Invokable(None, execute)

	@singleton
	def newwin():
		def execute(ast, args):
			context['ui'].terminal.newTerminalWindow()
		return Runtime.Types.Invokable(None, execute)
	
	@singleton
	def GrammarWindow():
		def execute(ast, args):
			import ui.gtk.ui
			ui.gtk.ui.GTKGrammarWindow()
		return Runtime.Types.Invokable(None, execute)
	
	builtins = {}
	for k,v in locals().iteritems():
		if Runtime.Types.IsA(v, Runtime.Types.Invokable):
			builtins[k] = v
	return builtins


class GCISchema(schema.Schema):
	""" GCI Schema for GCI builtin functions. """
	
	def __init__(self, context):
		schema.Schema.__init__(self, context)
		self.functions = GCILibrary(context)

	def HasItem(self, name):
		return name in self.functions
	
	def GetItem(self, name):
		return self.functions[name]

schema.Context.register('gci', GCISchema)

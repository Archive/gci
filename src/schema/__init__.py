# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Defines global names for the schema package.

(c) Daniel Ramage 2005
"""

from decorators import *

__all__ = 'Schema', 'Context'

class Schema(object):
	"""	This prototype class defines the methods a schema should export.  This
		export is enforced by assertValidSchema, below. """
	
	@takes("Schema", "Context")
	def __init__(self, context):
		self.context = context

	@takes("Schema", dict)	
	def UpdateSymbolLibrary(self, library):
		"""	Updates the given symbol library with new symbols for this schema.
			the global symbol library.  Symbol keys should look like, e.g.:
				'@sh.directory':RuleSymbol
				... """
	
	@takes("Schema", str)
	def HasItem(self, name):
		""" Returns true if the Schema contains the given item. """

	@takes("Schema", str)
	def GetItem(self, name):
		""" Returns the item with the given name relative to the given context. """

class Context(dict):
	""" Context is a dict that maps from a schema to schema-specific context. """
	def __init__(self, defaults={}):
		self.update(defaults)
	
		for name, constructor in Context.constructors.iteritems():
			self[name] = constructor(self)
	
	# list of registered schema constructors
	constructors = {}
	
	@staticmethod
	@takes(str, callable)
	def register(name, constructor):
		""" Registers the given schema constructor for future instances. """
		if name in Context.constructors:
			raise NameError, "'%s' already registered as %s" \
				% (name, Context.constructors[name])
		Context.constructors[name] = constructor

##
## import default schemas
##

import gci, app, sh

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
app: schema

Communicates with running applications over D-BUS.

(c) Daniel Ramage 2005
"""

import dbus

from decorators import *
from OPEP.AST import *

import Runtime.Parser
import Runtime.Types

import schema

bus = dbus.SessionBus()

class RemoteApp(object):
	"""	Everything needed to communicate with an app over D-Bus. """
	@takes("RemoteApp", str)
	def __init__(self, dbusBusName):
		self.dbusBusName = dbusBusName

	@property
	def remote():
		""" The GCIAppService description object. """
		def fget(self):
			if "_remote" not in dir(self):
				proxy = bus.get_object(self.dbusBusName, '/GCIAppService')
				self._remote = dbus.Interface(proxy, 'org.gnome.GCI.AppService')
			return self._remote

	@property
	def grammar():
		""" The grammar provided by the GCIAppService. """
		def fget(self):
			return self.remote.GetGrammar()

	@memoized
	def GetParser(self, context):
		return Runtime.Parser.CustomParser(self.grammar, context)

class AppRegistry(dict):
	""" Keeps the map from app shortname to RemoteApp. """
AppRegistry = AppRegistry()

##
## hardcoded app registry .. TODO: un-hardcode
##

# AppRegistry['gci'] = RemoteApp("org.gnome.GCI.GCIAppService")
AppRegistry['nautilus'] = RemoteApp("org.gnome.nautilus.GCIAppService")

##
## Export the schema
##

class AppSchema(schema.Schema):
	""" Exported 'app:' schema. """
	def __init__(self, context):
		schema.Schema.__init__(self, context)

	def HasItem(self, name):
		return name in AppRegistry
	
	def GetItem(self, name):
		def execute(ast, args):
			cmd = list(ast.walkForAttribute('cmd'))[0].value
			return AppRegistry[name].remote.Invoke(cmd, args)
		
		parser = AppRegistry[name].GetParser(self.context)
		return Runtime.Types.Invokable(parser, execute)

schema.Context.register('app',AppSchema)

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Interface module provides decorators and utilities for dynamic
typesafe polymorphic interfaces in Python >=2.4.

(c)Daniel Ramage 2005
"""

import types

__all__ = ['Interface', 'Proxy', 'InterfaceTypeError',
           'implements', 'isa', 'asa']

class Interface(object):
	"""	Interface definition.
		
		Subclasses should extend Interface in order to provide
		method signatures for required interface methods. """


# List of reserved names that interface methods may not use.
ReservedProxyNames = ("Object", "Interface")

class Proxy(object):
	""" A view of a "real" object as a particular Interface. """
	def __init__(self, object, interface):
		self.Object = object
		self.Interface = interface
		
		for interface in interface.__mro__:
			for if_attr_name in (n for n in dir(interface) if n[0] != '_'):
				# the unbound interface method
				if_method = getattr(interface, if_attr_name)
			
				# find a bound implementation method
				im_method = None
				for im_attr_name in dir(self.Object):
					im_method = getattr(self.Object, im_attr_name)
					if 'implements' in dir(im_method) and \
						if_method in getattr(im_method, 'implements'):
						break
				else:
					raise InterfaceTypeError(if_method,
						"Unbound method %s" % if_attr_name)
			
				def create_invoker():
					bound_method = im_method
					def proxy_invoke(*args, **argd):
						return bound_method(*args, **argd)
					proxy_invoke.__doc__ = bound_method.__doc__
					return proxy_invoke
		
				setattr(self, if_attr_name, create_invoker())
			
class InterfaceTypeError(Exception):
	"""	Interface-exception. """
	def __init__(self, interface_method, message):
		Exception.__init__(self, "%s\n  %s" 
			% (interface_method.func_name,message))

def implements(interface_method):
	"""	Decorator for marking a method as an implementation of
		a particular interface method.
		
		Example usage:
		
		class IPrint(Interface):
			def print(string):
				" Print a string "
		
		class ConsolePrinter:
			@implements(IPrint.print)
			def showIt(string):
				print string
		"""
	def decorator(implementation_method):
		# name checking
		if interface_method.func_name[0] == '_':
			raise InterfaceTypeError(
				interface_method,
				"Private and special methods are illegal in Interfaces")
		elif interface_method.func_name in ReservedProxyNames:
			raise InterfaceTypeError(
				interface_method,
				"Interface defines method with a reserved name")
		
		if not hasattr(implementation_method, 'implements'):
			implementation_method.implements = []
		implementation_method.implements.append(interface_method)
		return implementation_method
	return decorator

def _InterfaceDictionary(cls):
	""" Builds the interface dictionary for the given class. """
	
	attrs = (getattr(cls, name) for name in dir(cls))
	methods = [attr for attr in attrs if type(attr) == types.UnboundMethodType]
	
	# set of interfaces that the class supports (or claims to)
	interfaces = set()
	
	# methods that are actually implemented
	implementations = []
	
	# populate interfaces, implementations
	for method in methods:
		if hasattr(method, 'implements'):
			for impl in getattr(method, 'implements'):
				implementations.append(impl)
				interfaces.add(impl.im_class)
	
	# ensure that all interfaces are fully supported
	for interface in interfaces:
		superinterfaces = interface.__mro__
		print superinterfaces
		for iface in superinterfaces:
			for iface_attr_name in (n for n in dir(iface) if n[0] != '_'):
				# the unbound interface method
				iface_method = getattr(iface, iface_attr_name)
			
				if iface_method not in implementations:
					raise InterfaceTypeError(iface_method, "not provided by %s"%cls)
	
	return interfaces

def asa(obj, interface):
	""" Returns a Proxy view of the given obj as the given interface.
		
		Raises InterfaceTypeError if not all methods are implemented. """
	real = obj
	if isinstance(real, Proxy): real = obj.Object

	assert isa(real, interface)
	return Proxy(real, interface)

def isa(obj, interface):
	"""	Returns true if the given object defines a valid implementation
		of all methods in the given interface. """
	real = obj
	if isinstance(real, Proxy): real = obj.Object
	
	if not hasattr(real.__class__, "interfaces"):
		try:
			setattr(real.__class__, "interfaces",
				_InterfaceDictionary(real.__class__))
		except TypeError, e:
			return False
	
	return interface in real.interfaces

##
## basic test cruft
##

if __name__ == '__main__':

	class IPrint(Interface):
		def base(string):
			""" Base """
		def show(string):
			""" Shows the given string. """

	class IPrintWithMore(IPrint):
		def throw(string):
			""" Thows a string high into the air. """

	class Base(object):
		@implements(IPrint.base)
		def base():
			pass

	class ConsolePrint(Base):
		@implements(IPrint.show)
		def printIt(self, string):
			print string

	class CountingPrinter(Base):
		def __init__(self):
			self.counter = 0
		
		@implements(IPrint.show)
		def printIt(self, string):
			self.counter += 1
			print self.counter, string
		
		@implements(IPrintWithMore.throw)
		def throwIt(self, string):
			print "threw", string

	print isa(ConsolePrint(), IPrint)
	console = asa(ConsolePrint(), IPrint)
	console.show("hi")

	counter = asa(CountingPrinter(), IPrintWithMore)
	counter.show("hi")
	counter.show("hi")
	counter.show("hi")
	counter.show("hi")
	counter.throw("woot")
	print counter.show.__doc__

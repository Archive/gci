# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Function and method decorators used throughout GCI.

Portions of this code have been borrowed from various cookbook locations
where cited.

(c) Daniel Ramage 2005
"""

import sys
import types
import weakref

def export(function, namespace=locals()):
	""" Marks a function for export from the module. """
	if '__all__' not in namespace:
		namespace['__all__'] = []
	if type(function) is types.FunctionType:
		namespace['__all__'].append(function.func_name)
	else:
		for name,value in namespace.iteritems():
			if value is function:
				namespace['__all__'].append(name)
	return function
export(export)

@export
def probe(function, args=()):
	"""	Returns a dict of locals defined in the given function. """
	lookup = {}
	def trace(frame, event, arg):
		# watch for return, steal locals
		if event == 'return':
			for k, v in frame.f_locals.iteritems():
				if k[0] != '_':
					lookup[k] = v
			sys.settrace(None)
		return trace
	sys.settrace(trace)
	function(*args)
	return lookup

@export
def property(function):
	"""	This decorator marks a function as a property.
		Example:
		
		class Village(object):
			@property
			def size():
				''' The number of villagers. '''
				def fget(self):
					return len(self.people)
		"""
	probed = probe(function)
	probed['doc'] = function.__doc__
	return __builtins__["property"](**probed)

@export
def singleton(function):
	""" A singleton is a no-arg function that is evaluated immediately. """
	return function()

# place to hold reference id's for keep-alive
_references = {}

@export
def reference(obj):
	"""	Returns a dict-key-friendly reference to any object.  Keeps a live
		reference to the given obj, preventing the object from being garbage
		collected.  To free the reference, call dereference(obj). """
	if id(obj) not in _references:
		_references[id(obj)] = obj
	return id(obj)

@export
def dereference(obj):
	""" Frees a reference to the give object. """
	if id(obj) in _references:
		del _references[id(obj)]

@export
def memoized(function, cache={}):
	"""	Caches the output of a function.  Keeps references to all arguments
		provided to the function, potentially keeping objects alive and un-
		garbage collected. """
	def proxy(*args):
		key = function, tuple([reference(arg) for arg in args])
		if key not in cache:
			cache[key] = function(*args)
		return cache[key]
	return proxy

@memoized
def square(x):
	print "squaring",x
	return x

################################################################################
#
# Method signature checking decorators
# Dmitry Dvoinikov <dmitry@targeted.org>
# from http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/426123
#
# Major modifications by Daniel Ramage
#
# Sample usage:
#
# class B(object):
#     @takes("B", basestring) # the third parameter (value) could have any type
#     def f(self, name, value):
#         return 0 # return type is not checked here
#
# class D(B):
#     @takes("D", basestring, B) # type D is unknown at this time and is provided by name
#     @returns(int)              # whereas the third parameter could be explicitly typed as B
#     def f(self, name, value):
#         print name
#         return B.f(name, value) + 1 # has to return int
#
################################################################################

################################################################################

def base_names(C):
    "Returns a tuple of names of base classes for a given class"
    
    return tuple([ x.__name__ for x in C.__mro__ ])
    
################################################################################

@export
def anything(argument):	return True

@export
def takes(*types):
	"Method signature checking decorator"
    
	def takes_proxy(method):

		if not __debug__:
			return method
    
		def TypeValidator(pos, sig):
			"""
			Returns a validator that tests if an argument is of the
			specified type.
			"""
			assert isinstance(sig, type)
			def proxy(where, value):
				if value is not None and not isinstance(value, sig):
					raise InputParameterError, \
						"%s expects parameter %d to have type %s; got %s" % \
						(where, pos, sig.__name__, type(value).__name__)
			return proxy
	
		def NamedTypeValidator(pos, sig):
			"""
			Returns a validator that tests if an argument is of the named
			type.
			"""
			assert isinstance(sig, str)
			def proxy(where, value):
				if value is not None and sig not in base_names(value.__class__):
					raise InputParameterError, \
						"%s expects parameter %d to have type %s: got %s" % \
						(where, pos, sig, type(value).__name__)
			return proxy

		def ListValidator(pos, sig):
			"""
			Returns a validator that tests if all elements of a list have the
			specified signature.
			"""
			assert isinstance(sig, list)
		
			if len(sig) != 1:
				raise TypeError, "@takes decorator for list argument %s must " \
					"have a single signature entry (type,string,list)" % (pos)
		
			helper = Validator(pos, sig[0])
		
			def proxy(where, value):
				for v in value:
					helper(where, v)
			return proxy
		
		def CallableValidator(pos, sig):
			""" Returns a validator that tests if the given function returns
				true when applied to the given method. """
			assert callable(sig)
			def proxy(where, value):
				if value is not None and not sig(value):
					raise InputParameterError, \
						"%s expects parameter %d to pass %s test" %	(where, pos, sig)
			return proxy
    
		def Validator(pos, sig):
			"""
			Returns a validator for the argument at pos with the given signature.
	    	
			Raises TypeError on invalid signature.
			"""
			if isinstance(sig, type):
				return TypeValidator(pos, sig)
			elif isinstance(sig, str):
				return NamedTypeValidator(pos, sig)
			elif isinstance(sig, list):
				return ListValidator(pos, sig)
			elif callable(sig):
				return CallableValidator(pos, sig)
			elif isinstance(sig, optional):
				return OptionalValidator(pos, sig)
			else:
				raise TypeError, "@takes decorator expects a signature for "\
					"parameter %s (type, string name of a type, or single-"\
					"element list of either)" % (pos)

		validators = [Validator(i+1,types[i]) for i in range(len(types))]

        
		def takes_invocation_proxy(*args):

			if len(args) > len(types):
				raise InputParameterError, \
					"%s expects at most %d parameters, got %d" % \
					(method.__name__, len(types), len(args))

			where = method.__name__
			if len(args) > 0 and hasattr(args[0],'__class__'):
				where = "%s.%s.%s" % (str(args[0].__class__.__module__),
				                      str(args[0].__class__.__name__), where)

			for i in range(len(args)):
				validators[i](where, args[i])
                
			return method(*args)

		takes_invocation_proxy.__name__ = method.__name__
		
		return takes_invocation_proxy
    
	return takes_proxy

class InputParameterError(TypeError): pass

################################################################################

@export
def returns(sometype):
	"Return type checking decorator"
    
	if not isinstance(sometype, type) and not isinstance(sometype, str):
		raise TypeError("@returns decorator expected parameter to be a type " \
						"or a string (type name), got %s" % type(sometype).__name__)

	def returns_proxy(method):
		
		if not __debug__:
			return method
        
		def returns_invocation_proxy(*args):
			
			result = method(*args)
            
			if result is None:
				return None
            
			if isinstance(sometype, type) and not isinstance(result, sometype):
				raise ReturnValueError("%s should have returned a value of type %s, got %s" %
					(method.__name__, sometype.__name__, type(result).__name__))
			elif isinstance(sometype, str) and not sometype in base_names(result.__class__):
				raise ReturnValueError("%s should have returned a value of type %s, got %s" %
					(method.__name__, sometype, type(result).__name__))
			else:
				return result

		returns_invocation_proxy.__name__ = method.__name__ # just love Python for such tricks
		
		return returns_invocation_proxy
    
	return returns_proxy

class ReturnValueError(TypeError): pass


	
##
## from http://soiland.no/software/abstract/view
##

"""
Abstract method decorator. 

Copyright (c) 2004 Stian Soiland

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Author: Stian Soiland <stian@soiland.no>
URL: http://soiland.no/software
License: MIT
"""

@export
def abstract(f):
    """Decorator for tagging a method as abstract. """
    f.abstract = True
    f.__doc__ = (f.__doc__ or "") + "\nNOTE: Abstract method"
    return f

class Abstract(object):
    """Base class for classes with abstract methods. 
    Raises a TypeError on instantiation if an abstract
    method is not implemented.
    
    Example::

        class AbstractSomething(Abstract):
            def working_method(self):
                return "This method works"

            @abstract    
            def future_work(self):
                '''Implement this method later'''    
        
        class Something(AbstractSomething):
            def future_work(self):
                return "Finally implemented"
    
    Instantiating AbstractSomething will raise an TypeError,
    even if future_work isn't called. Instantiating 
    Something will work, as it's overriding method future_work
    isn't tagged by the @abstract decorator.
               
    Note that decorator syntax is new in Python 2.4. For earlier
    versions, abstract will have to be applied like this::
    
        class AbstractSomething(Abstract):
            def future_work(self):
                '''Implemented later'''
            future_work = abstract(future_work)               

    or::
    
        class AbstractSomething(Abstract):
            def future_work(self):
                '''Implemented later'''
            future_work.abstract = True    
    """
    def __init__(self):
        for methodname in dir(self):
            method = getattr(self, methodname, None)
            if not callable(method):
                continue
            if hasattr(method, "abstract"):
                raise TypeError, ("Abstract method %s not implemented" %
                      method)
export(Abstract)

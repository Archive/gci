# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Defines runtime values and types.

(c) Daniel Ramage 2005
"""

from decorators import *

import itertools

from OPEP.AST import *
from OPEP.Grammar import *
from Types import *


##
## Runtime type library
##

@takes("Context")
def GCITypeLibrary(context):
	"""	Library of symbols referred to by the RuntimeParser. """
	##
	## library of symbols to generate
	##
	
	library = {}

	##
	## list of symbols that will be accessible through @gci.value
	## and decorator for putting them there
	##

	valueSymbols = []
	def ValueSymbol(symbol):
		valueSymbols.append(symbol)
		return symbol
	
	
	##
	## non-exported literals
	##
	
	Colon = LiteralTokenSymbol('COLON', ':')
	
	# a valid name (for variables)
	GCINameLiteral = RegexTokenSymbol("NAME",
		"[a-zA-Z_][a-zA-Z0-9_]*")

	"""
	@gci.literal.word := @builtin.word
	@gci.literal.string := @builtin.squote | @builtin.dquote
	@gci.literal.name := /[a-zA-Z_][a-zA-Z0-9_]*/

	@gci.literal.int := @builtin.word
		{value: int($$)}
		
	@gci.literal.long := @builtin.word
		{value: long($$)}
		
	@gci.literal.float := @builtin.word
		{value: float($$)}
	
	@gci.variable.bound := @gci.literal.name
		{requires: $$ in context}
		{completions: keys(context)}
	
	@gci.variable.lookup := [BreaksForbidden](@gci.value.ItemProvider ':' @gci.literal.name)
		{requires("Item not found"): $child[0]$.HasItem($child[2]$)}
		{completions: $0$.ListItems()}
		{value: $child[0]$.GetItem($child[2]$)}
	
#	@gci.value<T> := @gci.literal.* | @gci.variable.*
#		{requires("Invalid type"): IsA($0$, T)}
#		{value: AsA($0$, T)}
	@gci.value.ItemProvider := @gci.variable.bound | @gci.variable.lookup
		{requires("Not an ItemProvider"): IsA($0$, ItemProvider)}
		{value: AsA($0$, ItemProvider)}
	"""
	
	##
	## literal symbols
	##

	@ValueSymbol
	@SemanticRule("@gci.literal.word", library, WordTokenSymbol)
	def GCIWordLiteral():
		""" An unbound word. """
		def transformAST(ast):
			return AST(ast.type, ast.children[0].value, ast.children[0].match)
	
	@ValueSymbol
	@SemanticRule("@gci.literal.string", library,
	                SQuoteStringTokenSymbol, DQuoteStringTokenSymbol)
	def GCIStringLiteral():
		""" A string literal """
		def transformAST(ast):
			return AST(ast.type, ast.children[0].value, ast.match)

	@ValueSymbol
	@SemanticRule("@gci.literal.int", library, WordTokenSymbol)
	def GCIIntLiteral():
		""" An int literal """
		def transformAST(ast):
			try:
				ast.value = int(ast.children[0].value)
			except ValueError, e:
				ast.addTag(Tag(Status.Reject, "Invalid literal for int"))
			return ast
		
		def acceptsAST(ast):
			return ast.value is not None

	@ValueSymbol
	@SemanticRule("@gci.literal.long", library, WordTokenSymbol)
	def GCILongLiteral():
		""" A long literal """
		def transformAST(ast):
			try:
				ast.value = long(ast.children[0].value)
			except ValueError, e:
				ast.addTag(Tag(Status.Reject, "Invalid literal for long"))
			return ast
		
		def acceptsAST(ast):
			return ast.value is not None

	@ValueSymbol
	@SemanticRule("@gci.literal.float", library, WordTokenSymbol)
	def GCIFloatLiteral():
		""" A float literal """
		def transformAST(ast):
			try:
				ast.value = float(ast.children[0].value)
			except ValueError, e:
				ast.addTag(Tag(Status.Reject, "Invalid literal for float"))
			return ast
		
		def acceptsAST(ast):
			return ast.value is not None

	@ValueSymbol
	@SemanticRule("@gci.variable.bound", library, GCINameLiteral)
	def GCIBoundVariable():
		""" Name of a bound variable. """
		def transformAST(ast):
			name = ast.children[0].value
			if name not in context:
				ast.addTag(Tag(Status.Reject, "Unknown name '%s'" % name))
			else:
				ast.addTag(Tag(Status.Accept))
				ast.value = context[name]
			return ast

	@ValueSymbol
	@SemanticRule("@gci.literal.schema", library, GCINameLiteral)
	def GCISchemaLiteral():
		""" Name referring to a valid schema. """
		def transformAST(ast):
			name = ast.children[0].value
			if name not in context:
				ast.addTag(Tag(Status.Reject, "Unknown schema '%s'" % name))
			else:
				ast.addTag(Tag(Status.Accept))
				ast.value = context[name]
			return ast
		def getASTCompleter(ast):
			def completions(pos):
				base = ""
				if len(ast.children): base = ast.children[0].value[:pos]
				for name in context.keys():
					if name.startswith(base):
						yield name
			return Completer(ast.match, completions)
		
	##
	## @gci.value puts all value symbols together
	##

	@SemanticRule("@gci.value", library, *valueSymbols)
	def GCIValue():
		""" Any valid value. """
		def transformAST(ast):
			# ast.value = ast.children[0].value
			ast.transient = True
			return ast
		
	##
	## runtime-typed symbols
	##
	
	def TypedSymbol(name, runtype, *parents):
		""" Shorthand for creating a type-enforcing symbol. """
		@SemanticRule(name, library, *parents)
		def AutoGeneratedSymbol():
			def transformAST(ast):
				if ast.status == Status.Reject:
					return ast
				ast.value = ast.children[0].value
				return ast
			
			def acceptsAST(ast):
				if ast.status is not None:
					return True
				else:
					return IsA(ast.value, runtype)
		return AutoGeneratedSymbol
	
	GCIString = TypedSymbol("@gci.value.string",
		str,
		GCIStringLiteral)
		
	GCIInt = TypedSymbol("@gci.value.int",
		int,
		GCIIntLiteral)
		
	GCILong = TypedSymbol("@gci.value.long",
		long,
		GCILongLiteral)
		
	GCIFloat = TypedSymbol("@gci.value.float",
		float,
		GCIFloatLiteral)
	
	import schema
	GCISchema = TypedSymbol("@gci.value.schema",
		schema.Schema,
		GCISchemaLiteral)
	
	GCIItemProvider = TypedSymbol("@gci.value.ItemProvider",
		ItemProvider,
		GCIBoundVariable)

	@SemanticRule("@gci.value.typed", library,
		[GCISchema, Colon, GCINameLiteral], [GCINameLiteral])
	def GCITypedValue():
		def transformAST(ast):
			# mark this node as a compound leaf for breaks_required
			ast.isCompoundLeaf = True
			
			if ast.status == Status.Reject: return ast
			
			if len(ast.children) == 3:
				# schema specified
				schema, smatch = ast.children[0].value, ast.children[0].match
				name, nmatch = ast.children[2].value, ast.children[2].match
				
				if not schema.HasItem(name):
					ast.addTag(Tag(Status.Reject, "Unknown name '%s'" % name, nmatch))
				else:
					ast.addTag(Tag(Status.Accept))
					ast.value = schema.GetItem(name)
				return ast
				
			else:
				# no schema specified, search them
				import schema
				
				if ast.match.end+1 < len(ast.match.context):
					if ast.match.context[ast.match.end+1] == ':':
						ast.addTag(Tag(Status.Reject, "Incomplete specification"))
						return ast
				
				name = ast.children[0].value
			
				found = []
				for sName,sSchema in context.iteritems():
					if sSchema.HasItem(name):
						found.append((sName,sSchema))
				
				if len(found) == 0:
					ast.addTag(Tag(Status.Reject, "No schema found for '%s'"%name))
				elif len(found) > 1:
					ast.addTag(Tag(Status.Reject,
						"Multiple schemas found for '%s'.  Use %s"
						% (name, " or ".join(["%s:%s"%(info[0],name) for info in found]))))
				else:
					sName,sSchema = found[0]
					ast.addTag(Tag(Status.Accept,
						"'%s' is potentially ambiguous, use '%s:%s'"
						% (name,sName,name)))
					ast.value = sSchema.GetItem(name)
				return ast
	
	import Parser
	for rule in library['@gci.value.typed'].rules:
		Parser.RuntimeDecorators['BreaksForbidden'].decorate(rule)
	
	GCIInvokable = TypedSymbol("@gci.value.invokable",
		Invokable,
		GCITypedValue)
	
	##
	## item lookup
	##

	@ValueSymbol
	@SemanticRule("@gci.variable.lookup", library,
				[GCIItemProvider, Colon, GCINameLiteral])
	def GCILookupVariable():
		""" A variable accessed via (AttributeProvider):attr """
		def transformAST(ast):
			provider = ast.children[0].value
			itemname = ast.children[2].value
			
			if not isa(provider, ItemProvider):
				ast.addTag(Tag(Status.Reject,
					"%s does not provide any items"%repr(ast.children[0].value),
					ast.children[0].match))
			else:
				provider = asa(provider, ItemProvider)
				if not provider.HasItem(itemname):
					ast.addTag(Tag(Status.Reject,
						"Unknown item %s" % repr(itemname),
						ast.children[2].match))
				else:
					ast.value = provider.GetItem(itemname)
			return ast
	
	import Parser
	for rule in library['@gci.variable.lookup'].rules:
		Parser.RuntimeDecorators['BreaksForbidden'].decorate(rule)

	return library


'''
class T(object):
	""" Defines a runtime type of an object. """
	@staticmethod
	def isa(obj):
		""" Returns true if the object is of the given type. """
		raise "checkrep Unimplemented: subclass of T does not override checkrep"
	
class TNone(T):
	""" No value.  Like void or Python's NoneType. """
	@staticmethod
	def isa(obj):
		return obj is None

class TPrimitive(T):
	""" Primitive value types contain a quantum of data. """

class TCollection(T):
	""" Collection types contain a collection of any other typed value. """

class TExecutable(T):
	""" A value that can be invoked. """

##
## Supported PrimitiveTypes.
##

class TInteger(TPrimitive):
	""" Integer value. """
	@staticmethod
	def isa(obj):
		return isinstance(obj, int)

class TFloat(TPrimitive):
	""" Floating point value. """
	@staticmethod
	def isa(obj):
		return isinstance(obj, float)

class TString(TPrimitive):
	""" String value. """
	@staticmethod
	def isa(obj):
		return isinstance(obj, str)	

class TBoolean(TPrimitive):
	""" A boolean value. """
	@staticmethod
	def isa(obj):
		return True

##
## Supported CollectionTypes.
##

class TIterable(TCollection):
	""" Can be iterated. """
	@staticmethod
	def isa(obj):
		return '__iter__' in dir(obj)

class TList(TCollection):
	""" List of values. """
	@staticmethod
	def isa(obj):
		return isinstance(obj, list)


##
## Value definition - type, instance, metadata
##

def Value(object):
	""" A value with metadata in the GCI runtime engine. """
	
	@takes("Value", Type, object)
	def __init__(self, types, value):
		self.types = set(types)
		self.value = value

	@takes("Value", Type)
	def isa(self, type):
		""" Returns true if this value is of the given type. """
		return type in self.types
	
	def __eq__(self, other):
		return isinstance(other, Value) and \
			self.types == other.types and \
			self.value == other.value
	
	def __hash__(self):
		return hash(len(self.types)) + hash(self.value)


## full list of all types
types = set([locals()[n] for n in dir() if n[0] == 'T'])
'''

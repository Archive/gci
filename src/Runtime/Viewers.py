# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Viewer infrastructure for finding viewer based on value type.

(c) Daniel Ramage 2005
"""

from decorators import *

from Types import *

viewers = []

@takes(str, object, callable)
def register(ui, classtype, constructor):
	""" Registers the given constructor as a function that returns a viewer
		for objects of the given type. """
	viewers.append((ui, classtype, constructor))

@takes(str, object)
def viewer(ui, instance):
	""" Returns the constructor for creating a viewer for the given ui and
		instance. """
	for r_ui, r_classtype, r_constructor in viewers:
		if ui == r_ui and IsA(instance, r_classtype):
			return r_constructor

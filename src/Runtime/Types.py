# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Defines runtime values and types.

(c) Daniel Ramage 2005
"""

from decorators import *
from interface import *

class ItemProvider(Interface):
	"""	Provides attributes by name. """
	def HasItem(name):
		""" Returns true if an attribute with the given name exists. """
	def GetItem(name):
		""" Returns the attribute with the given name, if it exists.
			Raises KeyError otherwise. """
	def ListItems():
		""" Returns the list of attribute names. """

def IsA(obj, typ):
	"""	Returns true if the given object is of the given type. """
	return type(obj) == typ or isinstance(obj, typ) or isa(obj, typ)


##
## Invokable
##

from OPEP.AST import *
from OPEP.OPEP import *

class Invokable(object):
	""" An invokable expression. """
	
	@takes("Invokable", Parser, callable)
	def __init__(self, parser, function):
		""" An invokable expression consists of a callable function with
			a parser for validating its input. """
		self.parser = parser
		self.function = function

	@takes("Invokable", str)
	def parse(self, string):
		""" Parse an input string, returning the ParseResult and an Invocation.  """
		parsed = self.parser.parse(string)
		# arguments are values of ast nodes that were marked with [arg] decorator
		args = [arg.value for arg in parsed.ast.walkForAttribute('arg')]
		return parsed, Invocation(self.function, parsed.ast, args)
	
	@takes("Invokable", AST, list)
	def invocation(self, ast, args):
		""" Returns an invocation on the given arguments. """
		return Invocation(self.function, ast, args)
		
	def __call__(self, *args):
		self.function(*args)

class Invocation(object):
	""" A particular invocation an invokable expression. """
	
	@takes("Invocation",callable, AST, list)
	def __init__(self, function, ast, args):
		self.function = function
		self.ast = ast
		self.args = args
	
	def __call__(self):
		return self.function(self.ast, self.args)

	def __repr__(self):
		return "[%s(%s)]" % (self.function, self.args)

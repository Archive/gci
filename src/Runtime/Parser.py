# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
GCI Runtime Parser.  Defines the runtime grammar of GCI.

Used to parse and evaluate expressions in the GCI language.

(c) Daniel Ramage 2005
"""

from decorators import *
from OPEP.EBNF import *
from OPEP.AST import *
import schema

import Library
from Types import *

__verbose__ = False

@takes(schema.Context)
@memoized
def RuntimeTypeLibrary(context):
	""" Library of all OPEP.Grammar.Symbols defined by GCI and schemas. """
	library = dict(Library.GCITypeLibrary(context))
	
	library["@empty"] = RuleSymbol("@empty", [Rule([])])
	
	for schema in context.values():
		schema.UpdateSymbolLibrary(library)
	
	return library

# boundary symbols .. skip space and tab
BoundaryRequiredSymbol = RegexTokenSymbol('<TokenBoundary>', r'( |\t)+')
BoundaryOptionalSymbol = RegexTokenSymbol('<TokenBoundary>', r'( |\t)*')
BoundaryRequiredSymbol.getAST = BoundaryOptionalSymbol.getAST = lambda match: None

@singleton
def RuntimeDecorators():
	""" Decorators for runtime per-invokable grammars. """
	def arg(ast):
		""" Marks an AST as an argument. """
		for child in ast.children:
			child.arg = True
		return ast
	arg = RuleDecorator(transformAST=arg)

	def cmd(ast):
		""" Marks an AST as a command. """
		for child in ast.children:
			child.cmd = True
		return ast
	cmd = RuleDecorator(transformAST=cmd)
	
	library = {}
	library.update(EBNFDecorators(BoundaryRequiredSymbol, BoundaryOptionalSymbol))
	for k,v in locals().iteritems():
		if isinstance(v, RuleDecorator):
			library[k] = v
	return library


@takes(schema.Context)
@memoized
def RuntimeEBNFContext(context=None):
	""" Returns an EBNF context for parsing a runtime grammar. """
	_context = context
	if _context is None:
		_context = schema.Context()
	return EBNFContext(
		symbols=RuntimeTypeLibrary(_context),
		decorators=RuntimeDecorators,
		traverser=lambda:RuntimeTraverser(_context))

@takes(schema.Context)
@memoized
def RuntimeParser(context):
	"""	Returns a parser for validating GCI statements in a Context. """
	parsed = EBNFParser.parse(RuntimeGrammar, RuntimeEBNFContext(context))
	if __verbose__: print "RUTNIME:\n", parsed.ast.value
	return parsed.ast.value

@takes(str, schema.Context)
def CustomParser(grammar, context):
	"""	Returns a custom parser for the given grammar in the Runtime.Context
		determined by lookup from the given schema_context.. """
	parsed = EBNFParser.parse(grammar, RuntimeEBNFContext(context))
	if parsed.ast.value is None:
		raise SyntaxError, "Invalid grammar provided to CustomParser: %s" % list(parsed.ast.tags)
	if __verbose__: print "CUSTOM:\n", parsed.ast.value
	return parsed.ast.value

##
## GCI language.  References to @gci.* are defined in Runtime.Library.
##

RuntimeGrammar = r'''
@program := [BreaksForbidden](@newline* @statement*)
@statement := [BreaksForbidden]((@expression | @assignment) @newline)

@assignment := @variable "=" @expression
@expression := @invocation

@invocation := [BreaksRequired](@gci.value.invokable @argument*)
@argument   := @gci.literal.word | @gci.literal.string

#@value := @variable | @value.inlined | @value.typed
#@value.inlined := "{" @expression "}"

@variable := /\$[a-zA-Z_][a-zA-Z0-9_]*/
@newline := /( |\t)*(\n+|$)/
'''

class RuntimeTraverser(object):
	""" Traverser for validating and running GCI programs. """
	@takes("RuntimeTraverser", schema.Context)
	def __init__(self, context):
		self.context = context

	@TraversalStub("@statement", "@expression", "@argument")
	def pass_up(self, node):
		""" For statements that just pass up their child value. """
		node.value = node.children[0].value
	
	@TraversalStub("@program")
	def t_program(self, node):
		""" Value is a lambda that runs a set of statements. """
		statements = [statement.value for statement in node.children[1].children
		              if isinstance(statement.value, Invocation)
						 and statement.status is not Status.Reject]
		
		if len(statements):
			node.value = lambda: [statement() for statement in statements]

	@TraversalStub("@invocation")
	def t_invocation(self, node):
		""" Invoke an expression. """
		invokable = node.children[0].value
		if invokable is None: return
		
		if invokable.parser is None:
			# if invokable provides no parser, create the invocation ourselves
			node.value = invokable.invocation(node.children[1],
				[n.value for n in node.children[1].children])
		
		else:
			# invokable provides a parser
			def indexThroughWS(startpoint):
				while startpoint < len(node.match.context) and node.match.context[startpoint] in " \t":
					startpoint += 1
				return startpoint
			
			# 1. find the argument string within this node's match
			argsStart = indexThroughWS(node.children[0].match.end)
			argsEnd = indexThroughWS(node.match.end)

			# 2. parse the args string using the invokable's parser
			argsString = node.match.context[argsStart:argsEnd]
			parsed, invocation = invokable.parse(argsString)
			
			# 3. push generated tags and completers into this ast node
			for tag in parsed.ast.tags:
				tag.match = Match(node.match.context,
					tag.match.start+argsStart, tag.match.end+argsStart)
				node.addTag(tag)
			for completer in parsed.completers:
				node.addCompleter(Completer.OnNewMatch(completer, node.match, argsStart))
			
			# 4. return the invocation
			node.value = invocation

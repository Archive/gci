# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Unit tests for RuntimeParsers.

This should be run from the root test runner in src/testsuite.py

(c) Daniel Ramage 2005
"""

from OPEP.OPEP import *
import Parser
import schema

context = schema.Context()

def CustomParser(grammar):
	return Parser.CustomParser(grammar, context)

class RuntimeParserTest(ParserTestCase):
	def testBreaksRequiredDecoratorLiteral(self):
		self.parser = CustomParser(
			"@p := [BreaksRequired](@gci.literal.string @gci.literal.string?)")
		self.assertGudParse(r'"hi" "there"')
		self.assertBadParse(r'"hi""there"')
		
	def testBreaksRequiredDecoratorPlus(self):
		self.parser = CustomParser(
			"@p := [BreaksRequired](@gci.literal.string+)")
		self.assertGudParse(r'"hi" "there" "how"')
		self.assertBadParse(r'"hi""there" "how"')
		self.assertBadParse(r'"hi""there""how"')
		self.assertGudParse(r'"hi" "there " "how"')
		self.assertBadParse(r'"hi""there ""how"')
	
	def testBreaksRequiredDecoratorStar(self):
		self.parser = CustomParser(
			"@p := [BreaksRequired](@gci.literal.string*)")
		self.assertGudParse(r'"hi" "there" "how"')
		self.assertBadParse(r'"hi""there" "how"')
		self.assertBadParse(r'"hi""there""how"')
		self.assertGudParse(r'"hi" "there " "how"')
		self.assertBadParse(r'"hi""there ""how"')

class RuntimeLibraryTest(ParserTestCase):
	def testIntType(self):
		self.parser = CustomParser("@p := @gci.value.int")
		self.assertGudParse("1")
		self.assertBadParse("1 ")
		self.assertBadParse("1 1")
		self.assertGudParse("01240135")
		self.assertBadParse("035k")

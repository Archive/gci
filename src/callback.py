# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
A module for scheduling function calls and connecting function calls
to arbitrary "signals".

Note that some of these functions look a lot like the built in sched
class.  The distinction is that this class is completely *synchronous*.

(c)Daniel Ramage 2005
"""

import heapq
import time
import weakref

class callback:
	"""
	A no-argument-callable object that, when called, applies
	a function to an argument list.  Equality and hashcode are
	provided.
	"""
	def __init__(self, function, arguments):
		self.function = function
		self.arguments = arguments
	
	def __call__(self):
		return self.function(*self.arguments)

	def __eq__(self,other):
		return self.function == other.function and \
			self.arguments == other.arguments
	
	def __hash__(self):
		return hash(self.function)+hash(self.arguments)

# (ref(thing),signal) => [callback1,callback2,...]
connections = {}

def connect(thing, signal, callback):
	key = (weakref.ref(thing),signal)
	if key not in connections:
		connections[key] = []
	connections[key].append(callback)

def notify(thing, signal):
	key = (weakref.ref(thing),signal)
	if key in connections:
		for callback in connections[key]:
			callback()

def disconnect(thing, signal, callback):
	key = (weakref.ref(thing),signal)
	if callback not in connections[key]:
		raise "Cannot disconnect unconnected callback."
	connections[key].remove(callback)

idle = []
def register_idle(function):
	""" Registers the given function as an idle function, with arguments. """
	if function not in idle:
		idle.append(function)
def unregister_idle(function):
	""" Unregisters the previously registered idle function. """
	idle.remove(function)

def pull(enqueued_time, proposed_time):
	"""
	This CallbackQueue resolution policy says that we keep the
	currently enqueued time if it is SMALLER than the proposed time.
	"""
	return enqueued_time < proposed_time

def push(enqueued_time, proposed_time):
	"""
	This CallbackQueue resolution policy says that we keep the
	currently enqueued time if it is LARGER than the proposed time.
	"""
	return enqueued_time > proposed_time

class CallbackQueue:
	def __init__(self):
		self.callbacks = {}	# callback -> time
		self.queue = []		# [(time,callback)] sorted by time

	def enqueue(self, delay, callback, resolve=pull):
		"""
		Schedules callback() to be called within at most time seconds.
		resolve to resolve what happens if the callback is
		already in the queue.
		"""
		realtime = time.time() + delay

		# if the callback is found, use the resolution policy to
		# determine if we keep the newly proposed time.
		if callback in self.callbacks:
			if resolve(self.callbacks[callback], realtime):
				return
			else:
				self.queue.remove((self.callbacks[callback],callback))
				heapq.heapify(self.queue)
				del self.callbacks[callback]
		
		self.callbacks[callback] = realtime
		heapq.heappush(self.queue, (realtime,callback))
	
	def pending(self):
		"""
		Returns the number of pending events in the queue.
		"""
		return len(self.queue)
		
	def poll(self):
		"""
		Polls all callbacks, calling ones that are expired.
		"""
		now = time.time()
		while self.pending() and self.queue[0][0] < now:
			callback = heapq.heappop(self.queue)[1]
			callback()
			del self.callbacks[callback]

__queue = CallbackQueue()		

within = __queue.enqueue
pending = __queue.pending

def poll():
	""" Polls the main queue and calls are registered idle functions. """
	__queue.poll()
	for function in idle:
		function()

if __name__ == "__main__":
	import unittest

	def printTime(arg):
		print arg, "says the time is", time.time()

	class ConnectTest(unittest.TestCase):
		def testConect(self):
			connect(self, "signal1", callback(printTime,("Me",)))
			connect(self, "signal1", callback(printTime,("You",)))
			connect(poll, "signal2", callback(printTime,("Him",)))
			print "A, Signal1:"
			notify(self, "signal1")
			print "A, Signal2:"
			notify(self, "signal2")
			print "B, Signal1:"
			notify(poll, "signal1")
			print "B, Signal2:"
			notify(poll, "signal2")
	
	class CallbackQueueTest(unittest.TestCase):
		def BLARGHtestQueue(self):
			printTime("Main")
			within(4, callback(printTime,("Joe",)), resolve=push)
			within(1, callback(printTime,("Bob",)))
			within(3, callback(printTime,("Bob",)))
			within(2, callback(printTime,("Joe",)), resolve=push)
			printTime("Main")
	
			while pending():
				poll()
	
	unittest.main()

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
One-Pass Earley Parser

Flexible single-stage parsing algorithm - tokenization and parsing are combined
into a single-pass-of-fun.

(c) Daniel Ramage 2005
"""

from decorators import *
from AST import *
from Grammar import *
from Traverser import *

# set to True for outputing parser trace
__verbose__ = False

class EOFSymbol(TerminalSymbol):
	"""
	Special symbol for matching the end of string.
	"""
	def __init__(self):
		TerminalSymbol.__init__(self, "EOF")
	
	@takes("EOFSymbol", str, int)
	@returns(Match)
	def match(self, string, offset):
		if offset == len(string):
			return Match(string, offset, offset)

class State(object):
	"""
	A State is a state in the Earley Parser, consisting of a Rule,
	a position within that rule (representing the location of the dot)
	and a start to end index for where in the input string that State applies.
	This State is augmented to carry an AST representing the partial parse
	tree.
	"""
	@takes("State", Rule, int, int, int, AST)
	def __init__(self, rule, index, start, end, ast=None):
		assert index <= len(rule.symbols)
		self.rule = rule
		self.index = index
		self.start = start
		self.end = end
		self.ast = ast
		
		##
		## state information
		##
		
		# true if this state is pointing past the last rule
		self.isFinal = (self.index == len(self.rule.symbols))

		# the next symbol otherwise
		self.nextSymbol = (not self.isFinal) and self.rule.symbols[self.index]

	def __str__(self):
		values = [str(s) for s in self.rule.symbols]
		values.insert(self.index, "*")
		return "[%s]@(%s,%s)" % (" ".join(values), self.start, self.end)

	def __eq__(self, other):
		""" Observational equality. """
		return self.rule is other.rule and \
			self.index == other.index and \
			self.start == other.start and \
			self.end == other.end and \
			self.ast == other.ast
	
	def __hash__(self):
		""" Equality-consistent hashcode. """
		hashcode = hash(self.rule)+13*self.index-23*self.start+11*self.end
		
		# memoize for later call
		self.__hash__ = lambda: hashcode
		
		return hashcode


##
## One-Pass Earley Parser
##

class Parser(object):
	"""
	The One-Pass Earley Parser.  Finds all Symbols referenced from a single
	start Symbol to determine the rule list.
	"""

	@takes("Parser", RuleSymbol, [TerminalSymbol], callable)
	def __init__(self, start, skiplist, traverser=None):
		assert start is not None, "No value provided for start node"
	
		# special end of input symbol
		self._EOF = EOFSymbol()

		# provided start rule
		self.start = start
		
		# special start state
		self._START = Rule([start, self._EOF])
		
		# map from Rule to RuleSymbol referencing it
		self.symbols = {self._START: None}
		
		# list of TerminalSymbols to ignore in input
		self.skiplist = list(skiplist)
		
		# rules seen by recursiveRuleFind
		seen = set()
		
		@takes(RuleSymbol)
		def recursiveRuleFind(symbol):
			""" DFS on symbol tree mapping each rule to the unique symbol that
				generates it. """
			seen.add(symbol)
			
			for rule in symbol.rules:
				if rule in self.symbols and self.symbols[rule] != symbol:
					raise "Rule %s is part of symbols %s and %s" \
						% (rule, symbol, self.symbols[rule])
				else:
					self.symbols[rule] = symbol
				
				for s in rule.symbols:
					if isinstance(s, RuleSymbol) and s not in seen:
						recursiveRuleFind(s)
		recursiveRuleFind(start)
		
		##
		## initialize traversal / validation
		##
		
		# constructor creating traversers to walk the AST
		self.traverser = traverser
		
		# default comparator for disambiguating between possible ASTs
		def comparator(a, b):
			""" Sorts ASTs according to their tags. """
			# take the one with better status
			statcmp = cmp(a.status,b.status)
			if statcmp != 0: return statcmp
			
			# same status, compare tags directly
			atags = list(a.tags)
			btags = list(b.tags)
			
			# cancel all atags that have a corresponding btag with better status
			# and vice verse (hasTrumpTag utility function defined at bottom
			# before unit tests).
			#
			# NB: this is an O(n^2) operation in the length of the tagset.  I'm
			# not sure how to get around that ...
			atags2 = [tag for tag in atags if not hasTrumpTag(btags, tag)]
			btags2 = [tag for tag in btags if not hasTrumpTag(atags, tag)]
			
			# compare by number of tags cancelled, taking the one with fewer
			lencmp = cmp(len(atags) - len(atags2), len(btags) - len(btags2))
			if lencmp != 0: return lencmp

			# try sorting the tags and directly comparing
			atags2.sort()
			btags2.sort()

			# compare sorted lists, breaking ties by id
			return cmp(atags2, btags2) or cmp(id(a),id(b))
		self.comparator = comparator
	

	@returns("ParseResult")	
	def parse(self, text, *traverserArgs):
		"""	Parses the given input string into an AST.
			
			If traverser was given to the constructor, it is used to
			construct a traverser instance (with the given traverserArgs),
			which is used to traverse all ASTs returned by the parsing engine.
			
			For ambiguous parses (where the engine returns more than one AST),
			the ASTs are sorted post-traversal by self.comparator and the
			first one is returned.
			
			Returns tuple of (completers, ast).
			"""
		
		completers, options = ParseInstance(self, text).parse()
		
		if len(options) == 0:
			ast = AST("InvalidParse", None, Match(text))
			ast.addTag(Tag(Status.Reject, "Unrecognizable input"))
			return ParseResult(completers, ast)
		
		def traversed(ast):
			# returns a traversed version of a deep copy of the given ast
			if self.traverser is not None:
				ast = AST.DeepCopy(ast)
				traverse(self.traverser(*traverserArgs), ast)
			return ast
		
		walked = [traversed(ast) for ast in options]
		walked.sort(self.comparator)
		
		return ParseResult(completers, walked[0])

	@takes("Parser")
	def __str__(self):
		""" Outputs the parser in EBNF. """
		symbols = []
		def findSymbols(symbol):
			if symbol in symbols: return
			if isinstance(symbol, RuleSymbol):
				symbols.append(symbol)
				for rule in symbol.rules:
					for symbol in rule.symbols:
						findSymbols(symbol)
		findSymbols(self.start)
		return "\n".join([s.toEBNF() for s in symbols])
	
	def __repr__(self):
		return str(self)

class ParseResult(object):
	"""	The result of a parse as returned by Parser.parse """
	@takes("ParseResult", [Completer], AST)
	def __init__(self, completers, ast):
		self._completers = completers
		self.ast = ast

	@takes("ParseResult", int)
	def completions(self, pos):
		"""	Returns the list of completions at the given position. """
		rv = set()
		for completer in self.completers:
			if pos >= completer.match.start and pos <= completer.match.end:
				for completion in completer.completions(pos):
					rv.add(completion)
		## TODO: group by completer type
		return sorted(list(rv))
	
	@property
	def completers():
		def fget(self):
			for completer in self._completers:
				yield completer
			for completer in self.ast.completers:
				yield completer
	
	@property
	def value():
		def fget(self):
			return self.ast.value
	
	@property
	def status():
		def fget(self):
			return self.ast.status

class ParseInstance(object):
	"""
	The ongoing parse state of a particular input string, as requested
	by Parser.parse.  This split is for thread-safety.
	"""

	@takes("ParseInstance", Parser, str)
	def __init__(self, parser, text):
		# parser configuration we use for parsing
		self._parser = parser

		# rule -> symbol mapping from parser
		self._symbols = parser.symbols
		
		# input string we parse
		self._input = text
		
		# the chart of parse States
		self._chart = [[] for i in xrange(len(self._input)+1)]
		self._sets = [set() for i in xrange(len(self._input)+1)]
		
		# the completers for this input sequence
		self._completers = []
		
		# begin!
		self.enqueue(State(parser._START,0,0,0,AST('_START')))

	@takes("ParseInstance", State)
	def enqueue(self, state):
		""" Enqueues the given state in the chart at its end index. """
		if state not in self._sets[state.end]:
			self._sets[state.end].add(state)
			self._chart[state.end].append(state)

	@takes("ParseInstance", State)
	def parents(self, state):
		""" Generator for retrieving all parents of the given state. """
		for s in self._chart[state.start]:
			if s.isFinal:
				continue
			elif s.nextSymbol is self._symbols[state.rule]:
				yield s
	
	@takes("ParseInstance")
	def parse(self):
		for i in xrange(len(self._input)+1):
			# for each iteration, this is a list of completed empty
			# production symbols
			self._empties = set()

			# if skip terminals match, move all states forward
			self.punt(i)

			# for pretty state printing
			if __verbose__:
				seen = 0
				if i < len(self._input):
					char = self._input[i]
				else:
					char = "<EOF>"
		
			# core of the Earley algorithm: iterate over states
			# in the current chart.  Complete, Predict, Scan.
			for state in self._chart[i]:
				if __verbose__:
					seen += 1
					print "@ %s %s %s (%s/%s)" % \
						(i,char,state,seen,len(self._chart[i]))
				
				# Complete: R -> a .
				if state.isFinal:
					# final => complete
					self.completer(state)
				
				# Scan: R -> a . b
				elif isinstance(state.nextSymbol, TerminalSymbol):
					# non-final terminal => scan
					self.scanner(state)
				
				# Predict: R -> a . N
				else:
					# non-final non-terminal => predict
					assert isinstance(state.nextSymbol, RuleSymbol)
					self.predictor(state)
		
		# generated AST from all end states in the final chart
		asts = [state.ast.children[0] for state in self._chart[-1]
		        if state.rule is self._parser._START and state.isFinal]

		return self._completers, asts
			
	
	def punt(self, index):
		"""
		Ignore some input sequences in the skip list provided to Parser.
		This is accomplished by copying every current State to the StateSet
		starting at the end of the ignored match.
		"""
		# to keep track of which indeces have already received the punt
		done = set()
			
		for skip in self._parser.skiplist:
			match = skip.match(self._input, index)
			if match is not None and match.end not in done:
				done.add(match.end)
				if __verbose__:
					print "@ %s PUNT -> %s : %s states" % \
						(index, match.end, len(self._chart[index]))
				
				for state in self._chart[index]:
					self.enqueue(State(state.rule, state.index, \
							           state.start, match.end, state.ast))
	
	@takes("ParseInstance", State)
	def completer(self, state):
		"""
		Complete: R -> a .
		
		Increment all parent states' within-rule index.
		"""
		## If completed rule is an empty production, then the parent states
		## that need to be notified may actually be later states in this
		## chart, so we enqueue the completion for the predictor stage.
		##
		## Note that the completion queue should NOT already have
		## symbols[state.rule] in it.  This would mean we have two empty
		## productions under the same name (!!)
		if state.rule.isEmpty:
			self._empties.add(self._symbols[state.rule])

		# give the rule a chance to transform the AST
		ast = state.rule.transformAST(state.ast)
		
		# do not enqueue if the rule rejects the (transformed) AST
		if not state.rule.acceptsAST(ast):
			return
		assert isinstance(ast, AST) or ast is None, \
			"transformAST %s in module %s did not return an AST" \
				% (state.rule.transformAST, state.rule.transformAST.__module__)

		# register the rule's completer in the AST itself
		if state.rule.hasCustomASTCompleter:
			ast.addCompleter(state.rule.getASTCompleter(ast))
		
		if __verbose__:
			print "  COMP: %s" % state
		for parent in self.parents(state):
			self.enqueue(State(parent.rule, parent.index+1,
			                   parent.start, state.end,
			                   AST.Add(parent.ast, ast)))
	
	@takes("ParseInstance", State)
	def scanner(self, state):
		"""
		Scan:  R -> a . b
		
		Increment the state within-rule index if it matches the next part
		of the input.
		"""
		assert isinstance(state.nextSymbol, TerminalSymbol)
		b = state.nextSymbol
		
		match = b.match(self._input, state.end)
		if match is not None:
			# the terminal matched something, so:
			
			# 1. register the match's completer
			completer = b.getMatchCompleter(match)
			if completer is not None:
				self._completers.append(completer)
		
			# 2. get its AST and increment current state
			if b.acceptsMatch(match):
				ast = AST.Add(state.ast, b.getAST(match))
				nextState = State(state.rule, state.index+1,
				                  state.start, match.end, ast)
			
				if __verbose__:
					print "  SCAN: %s @ %s from '%s'" % \
						(nextState, match.end, match.string)
		
				self.enqueue(nextState)
	
	@takes("ParseInstance", State)
	def predictor(self, state):
		"""
		Predict:  R -> a . N

		Add a new state S for every rule N -> **
		"""
		assert isinstance(state.nextSymbol, RuleSymbol)
		N = state.nextSymbol
		
		## N may have been an empty production: if so, we will find it in
		## _empties.  We can now complete ourself (we were waiting on a
		## [now completed] empty production).
		if N in self._empties:
			if __verbose__:
				print "  COMP (EMPTY) %s" % state
			
			# we can add on an empty AST here because it is an empty production
			self.enqueue(State(state.rule, state.index+1,
			                   state.start, state.end,
			                   AST.Add(state.ast, AST(N.name))))

		if __verbose__: size = len(self._chart[state.end])
		
		for rule in N.rules:
			ast = AST(N.name)

			# 1. register the rule's completer
			if rule.hasCustomASTCompleter:
				ast.match = Match(self._input, state.end, state.end)
				self._completers.append(rule.getASTCompleter(ast))

			# 2. enqueue a new predictor state		
			self.enqueue(State(rule, 0, state.end, state.end, ast))
		
		if __verbose__:
			print "  PRED %s states: %s states added" % \
				(len(state.nextSymbol.rules), len(self._chart[state.end])-size)

##
## utility functions
##

def hasTrumpTag(tags, reference):
	"""	Returns true if a tag exists in tags that marks the
		same region as reference but with a better status. """
	for tag in tags:
		if   tag.match.start >= reference.match.start \
		 and tag.match.end <= reference.match.end \
		 and tag.status <= reference.status:
		 	return True
	return False


##
## Unit tests
##

import unittest

class ParserTestCase(unittest.TestCase):
	def assertBadParse(self, line):
		'''
		Asserts that the given line IS NOT a valid parse.
		'''
		result = self.parser.parse(line)
		self.assertEquals(result.status, Status.Reject)
		return result

	def assertGudParse(self, line):
		'''
		Asserts that the given line IS a valid parse.
		'''
		result = self.parser.parse(line)
		self.assertNotEquals(result.status, Status.Reject)
		return result

class BasicParserTest(ParserTestCase):
	def setUp(self):
		pass
		
	def testBasic(self):
		""" Single matched pair of '()' """
		tLPAREN = LiteralTokenSymbol('tLPAREN','(')
		tRPAREN = LiteralTokenSymbol('tRPAREN',')')
		sMATCHED = RuleSymbol('MATCHED',	[Rule([tLPAREN, tRPAREN])])
			
		self.parser = Parser(sMATCHED,[])
		self.assertGudParse("()")
		self.assertBadParse(" ()")
		self.assertBadParse("( )")
		self.assertBadParse("())")
		self.assertBadParse("(())")
		self.assertBadParse("(")
		
	def testEmbed(self):
		""" () with center embedding """
		ns = {}
		tLPAREN = LiteralTokenSymbol('tLPAREN','(')
		tRPAREN = LiteralTokenSymbol('tRPAREN',')')
		sMATCHED = RuleSymbol('MATCHED',	[
			Rule([tLPAREN, tRPAREN]),
			Rule([tLPAREN, SymbolReference("sMATCHED",ns), tRPAREN])
		])
		ns["sMATCHED"] = sMATCHED
		
		self.parser = Parser(sMATCHED,[])
		self.assertGudParse("()")
		self.assertGudParse("(())")
		self.assertBadParse("(()())")
		self.assertGudParse("((()))")
		self.assertGudParse("((((()))))")
		self.assertBadParse("((())")
		
	def testNumbers(self):
		""" Simple grammar for recognizing numbers """
		ns = {}
		tDIGITS = RegexTokenSymbol('tDIGITS',r'\d+')
		tMINUS = LiteralTokenSymbol('tMINUS','-')
		tDOT = LiteralTokenSymbol('tDOT','.')
		rNUMBER = RuleSymbol('NUMBER', [
			Rule([tMINUS, tDIGITS]),
			Rule([tMINUS, tDIGITS, tDOT]),
			Rule([tMINUS, tDOT, tDIGITS]),
			Rule([tMINUS, tDIGITS, tDOT, tDIGITS]),
			Rule([tDIGITS]),
			Rule([tDIGITS, tDOT]),
			Rule([tDOT, tDIGITS]),
			Rule([tDIGITS, tDOT, tDIGITS])
		])
			
		self.parser = Parser(rNUMBER,[])
		self.assertGudParse('1.2')
		self.assertGudParse('111')
		self.assertGudParse('-.4')
		self.assertBadParse('-.4.')
		self.assertGudParse('-4333.')
		self.assertBadParse('-')
		self.assertBadParse('-.')
		self.assertBadParse('9-')

	def testEmpty(self):
		""" Empty productions test """
		tLPAREN = LiteralTokenSymbol('tLPAREN','(')
		tRPAREN = LiteralTokenSymbol('tRPAREN',')')
		sMATCHED = RuleSymbol('MATCHED',	[
			Rule([tLPAREN, RuleSymbol('EMPTY',[Rule([])]), tRPAREN])
		])
		
		self.parser = Parser(sMATCHED,[])
		self.assertGudParse("()")
		self.assertBadParse(" ()")
		self.assertBadParse("( )")
		self.assertBadParse("())")
		self.assertBadParse("(())")
		self.assertBadParse("(")
		
	def testIgnored(self):
		""" White space skipping test """
		tLPAREN = LiteralTokenSymbol('tLPAREN','(')
		tRPAREN = LiteralTokenSymbol('tRPAREN',')')
		tWS = RegexTokenSymbol('tWS',r'\s+')
		sMATCHED = RuleSymbol('MATCHED',	[Rule([tLPAREN, tRPAREN])])
		
		self.parser = Parser(sMATCHED,[tWS])
		self.assertGudParse("()")
		self.assertGudParse(" ()")
		self.assertGudParse("( )")
		self.assertBadParse("())")
		self.assertBadParse("(())")
		self.assertBadParse("(")
		
if __name__ == "__main__":
	unittest.main()

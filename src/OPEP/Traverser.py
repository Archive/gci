# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
AST Traversal and disambiguation/Validation utilities.

(c) Daniel Ramage 2005
"""

from decorators import *
from AST import *

__verbose__ = False

def TraversalStub(*rulenames):
	"""	Decorator for marking a function as a TraversalStub for a given rule.
	
		A TraversalStub is a function that will be called by traverse() when
		it encounters a node whose type is named in rulenames.  The function
		should take a single AST argument representing the partial tree to
		traverse.  The type of that ast will be found in rulenames.  The stub's
		return value is ignored - it should mutate ast (by changing ast.value
		or adding tags) to communicate with other traveral stubs. """
	def decorated(function):
		if not hasattr(function, "TraversalTypes"):
			function.TraversalTypes = []
		function.TraversalTypes.extend(rulenames)
		return function
	return decorated

def traverse(traverser, ast):
	"""	Walks an AST (postorder) calling TraversalStubs specified in the given
		traverser instance. """
	# initialize TraversalStubs dictionary in the class instance if necessary
	if not hasattr(traverser, "TraversalStubs"):
		traverser.TraversalStubs = {}
		for name in dir(traverser):
			function = getattr(traverser, name)
			if hasattr(function, "TraversalTypes"):
				for rulename in function.TraversalTypes:
					if rulename in traverser.TraversalStubs:
						raise NameError, "More than one function is marked as a traverser for '%s' in %s"  % (rulename, traverser)
					traverser.TraversalStubs[rulename] = function
	
	# recursive postorder ast walk
	def walk(node):
		for kid in node.children:
			walk(kid)

		if node.type in traverser.TraversalStubs:
			traverser.TraversalStubs[node.type](node)
	
	if __verbose__: print "## PRE-WALK:\n%s" % ast
	walk(ast)
	if __verbose__: print "## WALKED:\n%s" % ast

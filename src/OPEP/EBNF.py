# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
EBNF Grammar Interpreter.

This file uses a custom OPEP parser to parse a grammar description in EBNF,
returning an OPEP parser for that grammar.

EBNF refers to Extended Backus-Naur Form, which is a more intuitive
way to specify a grammar than standard BNF.  The additions are qualifiers
on symbols and or's in a single rule's symbol list.  EBNF grammars tend
to be a fraction of the size of BNF grammars.

Most other parsing toolkits do not support BNF form because they do not
use a parser that can gracefully handle the (possibly ambiguous) BNF
equivalent of an EBNF specification.  Earley parsers like the one in
this package have no trouble with ambiguity.

(c) Daniel Ramage 2005
"""

from AST import *
from OPEP import *
from Traverser import *
from Grammar import *
from ThunkGenerator import *

import itertools

class EBNFParser(Parser):
	""" Parser for dramage-extended EBNF.  Returns an AST representing
		the parse of a given EBNF grammar.  If the parse is successful,
		ast.value of the returned AST will contain an OPEP.Parser instance
		for parsing according to the grammar. """
	
	@takes("EBNFParser")
	def __init__(self):
		tokens = [
			RegexTokenSymbol('tNAME',
				'\\@([a-zA-Z_][a-zA-Z_0-9]*)(\\.([a-zA-Z_][a-zA-Z_0-9]*))*'),
			RegexTokenSymbol('tLITERAL',
				'[a-zA-Z_][a-zA-Z_0-9]*'),
			LiteralTokenSymbol('tBIND', ':='),
			LiteralTokenSymbol('tAPPEND', '|='),
			LiteralTokenSymbol('tPIPE', '|'),
			LiteralTokenSymbol('tLPAREN', '('),
			LiteralTokenSymbol('tRPAREN', ')'),
			LiteralTokenSymbol('tLSQUARE', '['),
			LiteralTokenSymbol('tRSQUARE', ']'),
			LiteralTokenSymbol('tLBRACE', '{'),
			LiteralTokenSymbol('tRBRACE', '}'),
			RegexTokenSymbol('tNONBRACE', '[^\{|\}]*'),
			LiteralTokenSymbol('tPLUS', '+'),
			LiteralTokenSymbol('tQMARK', '?'),
			LiteralTokenSymbol('tSTAR', '*'),
			RegexTokenSymbol('tNEWLINE', "(\\r|\\n)+"),
			LiteralTokenSymbol('tDOT', '.'),
			LiteralTokenSymbol('tTWIDDLE', '~'),
			LiteralTokenSymbol('tCOLON', ':'),
			SQuoteStringTokenSymbol,
			DQuoteStringTokenSymbol,
			RegexStringTokenSymbol ]

		skiplist = [
			RegexTokenSymbol('SPACE', r'\s+'),
			RegexTokenSymbol('tCOMMENT', "\#(.*?)(\n|$)") ]
		
		# seed dictionary with tokens
		ns = {}
		for symbol in tokens:
			ns[symbol.name] = symbol

		# ebnf grammar grammar
		SemanticRule('grammar', ns,
			'rule_list')
		SemanticRule('rule_list', ns,
			'newline rule_list rule',
			'newline rule')
		SemanticRule('rule', ns,
			'tNAME bind alternatives_list action_list newline',
			'tNAME bind newline')
		SemanticRule('alternatives_list', ns,
			'alternatives_list tPIPE symbol_list',
			'symbol_list')
		SemanticRule('symbol_list', ns,
			'symbol_list boundary qualified_symbol',
			'qualified_symbol')
		SemanticRule('qualified_symbol', ns,
			'decorators symbol qualifier')
		SemanticRule('action_list', ns,
			'action_list action', '')
		SemanticRule('action', ns,
			'tLBRACE tLITERAL arguments tCOLON code tRBRACE')
		SemanticRule('code', ns,
			'tNONBRACE', 'tNONBRACE tLBRACE code tRBRACE tNONBRACE')
		SemanticRule('arguments', ns,
			'tLPAREN arg_list tRPAREN', '')
		SemanticRule('arg_list', ns,
			'arg_list arg', '')
		SemanticRule('arg', ns,
			'SQuoteString', 'DQuoteString')
		
		@SemanticRule('decorators', ns,
			'decorators decorator',	'decorator', '')
		def decorators():
			def transformAST(node):
				""" Collapse grandchildren of decorators in parse tree. """
				children = []
				if len(node.children) == 2:
					children.extend(node.children[0].children)
					children.append(node.children[1])
				elif len(node.children) == 1:
					children.append(node.children[0])
				node._children = children
				return node
		
		SemanticRule('decorator', ns,
			'tLSQUARE tLITERAL tRSQUARE')
		SemanticRule('symbol', ns,
			'tLPAREN alternatives_list tRPAREN',
			'tNAME',
			'tLITERAL',
			'SQuoteString',
			'DQuoteString',
			'RegexString')
		SemanticRule('qualifier', ns,
			'tQMARK', 'tPLUS', 'tSTAR', '')
		SemanticRule('boundary', ns,
			'tDOT', 'tTWIDDLE', '')
		SemanticRule('bind', ns,
			'tBIND', 'tAPPEND')
		
		@SemanticRule('newline', ns,
			'tNEWLINE', '')
		def newline():
			def transformAST(node):
				""" Ignore newlines in the parse tree. """
				return None
		
		Parser.__init__(self, ns['grammar'], skiplist, EBNFTraverser)

	@takes("EBNFParser", str, "EBNFContext")
	def parse(self, grammar, context=None):
		""" Returns a Parser associated with the given grammar in ENBF.
			Retrieves the token skiplist and traverser for the generated parser
			from the given EBNFContext. """
		if context is None: context = EBNFContext()
		return Parser.parse(self, grammar, context)

class Bundle(object):
	"""
	Represents the set of Rules that will eventually be grouped into a
	RuleSymbol.
	"""
	@takes("Bundle", Symbol)
	def __init__(self, symbol=None):
		""" Single symbol for leaves. """
		self._lists = [[symbol]]

	@takes("Bundle", str)
	@returns(RuleSymbol)
	def toRuleSymbol(self, name):
		""" Return a named RuleSymbol corresponding to the contents of this Bundle. """
		return RuleSymbol(name, [Rule(symbols) for symbols in self._lists])

	@takes("Bundle")
	def isSingleton(self):
		""" Returns true if the bundle wraps only a single symbol. """
		return len(self._lists) == 1 and len(self._lists[0]) == 1

	@property
	def singleton():
		""" Returns the single symbol wrapped by the list, if isSingleton() """
		def fget(self):
			assert self.isSingleton()
			return self._lists[0][0]

	@staticmethod
	@takes("Bundle")
	@returns("Bundle")
	def Clone(bundle):
		""" Returns a cloned copy of the given bundle. """
		new = Bundle()
		new._lists = [[symbol for symbol in options] for options in bundle._lists]
		return new

	@staticmethod
	@takes([[Symbol]])
	@returns("Bundle")
	def Literal(lists):
		new = Bundle()
		new._lists = lists
		return new

	@staticmethod
	@takes("Bundle","Bundle")
	@returns("Bundle")	
	def Concatenate(a, b):
		"""
		Returns a new Bundle, which consists of all possible productions
		in A followed by all possible productions in B. (product).
		"""

		if len(b._lists) == 0:
			return Bundle.Clone(a)
		if len(a._lists) == 0:
			return Bundle.Clone(b)

		lists = []
		for aList in a._lists:
			for bList in b._lists:
				joined = []
				joined.extend(aList)
				joined.extend(bList)
				lists.append(joined)
		
		bundle = Bundle()
		bundle._lists = lists
		return bundle

	@staticmethod
	@takes("Bundle", "Bundle")
	@returns("Bundle")
	def Union(a, b):
		""" Returns a new Bundle, which consists of any rule from A or from B. """
		lists = []
		lists.extend(a._lists)
		lists.extend(b._lists)
		
		bundle = Bundle()
		bundle._lists = lists
		return bundle

	@staticmethod
	@takes("Bundle", str)
	@returns("Bundle")
	def Qualify(bundle, qualifier, counter=itertools.count()):
		""" Returns a new Bundle which represents qualifying the existing
			bundle with one of 'tPLUS','tSTAR','tQMARK'. """
		if not qualifier:
			return bundle
			
		assert qualifier in ('tPLUS', 'tSTAR', 'tQMARK')
		
		# the private dictionary we will use for lazy qRules
		dictionary = {}

		# the rule representing the current bundle
		vRule = bundle.toRuleSymbol("<helper:%s>" % counter.next())

		# q* = the rule representing the qualified bundle.
		# we use LazyRules because they are self-referential (so
		# they depend on the RuleSymbol that can't be made
		# until the Rules are done.)
		qName = "<qualified:%s>" % counter.next()
		qRuleReference = SymbolReference(qName,dictionary)
		if qualifier == "tSTAR":
			# star goes to qRule vRule or nothing
			qRule = RuleSymbol(qName,
				[Rule([qRuleReference, vRule]),
				 Rule([vRule]),
				 Rule([ ]) ])
		elif qualifier == "tPLUS":
			# plus goes to qRule vRule or vRule
			qRule = RuleSymbol(qName,
				[Rule([qRuleReference, vRule]),
				 Rule([vRule]) ])
		elif qualifier == "tQMARK":
			# qmark goes to vRule or nothing
			qRule = RuleSymbol(qName,
				[Rule([vRule]),
				 Rule([ ]) ])

		# add the qRule to the namespace so the self-references can work
		dictionary[qName] = qRule

		# transformAST that marks the ast as transient (to be inlined when added
		# as a child)
		def markTransient(ast):
			ast.transient = True
			return ast

		# set transformAST to grandkids for all rules we've defined			
		for rule in itertools.chain(qRule.rules,vRule.rules):
			rule.transformAST = markTransient

		# bundle rule for putting it all together (required placeholder
		# so that parent rules can rely on positions of children even
		# if the bundle produces more or less than 1 production
		bRule = RuleSymbol("*bundle:%s" % counter.next(), [Rule([qRule])])

		# return a bundle that refers to the new bundled rule
		return Bundle(bRule)

@memoized
@takes(TerminalSymbol, TerminalSymbol)
@returns(dict)
def EBNFDecorators(boundary_required_symbol=None, boundary_optional_symbol=None):
	"""	Returns the library of default decorators for parsing EBNF. """
	boundary_required_symbol = boundary_required_symbol \
		or RegexTokenSymbol('BoundaryRequired','( |\t)+')
	boundary_optional_symbol = boundary_optional_symbol \
		or RegexTokenSymbol('BoundaryOptional','( |\t)*')
	
	def do_breaks_required(rule, counter=itertools.count()):
		"""	Specifies that boundaries are required between adjacent
			symbols in this rule. """
		if hasattr(rule, 'BoundaryPolicy'):
			return

		def deferredTransform():
			""" Save the rule transformation until the EBNF traversal postwalk,
				because we might not be ready to test if unresolved
				SymbolReferences are possibly empty. """
			_empties = set()
			if len(rule.symbols) > 1:
				next_symbol = rule.symbols[0]
				empty_symbol = EmptyProductionSymbol(next_symbol)
				if empty_symbol is not None:
					_empties.add(empty_symbol)
					bundle = Bundle.Literal([[next_symbol], [empty_symbol]])
				else:
					bundle = Bundle(next_symbol)
				
				for next_symbol in rule.symbols[1:]:
					empty_symbol = EmptyProductionSymbol(next_symbol)
					if empty_symbol is not None:
						_empties.add(empty_symbol)
						
						empties_bundle = Bundle.Clone(bundle)
						for l in empties_bundle._lists:
							l.append(empty_symbol)
						for l in bundle._lists:
							if len(l) and l[-1] not in _empties:
								l.append(boundary_required_symbol)
							l.append(next_symbol)
						bundle = Bundle.Union(bundle, empties_bundle)
					else:
						for l in bundle._lists:
							if len(l) and l[-1] not in _empties:
								l.append(boundary_required_symbol)
							l.append(next_symbol)
				
				if len(bundle._lists) == 1:
					# only one symbol list in bundle, take it directly
					rule.symbols = bundle._lists[0]
				else:
					def transientTransform(ast):
						ast.transient = True
						return ast
				
					symbol = bundle.toRuleSymbol('<BoundaryRequiredHelper:%s>' \
						% counter.next())
					symbol.setTransformAST(transientTransform)
					rule.symbols = [symbol]
			rule.BoundaryPolicy = 'REQUIRED'
		
		# save the transform for later
		rule.BoundaryPolicy = deferredTransform

		# recurse
		for symbol in rule._symbols:
			if isinstance(symbol,RuleSymbol):
				for subrule in symbol.rules:
					do_breaks_required(subrule)
		
	BreaksRequired = RuleDecorator(decorateRule=do_breaks_required)
	
	def do_breaks_optional(rule):
		""" Specifies that boundaries are optional between adjacent
			symbols in this rule. """
		if hasattr(rule, 'BoundaryPolicy'):
			return
		
		def deferredTransform():
			if len(rule._symbols):
				new_symbols = [rule._symbols[0]]
				for next_symbol in rule._symbols[1:]:
					new_symbols.append(boundary_optional_symbol)
					new_symbols.append(next_symbol)
				rule._symbols = new_symbols
				rule._resolved = None
			rule.BoundaryPolicy = 'OPTIONAL'
		
		# save the transform for later
		rule.BoundaryPolicy = deferredTransform
		
		# recurse
		for symbol in rule._symbols:
			if isinstance(symbol, RuleSymbol):
				for subrule in symbol.rules:
					do_breaks_optional(subrule)
		
	BreaksOptional = RuleDecorator(decorateRule=do_breaks_optional)

	def do_breaks_forbidden(rule):
		""" Specifies that boundaries are forbidden between adjacent
			symbols in this rule. """
		if hasattr(rule, 'BoundaryPolicy'):
			return
			
		def deferredTransform():
			rule.BoundaryPolicy = 'FORBIDDEN'
		
		# saave the transform for later
		rule.BoundaryPolicy = deferredTransform
		
	BreaksForbidden = RuleDecorator(decorateRule=do_breaks_forbidden)
	
	library = {}
	for k,v in locals().iteritems():
		if isinstance(v, RuleDecorator):
			library[k] = v
	return library

class EBNFContext(object):
	"""	Context for an EBNF grammar traversal.
	
		Contains the following properties:
	
		symbols: {name:Symbol} for auxiliary symbol lookup
		decorators: {name:RuleDecorator} for decorator lookup
		skiplist: [TerminalSymbol] tokens to skip in generated parser
		          (defaults to whitespace)
		traverser: traverser to use for generated parser
	"""
	
	# @takes("EBNFContext", dict, dict, callable, [TerminalSymbol])
	def __init__(self, symbols=None, decorators=None, traverser=None, skiplist=[]):
		self.symbols = symbols or {}
		self.decorators = decorators
		if self.decorators is None: self.decorators = EBNFDecorators()
		self.skiplist = skiplist
		self.traverser = traverser

class EBNFTraverser(object):
	"""	Traverser for walking the EBNF tree to generate an OPEP parser. """
	@takes("EBNFTraverser", EBNFContext)
	def __init__(self, context=None):
		# dictionary for symbol definitions
		self.dictionary = {}
		
		# parser context
		self.context = context or EBNFContext()
		
		# incrementor for name references
		self._uid = 0

	def hasSymbol(self, name):
		"""	True if a symbol exists with the given in name in dictionary
			or context.symbols. """
		return name in self.dictionary or name in self.context.symbols
	
	def getSymbol(self, name):
		""" Returns the symbol associated with the given name by first looking
			in self.dictionary, then in self.context.symbols. """
		if name in self.dictionary: return self.dictionary[name]
		if name in self.context.symbols: return self.context.symbols[name]

	def postwalk(self, ast):
		""" Mark invalid references. """
		if isinstance(ast.value, Bundle) and ast.value.isSingleton() and \
		   isinstance(ast.value.singleton,SymbolReference):
			ref = ast.value.singleton
			if not self.hasSymbol(ref.name):
				ast.addTag(Tag(Status.Reject,
					"Reference to unkown symbol '%s'" % ast.match.string))
		
		for kid in ast.children:
			self.postwalk(kid)

	@TraversalStub("qualifier")
	def t_qualifier(self, node):
		""" Qualifier: pass up a qualifier's type as the value """
		if len(node.children) == 0:
			node.value = ""
		else:
			node.value = node.children[0].type
	
	@TraversalStub("bind")
	def t_bind(self, node):
		""" Binder: pass up the binder's type as value """
		node.value = node.children[0].type
	
	@TraversalStub("boundary")
	def t_boundary(self, node):
		""" Boundary: pass up the boundary's type as value """
		if len(node.children) == 0:
			node.value = None
		else:
			node.value = node.children[0].type

	@TraversalStub("qualified_symbol")
	def t_qualified_symbol(self, node):
		""" Symbol with qualifier: pass up a qualified Bundle. """
		if node.status == Status.Reject: return
		
		decorators = node.children[0].value
		bundle = node.children[1].value
		qualifier = node.children[2].value

		if len(decorators):
			# if decorators defined, apply them by making a transient
			# rule symbol from the bundle.
			symbol = bundle.toRuleSymbol("<decorated_%s>" % self._uid)
			self._uid += 1
			for rule in symbol.rules:
				def baseTransform(ast):
					ast.transient = True
					return ast
				rule.transformAST = baseTransform
				for decorator in decorators:
					decorator.decorate(rule)
			bundle = Bundle(symbol)
		
		node.value = Bundle.Qualify(bundle, qualifier)

	@TraversalStub("decorators")
	def t_decorators(self, node):
		if node.status == Status.Reject: return
		node.value = [child.value for child in node.children]
	
	@TraversalStub("decorator")
	def t_decorator(self, node):
		name = node.children[1].value
		if name not in self.context.decorators:
			node.addTag(Tag(Status.Reject, "Unknown decorator '%s'" % name))
		elif not isinstance(self.context.decorators[name], RuleDecorator):
			node.addTag(Tag(Status.Reject, "Decorator '%s' is not valid" % name))
		else:
			node.value = self.context.decorators[node.children[1].value]

	@TraversalStub("symbol")
	def t_symbol(self, node):
		""" Symbol: pass up a Bundle with Symbols from the child """
		if node.status == Status.Reject: return
		
		if len(node.children) == 3:
			value = node.children[1].value
		else:
			kid = node.children[0]
			if kid.status == Status.Reject:
				value = None
			else:
				if kid.type == 'tNAME':
					if kid.value in self.context.symbols:
						symbol = self.context.symbols[kid.value]
					else:
						symbol = SymbolReference(kid.value, self.dictionary)
				elif kid.type == 'RegexString':
					symbol = RegexTokenSymbol('t_%s'%self._uid, kid.value)
					self._uid += 1
				else:
					symbol = LiteralTokenSymbol('t_%s'%self._uid, kid.value)
					self._uid += 1
				value = Bundle(symbol)
			
		node.value = value
	
	@TraversalStub("symbol_list")
	def t_symbol_list(self, node):
		""" Symbol list: concatenate children (Bundles) """
		if node.status == Status.Reject: return
		
		if len(node.children) == 3:
			# 0 is symbol_list, 2 is symbol
			node.value = Bundle.Concatenate(node.children[0].value, node.children[2].value)
		elif len(node.children) == 1:
			# 0 is symbol
			node.value = node.children[0].value
	
	@TraversalStub("alternatives_list")
	def t_alternatives_list(self, node):
		""" Alternatives list: Union child Bundles """
		if node.status == Status.Reject: return
		
		if len(node.children) == 3:
			# 0 is alternatives_list, 2 is symbol_list
			node.value = Bundle.Union(node.children[0].value, node.children[2].value)
		elif len(node.children) == 1:
			# 0 is symbol_list
			node.value = node.children[0].value
	
	@TraversalStub("action")
	def t_action(self, node):
		""" Action: set transformAST appropriately.
			
			Passes up (name,name_match,code,code_match) """
		name = node.children[1]
		code = node.children[4]
		node.value = name.value, name.match, GenerateThunk(code), code.match

	@TraversalStub("action_list")
	def t_action_list(self, node):
		""" List of actions. """
		if len(node.children) == 0:
			node.value = []
		else:
			node.value = []
			node.value.extend(node.children[0].value)
			node.value.append(node.children[1].value)
	
	@TraversalStub("rule")
	def t_rule(self, node):
		""" Rule: put rule into the namespace, passing up the rule """
		if node.status == Status.Reject: return
		
		name = node.children[0].value
		binder = node.children[1].value
		
		if len(node.children) == 4:
			# a bundle representing the right-hand side
			bundle = node.children[2].value
			actions = node.children[3].value
		else:
			# empty right-hand side
			bundle = Bundle()
			actions = []
		
		# new rule symbolf from bundle
		rs = bundle.toRuleSymbol(name)
		
		# apply actions
		transformAST = None
		for action in actions:
			if action[0] == 'value':
				if transformAST is not None:
					node.addTag(Tag(Status.Reject,
						"Value transformation defined twice", action[1]))
				def doTransform(ast):
					ast.value = action[2](None,ast)
					return ast
				transformAST = doTransform
			else:
				node.addTag(Tag(Status.Reject,
					"Unknown action identifier", action[1]))
		for rule in rs.rules:
			if transformAST is not None:
				def bindTransformAST(oldTransform,newTransform):
					return lambda ast: newTransform(oldTransform(ast))
				rule.transformAST = bindTransformAST(rule.transformAST, transformAST)

		if binder == 'tAPPEND':
			if name not in self.dictionary:
				node.addTag(Tag(Status.Reject,
				               "Attempt to append to a locally unbound rule",
				               node.children[1].match))
				return
			else:
				existing = self.dictionary[name]
				rules = []
				rules.extend(existing.rules)
				rules.extend(rs.rules)
				rs = RuleSymbol(name, rules)
		
		self.dictionary[name] = rs
		node.value = rs
	
	@TraversalStub("rule_list")
	def t_rule_list(self, node):
		""" Rule list: The start rule is the first rule in the grammar """
		# we don't just take node.children[0] .. have to take the latest
		# version of the rule referred to in node.children[0], in case of
		# "|=" appends
		if node.children[0].value is not None:
			node.value = self.dictionary[node.children[0].value.name]
	
	@TraversalStub("grammar")
	def t_grammar(self, node):
		""" Grammar: put the whole thing together with a final post-walk. """
		self.postwalk(node)
		if node.status == Status.Reject: return

		# if the decorators are derived from EBNFDecorators, mark all
		# unmarked rules with BoundaryRequired.
		if 'BreaksRequired' in self.context.decorators:
			for rule in RuleWalk(node.children[0].value):
				if not hasattr(rule, 'BoundaryPolicy'):
					self.context.decorators['BreaksRequired'].decorate(rule)
				
				# apply the boundary policy
				if callable(rule.BoundaryPolicy):
					## TODO: worry about how this interacts with rules
					## that are used in more than one grammar ...
					rule.BoundaryPolicy()
		
		node.value = Parser(node.children[0].value,
			self.context.skiplist, self.context.traverser)

##
## global singleton instances
##

EBNFParser = EBNFParser()

# default list of tokens to skip in EBNF-generated parsers
EBNFDefaultSkiplist = [RegexTokenSymbol('SPACE', r'\s+')]
DefaultContext = EBNFContext(skiplist=EBNFDefaultSkiplist,decorators={})

class EBNFTest(ParserTestCase):
	def testSimpleGrammar(self):
		self.parser = EBNFParser.parse("@matched := A B", DefaultContext).value
		self.assertGudParse("AB")
		self.assertGudParse("  A B")
		self.assertBadParse("BA")
		self.assertBadParse("ABA")
		self.assertBadParse("AAB")
		self.assertBadParse("AABB")
		self.assertBadParse("AAABBB")
		
	def testPipeGrammar(self):
		self.parser = EBNFParser.parse("@matched := A (B | C)", DefaultContext).value
		self.assertGudParse("AB")
		self.assertGudParse("AC")
		self.assertGudParse("  A B")
		self.assertBadParse("BA")
		self.assertBadParse("ABA")
		self.assertBadParse("AAB")
		self.assertBadParse("AABB")
		self.assertBadParse("AAABBB")
		
	def testAppendGrammar(self):
		self.parser = EBNFParser.parse("\n\n@matched := A B\n@matched |= A C", DefaultContext).value
		self.assertGudParse("AB")
		self.assertGudParse("AC")
		self.assertGudParse("  A B")
		self.assertBadParse("BA")
		self.assertBadParse("ABA")
		self.assertBadParse("AAB")
		self.assertBadParse("AABB")
		self.assertBadParse("AAABBB")
		
	def testQuotedGrammar(self):
		self.parser = EBNFParser.parse("@matched := 'A'? \"B\"", DefaultContext).value
		self.assertGudParse("B")
		self.assertGudParse("AB")
		self.assertGudParse("  A B")
		self.assertBadParse("BA")
		self.assertBadParse("ABA")
		self.assertBadParse("AAB")
		self.assertBadParse("AABB")
		self.assertBadParse("AAABBB")
		
	def testParenGrammar(self):
		self.parser = EBNFParser.parse("@matched := (('A') \"B\")", DefaultContext).value
		self.assertGudParse("AB")
		self.assertGudParse("  A B")
		self.assertBadParse("BA")
		self.assertBadParse("ABA")
		self.assertBadParse("AAB")
		self.assertBadParse("AABB")
		self.assertBadParse("AAABBB")

	def testEmbeddedGrammar(self):
		self.parser = EBNFParser.parse("@matched := A @matched? B", DefaultContext).value
		self.assertGudParse("AB")
		self.assertBadParse("BA")
		self.assertBadParse("ABA")
		self.assertBadParse("AAB")
		self.assertGudParse("AABB")
		self.assertGudParse("AAABBB")
		self.assertBadParse("AAABBBB")

	def testNestedQualifiers(self):
		'''
		Tests that we can nest qualifiers and still
		get the expected results.
		'''
		grammar = "@rule := ((a? b+)* c)+"
		self.parser = EBNFParser.parse(grammar, DefaultContext).value
		
		self.assertGudParse("a b c")
		self.assertGudParse("b c")
		self.assertGudParse("b c b c")
		self.assertGudParse("a b c a b c")
		self.assertGudParse("a b a b c")
		self.assertGudParse("a b b a b c")
		self.assertGudParse("a b b b b c")
		self.assertGudParse("b b c")
		self.assertGudParse("a b b c")
		self.assertGudParse("a b a b b c a b b c b b c")

	def testQualifiers(self):
		'''
		Tests that the qualifiers work correctly.
		'''
		grammar = '''
			@cmd := qPLUS @plus
			@plus := plus+
			@cmd |= qSTAR @star
			@star := star*
			@cmd |= qQMARK qmark?
			'''
		self.parser = EBNFParser.parse(grammar, DefaultContext).value

		self.assertBadParse("qPLUS")
		self.assertGudParse("qPLUS plus")
		self.assertGudParse("qPLUS plus plus")
		self.assertGudParse("qPLUS plus plus plus")

		self.assertGudParse("qSTAR")
		self.assertGudParse("qSTAR star")
		self.assertGudParse("qSTAR star star")
		self.assertGudParse("qSTAR star star star")

		self.assertGudParse("qQMARK")
		self.assertGudParse("qQMARK qmark")
		self.assertBadParse("qQMARK qmark qmark")
		self.assertBadParse("qQMARK qmark qmark qmark")

	def testDecorators(self):
		""" Tests that we can decorate a symbol. """
		grammar = '''
			@cmd := cd [arg]@directory?
			@directory := [woot]"~"
		'''
		decorator = RuleDecorator()
		context = EBNFContext(decorators={'arg':decorator, 'woot':decorator})
		self.parser = EBNFParser.parse(grammar, context).value

	def testBreaksDecorators(self):
		grammar = '''
			@S := [BreaksRequired](@a @b @c)
			@a := [BreaksForbidden]'a'+
			@b := [BreaksOptional]'b'+
			@c := [BreaksRequired]'c'+
		'''
		self.parser = EBNFParser.parse(grammar, EBNFContext()).value
		self.assertGudParse('a b c')
		self.assertGudParse('aaa b c')
		self.assertGudParse('aa bbb b c c')
		self.assertGudParse('aa b bb c')
		self.assertBadParse('a a b c')
		self.assertBadParse('a b cc')

	def testActions(self):
		""" Actions. """
		grammar = '''
			@sum := @integer '+' @integer
				{value: $0$+$2$}
				
			@integer := /\d+/
				{value: int($0$)}
		'''
		self.parser = EBNFParser.parse(grammar, DefaultContext).value
		self.assertGudParse('12+10')
		self.assertBadParse('12+10k')

if __name__ == '__main__':
	import unittest
	unittest.main()

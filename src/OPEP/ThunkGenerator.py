# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
EBNF-inlined code interpreter.

Returns a function.
"""

from AST import *

import re

# matches identifiers
regex = re.compile('\$(.*?)\$')

# single ingeter identifier
isint = re.compile('\d+')

@takes(AST)
def GenerateThunk(ast):
	"""	Generates a callable corresponding to the code refered to in
		the given AST.
		
		Input:  AST whose match.string contains code.
		Output: Callable(Context, AST)->Value.
		
		e.g. "int($0$)" -> lambda context, ast: int(ast.children[0].value)
		
		Code replacement is done for references of the form $[reference]$.
		
		$$		->	ast.value
		$i$		->	ast.children[i].value for integer i
		"""
	def reference(identifier):
		if len(identifier) == 0:
			return "ast.value"
		elif isint.match(identifier):
			return "ast.children[%s].value" % identifier
		else:
			raise "Unknown identifier %s" % identifier
		
	raw = ast.match.string
	last = 0
	code = ""
	for match in regex.finditer(raw):
		code += raw[last:match.start()]
		code += reference(match.group(1))
		last = match.end()
	code += raw[last:]
	
	def evaluate(context, ast):
		# print "!! Evaluating %s" % repr(code)
		# print "!! Child values:",[child.value for child in ast.children]
		return eval(code)
	
	return evaluate


#code = 'int($$)+int($$)'
#test = AST('woot','12')
#print GenerateThunk(AST('text',None,Match(code)))(None, test)

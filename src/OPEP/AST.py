# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Data structures produced by OPEP.

(c) Daniel Ramage 2005
"""

from decorators import *

class Match(object):
	"""
	A Match represents a region in a string from a start offset (inclusive)
	to end offset (exclusive).
	"""
	@takes("Match", str, int, int)
	def __init__(self, context, start=0, end=None):
		if end is None: end = len(context)
	
		assert start >= 0 and start <= len(context), \
			"Invalid context start offset"
		assert end >= start and end <= len(context), \
			"Invalid context end offset"
		
		self.context = context
		self.start = start
		self.end = end
	
	@property
	def string():
		def fget(self):
			return self.context[self.start:self.end]
	
	def __repr__(self):
		return repr(self.context)+"[%s:%s]"%(self.start,self.end)
	
	def __len__(self):
		return self.end - self.start
	
	def __eq__(self, other):
		return other is not None \
		   and self.start == other.start \
		   and self.end == other.end \
		   and self.context == other.context
	
	def __hash__(self):
		return hash(self.context) + 13*self.start - 17*self.end
	
	@staticmethod
	@takes("Match","Match")
	@returns("Match")
	def Union(a, b):
		if a is None:
			return b
		if b is None:
			return a
		
		if a.context != b.context:
			raise "Cannot Union matches on different contexts"
		
		if a.start < b.start:
			start = a.start
		else:
			start = b.start
		
		if a.end > b.end:
			end = a.end
		else:
			end = b.end
		
		return Match(a.context, start, end)

class Status(object):
	""" Enumeration of possible status values for a Tag. """
	def __repr__(self):
		for name in dir(Status):
			if getattr(Status,name) == self:
				return "Status.%s" % name
	
	def __cmp__(a, b):
		""" Returns the comparison of two status objects, with Status.Accept
			as the minimum, Status.Reject as the maximum. """
		if a is b: return 0
		if a is Status.Reject: return 1
		if a is Status.Warning and b is Status.Accept: return 1
		return -1
	
Status.Accept = Status()
Status.Reject = Status()
Status.Warning = Status()

class Tag(object):
	""" Applies a message and status to a Match. """
	@takes("Tag", Status, str, Match)
	def __init__(self, status=None, message=None, match=None):
		self.status = status
		self.message = message
		self.match = match

	def __repr__(self):
		return "Tag(match:%s, status:%s, message:%s)" % \
			(self.match, self.status, repr(self.message))
	
	def __eq__(self, other):
		return self.match == other.match and self.status == other.status \
			and self.message == other.message
	
	def __cmp__(self, other):
		return cmp(self.status, other.status) \
			or cmp(len(self.match), len(other.match)) \
			or cmp(self.match.start, other.match.start)
	
	def __hash__(self):
		return hash(hash(self.match) + hash(self.status) + hash(self.message))
	
class AST(object):
	"""
	Abstract Syntax Tree as produced by OPEP.
	"""
	
	@takes('AST', str, object, Match)
	def __init__(self, type, value=None, match=None):
		"""
		Type is a string representing the type of this AST, e.g. "token1".
		match is a Match representing the region of a string for the AST.
		"""
		self.type = type
		self.value = value
		self.match = match
		self.transient = False
		self.isCompoundLeaf = False

		# list of tags on this ast
		self._tags = set()
		
		# list of child ast's on this ast
		self._children = []
		
		# list of completers on this ast
		self._completers = []
	
	@staticmethod
	@takes('AST', 'AST')
	@returns('AST')
	def Add(a, b):
		"""
		Returns a new AST corresponding to the joining of b onto as a child.
		Properly handles edge cases where a or b is None.
		"""
		if a is None:
			return b
		if b is None:
			return a
		ast = AST.Clone(a)
		ast.addChild(b)
		return ast
	
	@staticmethod
	@takes('AST')
	@returns('AST')
	def Clone(ast):
		""" Returns a shallow clone of this AST. """
		new = AST(ast.type, ast.value, ast.match)
		new._tags = set(ast._tags)
		new._children = list(ast._children)
		new._completers = list(ast._completers)
		for name in dir(ast):
			if not hasattr(new, name):
				setattr(new, name, getattr(ast, name))
		return new
	
	@staticmethod
	@takes('AST')
	@returns('AST')
	def DeepCopy(ast):
		""" Returns a deep copy of the given AST. """
		new = AST(ast.type, ast.value, ast.match)
		new._tags = set(ast._tags)
		new._children = [AST.DeepCopy(a) for a in ast._children]
		new._completers = list(ast._completers)
		for name in dir(ast):
			if not hasattr(new, name):
				setattr(new, name, getattr(ast, name))
		return new
	
	@staticmethod
	@takes('AST', Status, str)
	@returns('AST')
	def Tagged(ast, status, message, match=None):
		""" Returns a shallow clone of this AST with the given Tag appended. """
		new = AST.Clone(ast)
		new.addTag(Tag(match or ast.match, status, message))
		return new
	
	@staticmethod
	@takes('AST')
	@returns('AST')
	def EatChildren(ast):
		""" Returns a version of the given AST with no child nodes.
			Tags from children are migrated up. """
		new = AST(ast.type, ast.value, ast.match)
		for tag in ast.tags:
			new.addTag(tag)
		return new
	
	@takes('AST', Tag)
	def addTag(self, tag):
		""" Adds the given tag to the list of tags on this AST. """
		if tag.match is None:
			tag.match = self.match
		elif tag.match.context is not self.match.context:
			raise "Attempt to apply tag to an AST with a different string context."
		self._tags.add(tag)
	
	@takes('AST', 'AST')
	def addChild(self, ast):
		""" Adds the given AST to the list of children on this AST. """
		self.match = Match.Union(self.match, ast.match)
		
		if ast.transient:
			# ast was marked transient .. ignore its type, value, and tags ..
			# insert all its children directly
			for child in ast.children:
				self.addChild(child)
		else:
			# standard addition .. append ast to child list
			self._children.append(ast)

	def addCompleter(self, completer):
		""" Adds the given completer to this AST's list of completers. """
		self._completers.append(completer)

	@takes('AST', str)
	def walkForAttribute(self, attribute):
		"""	Returns the list of ASTs that have been marked as args by the
			arg decorator. """
		if hasattr(self, attribute) and getattr(self, attribute):
			yield self
		for child in self.children:
			for good in child.walkForAttribute(attribute):
				yield good

	##
	## properties
	##
	
	@property
	def status():
		""" The overall status of the AST is the most severe status in its tags. """
		def fget(self):
			status = Status.Accept
			for tag in self.tags:
				if tag.status == Status.Reject:
					status = Status.Reject
				elif tag.status == Status.Warning and status == Status.Accept:
					status = Status.Warning
			return status

	@property
	def children():
		"""	The child AST nodes. """
		def fget(self):
			return self._children
	
	@property
	def tags():
		"""	The list of tags on this node and all child nodes. """
		def fget(self):
			for tag in self._tags:
				yield tag
				
			for child in self._children:
				for tag in child.tags:
					yield tag

	@property
	def completers():
		"""	The list of Completers associated with this AST. """
		def fget(self):
			for completer in self._completers:
				yield completer
			
			for child in self._children:
				for completer in child.completers:
					yield completer
	
	@property
	def leaves():
		""" The list of ASTs that are leaves under this AST. """
		def fget(self):
			if self.isCompoundLeaf:
				yield self
				
			else:
				if len(self._children) == 0:
					yield self
				for child in self._children:
					for leaf in child.leaves:
						yield leaf
	
	def __repr__(self, prefix=""):
		if self.match is not None:
			bounds = "%s:%s" % (self.match.start, self.match.end)
		else:
			bounds = "?"
		
		ret = "AST [%s] type=%s value=%s" % \
			(bounds, repr(self.type),repr(self.value))
		
		joinstr = "\n%s  " % prefix
		if len(self._tags):
			ret += joinstr+joinstr.join([repr(tag) for tag in self._tags])
		if len(self._children):
			ret += joinstr+joinstr.join([child.__repr__(prefix+"  ")
				for child in self._children])
		return ret
	
	def __eq__(self, other):
		"""	Returns true if the two ASTs are *structurally* identical, i.e.
			they have the same match, type, and children. """
		return self.type == other.type \
		   and self.match == other.match \
		   and self.children == other.children

if __name__ == '__main__':
	ast = AST('root')
	a = AST('a', 'a', Match("hi"))
	a.addChild(AST('a1'))
	a.addTag(Tag(Match("hi",0,1),Status.Warning,"Lowercase"))
	ast.addTag(Tag(Match("hi"),Status.Accept,"Great!"))
	ast.addChild(a)
	ast.addChild(AST('b'))
	print ast
	
	for tag in ast.tags:
		print tag
	
	print ast.status

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
Terminal defines the basic interfaces for Terminal objects in GSH,
i.e. objects that parse and display tty stuff.

(c)Daniel Ramage 2005
"""

#import pty
#import os

#import gci
#import jobs
#import descriptors
#import emulator

from OPEP.AST import Status

#class Terminal(object):
#	"""
#	Abstract base class of valid GSH terminals.
#	"""
#	def __init__(self, status, emulatorType=emulator.XTermEmulator):
#		"""
#		Initializes this terminal with the given StatusIndicator object
#		as the status keeper.
#		"""
#		assert isinstance(status, StatusIndicator)
#		
#		self._status = status
#		self._emulatorType = emulatorType
#		self._emulators = {}
#		
#		self._masterFD = {}	# job -> master (our) descriptor
#		self._slaveFD = {}	# job -> slave (child) descriptor
#
##		import clmg.clmg
#		
##		self.namespace = gsh.GSHNameSpace(self)
##		self.grammar = CLMG(self.namespace)
#		
#		# array of jobs.Job objects we are currently executing.
#		self._jobs = []
#
#	def execute(self, command):
#		"""
#		Executes the given command in this terminal.
#		"""
#		self.forkJob(self.grammar.interpret(command))
#
#	def getPTY(self, job):
#		"""
#		Creates and registers a new PTY with this Terminal (reusing an
#		existing PTY if one exists).  Data sent to the terminal will be
#		available for reading on the returned FD and data written to
#		that FD will be displayed in the terminal.
#		"""
#		if job in self._slaveFD:
#			return self._slaveFD[job]
#		
#		def toTerminal(string):
#			self._emulators[job].interpret(string)
#		
#		(master, slave) = pty.openpty()
#		self._masterFD[job] = master
#		self._slaveFD[job] = slave
#		descriptors.register(master, read=toTerminal)
#		descriptors.register(slave)
#
#		return slave
#	
#	def forkJob(self, job):
#		'''
#		Forks the given jobs.Job in this terminal.
#		'''
#		assert isinstance(job,jobs.Job)
#		
#		self.ITerminal_jobStarted(job)
#		
#		self._emulators[job] = self._emulatorType(
#			self.ITerminal_getTerminalDisplay(job))
#		
#		# some jobs may return instantly: if so we remember
#		self.quick_complete = False
#		def removeJob(done):
#			'''
#			Callback: removes the given job from the job list.
#			'''
#			if done in self._masterFD:
#				descriptors.poll()
#				os.close(self._slaveFD[job])
#				descriptors.drain(self._masterFD[job])
#				os.close(self._masterFD[job])
#				descriptors.unregister(self._masterFD[job])
#				descriptors.unregister(self._slaveFD[job])
#				del self._masterFD[job]
#				del self._slaveFD[job]
#
#			del self._emulators[job]
#						
#			if done in self._jobs:
#				self._jobs.remove(done)
#			elif done == job:
#				self.quick_complete = True
#			else:
#				raise "Rogue job completion?"
#			self.ITerminal_jobCompleted(job)
#		
#		fd = jobs.FDDescriptor(self.getPTY(job))
#		job.setDefaultStdin(fd)
#		job.setDefaultStdout(fd)
#		job.setDefaultStderr(fd)
#		job.execute(removeJob)
#		
#		if not self.quick_complete:
#			self._jobs.append(job)
#
#	def feedChild(self, job, bytes):
#		"""
#		Sends the given bytes to the child process.
#		"""
#		if job in self._masterFD:
#			os.write(self._masterFD[job], bytes)
#
#	def feedTerminal(self, job, string):
#		"Presents the string (from job) to the terminal for displaying."
#		self.getTerminalHandle(job).insert(string)
#		
#	##
#	## Properties
#	##
#		
#	def status():
#		"""
#		The StatusIndicator holding the status information for this
#		Terminal.
#		"""
#		def fget(self):
#			return self._status
#		return locals()
#	status = property(**status())
#	
#	def jobs():
#		doc = "Array of Jobs currently active in this Terminal."
#		def fget(self):
#			return tuple(self._jobs)
#		return locals()
#	jobs = property(**jobs())
#
#	
#	##
#	## BEGIN VIRTUAL METHODS - SUBCLASSES MUST IMPLEMENT
#	##
#
#	def ITerminal_getTerminalDisplay(self, job):
#		"Retrieves a TerminalHandle for the given job."
#		raise "Pure virtual function."
#	
#	def ITerminal_close(self):
#		"Closes the terminal."
#		raise "Pure virtual function."
#
#	def ITerminal_newtab(self):
#		"Adds a new tab in the enclosing window."
#		raise "Pure virtual function."
#		
#	def ITerminal_jobCompleted(self, job):
#		"Job completion notifier."
#		pass
#	
#	def ITerminal_jobStarted(self, job):
#		"Job started notifier."
#		pass


class StatusIndicator(object):
	"""
	Represents the setable status information for a single GSHTerminal.
	"""
	def __init__(self, status=Status.Accept):
		"""
		Initializes this GTK status indicator to use the given eventBox.
		"""
		self.status = status

	def update(self):
		"""
		Propogate the internal status to the GUI for displaying.
		"""
		raise "Pure virtual function."
	
	def status():
		doc = "The status of this indicator: STATUS_WARNING, STATUS_OK or STATUS_ERROR."
		def fget(self):
			return self._status
		def fset(self, value):
			assert value in (Status.Accept, Status.Reject, Status.Warning), \
				"status must be one of Status.{Accept|Reject|Warning}"
			self._status = value
			self.update()
		return locals()
	status = property(**status())

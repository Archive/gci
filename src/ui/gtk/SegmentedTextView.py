# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
An enhanced gtk.TextView.  Supports multiple segments in the same textview,
each with its own event handlers.

(c) Daniel Ramage 2005
"""

import sys
import gobject
import gtk

from decorators import *

import callback

if gtk.pygtk_version < (2, 8, 1):
	print "WARNING: PyGTK version is less than 2.8.1.  Incompatabilities likely."

##
## Basic classes
##

class SegmentedTextView(gtk.TextView):
	""" A gtk.TextView with support for multiple segments. """

	##
	## GObject signal prototypes
	##
	
	__gsignals__ = {
		'scrolled': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_OBJECT,)),
		'changed': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
	}

	def __init__(self, default_properties={}):
		gtk.TextView.__init__(self)
		
		##
		## properties
		##
		
		# all segments
		self._segments = []
		
		# segments with pending change notifications
		self._changed = set()
		
		# border widgets
		self._borderWidgets = {}
		self.set_border_window_size(gtk.TEXT_WINDOW_LEFT, 18)
		
		# default tag used in new segments
		self._defaultTag = self.buffer.create_tag(
			"stv_default", **default_properties)
		
		# default tag used for separators between segments
		self.separatorTag = self.buffer.create_tag(
			editable=False, background="red", foreground="white")
		
		##
		## gobject connections
		##

		self.connect("set-scroll-adjustments", self._connect_scroll_adjustments)
		self.connect("key-press-event", self._connect_keypress)
		self.buffer.connect("mark-set", self._connect_mark_set)
		self.buffer.connect("insert-text", self._connect_insert)
		self.buffer.connect("delete-range", self._connect_delete)
		self.buffer.connect("changed", self._connect_changed)

	def setBorderWidget(self, segment, widget):
		""" Sets the gtk.Widget to display in the left border region
			for the given segment. """
		self._borderWidgets[segment] = widget
		self.add_child_in_window(widget, gtk.TEXT_WINDOW_LEFT, 0, 0)
		self.updateBorderWidgetPositions()
		widget.show_all()

	def updateBorderWidgetPositions(self, *args):
		""" Positions all border widgets next to their segments. """
		for segment,widget in self._borderWidgets.iteritems():
			x, y = self.buffer_to_window_coords(gtk.TEXT_WINDOW_LEFT,
						0, self.get_line_yrange(segment.anchor)[0])
			self.move_child(widget, 0, y)

	def newSegment(self, index=None):
		""" Creates and returns a new segment in the SegmentedTextView.	"""
		assert index is None or index <= len(self._segments)

		if index is None:
			realIndex = len(self._segments)
		else:
			realIndex = index
		
		if realIndex == 0 and len(self._segments) > 0:
			pos = self._segments[realIndex].capStart("\n", self.separatorTag)
		elif realIndex > 0:
			pos = self._segments[realIndex-1].capEnd("\n", self.separatorTag)
		else:
			pos = self.buffer.get_start_iter()

		segment = Segment(self,
			self.buffer.create_mark(None,pos,True),
			self.buffer.create_mark(None,pos,False))

		self._segments.insert(realIndex, segment)
		
		return segment
	
	def getSegmentAtIter(self, textiter):
		""" Returns the segment at the given gtk.TextIter. """
		cursorPosition = textiter.get_offset()
		for segment in self._segments:
			if cursorPosition <= segment.end.get_offset():
				return segment

	##
	## gobject connections
	##
		
	def _connect_scroll_adjustments(self, widget, hadjustment, vadjustment):
		if vadjustment is None: return
	
		""" Connect to the given scroll adjustments. """
		def immediateWidgetMove(adjustment):
			self.updateBorderWidgetPositions()
		def deferredWidgetMove(adjustment):
			self.updateBorderWidgetPositions()
			self.emit("scrolled", adjustment)
		
		# i have no idea why these two lines work.  this is dark magic
		# inside the leaky abstractions of gtk.TextView.  discovered
		# by much trial and error.
		vadjustment.connect("value-changed", immediateWidgetMove)
		vadjustment.connect_after("value-changed", deferredWidgetMove)

	def _connect_keypress(self, widget, event):
		""" On a keypress, notify the focused segment.  If stop_emission
			has been called on that signal, we should stop emission here. """
		if not self.focused.emit("key-press-event", event):
			self.stop_emission("key-press-event")
	
	def _connect_mark_set(self, textbuffer, textiter, textmark):
		""" Callback when a mark changes in the buffer .. used to watch cursor. """
		if textmark is textbuffer.get_insert():
			textbuffer.move_mark(self.focused._cursor, textiter)
			self.focused.emit("cursor-moved", textiter)
	
	def _connect_insert(self, textbuffer, textiter, text, length):
		""" Notification of an insert event in the buffer.  Spool to handler. """
		segment = self.getSegmentAtIter(textiter)
		if segment is not None and segment not in self._changed:
			self._changed.add(segment)

	def _connect_delete(self, textbuffer, start, end):
		""" Notification of delete event in buffer. """
		segment = self.getSegmentAtIter(start)
		if segment is not None and segment not in self._changed:
			self._changed.add(segment)
		segment = self.getSegmentAtIter(end)
		if segment is not None and segment not in self._changed:
			self._changed.add(segment)
	
	def _connect_changed(self, textbuffer):
		""" Notification of buffer content change. """
		
		# copy and clear self._changed before iterating to emit signals
		# ... it is possible the emission will cause the buffer to be
		# changed again.
		changed = list(self._changed)
		self._changed.clear()
		
		for segment in changed:
			segment.emit("text-changed")
		
		self.emit("changed")
		
		# update the border widget positions, but not too aggressively ...
		# we actually *have* to use the callback module in order for this
		# function to run after the text change is complete (i.e. it is called
		# later by the main loop after this signal's processing is complete.)
		# any value of callback.within will work (including 0).
		#
		# we can't use connect_after on the changed signal because it actually
		# doesn't happen after the buffer contents have changed.  we need to
		# run updateBorderWidgetPositions after the content-change is complete
		# in order for the widgets to be positioned correctly.
		callback.within(.02, self.updateBorderWidgetPositions, callback.push)

	##
	## Properties
	##
	
	@property
	def segments():
		""" The list of regions currently in the viewer. """
		def fget(self):
			return tuple(self._segments)
	
	@property
	def focused():
		""" The currently focused segment. """
		def fget(self):
			## TODO: optimize by sniffing cursor moves
			return self.getSegmentAtIter(\
				self.buffer.get_iter_at_mark(self.buffer.get_insert()))
		def fset(self, segment):
			self._handlers[segment].grabFocus()
	
	@property
	def buffer():
		""" gtk.TextBuffer displayed by the widget. """
		def fget(self):
			return self.get_buffer()
	
	def __len__(self):
		return len(self._segments)

	def __repr__(self):
		return repr(self._segments)

# deprecated: gobject.type_register(SegmentedTextView)


class Segment(gobject.GObject):
	"""
	A mark-bounded region in a gtk.TextBuffer, owned by a single
	SegmentedTextView.
	"""
	
	# gobject signals
	
	__gsignals__ = {
		'text-changed':
			(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
		'key-press-event':
			(gobject.SIGNAL_RUN_LAST, gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT,)),
		'cursor-moved':
			(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,))
	}
	
	def do_key_press_event(self, event):
		""" Class default signal handler for key-press-event. """
		# returning True tells the STV to allow the key to pass through to the
		# TextView widget.
		return True
	
#	## 1x1 image place-holder for separating sub-regions
#	separator = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB,True,8,1,1)
	
	@takes("Segment",SegmentedTextView, gtk.TextMark, gtk.TextMark)
	def __init__(self, stv, start, end):
		"""
		Default constructor.  We are a segment in the given SegmentedTextView
		with start as our start TextMark and end as our end TextMark.
		"""
		gobject.GObject.__init__(self)
		
		##
		## instance variables
		##
	
		# SegmentedTextView containing the segment
		self.stv = stv

		# gtk.TextBuffer containing the segment
		self._buffer = stv.buffer

		self._start = start
		self._end = end

		# gtk.TextMark for the cursor position in this segment
		self._cursor = self._buffer.create_mark(None, self.end, False)
		
#		# list of Segments that are subregions of this one.
#		self._subsegments = []
				
#	def newSubSegment(self,tag=None):
#		"""
#		Returns a new Segment completely contained within this one.
#		"""
#		if len(self._subsegments):
#			self._subsegments[-1].capEnd()
#		
#		if tag is None:	myTag = self._tag
#		else:		myTag = tag
#		
#		self._subsegments.append(
#			Segment(self._buffer, self.end, self.end, myTag))
#		
#		return self._subsegments[-1]

	def __transform(self,position,default):
		"""
		Transforms an input position in a variety of formats to a
		gtk.TextIter in the current buffer.
		"""
		if position is None:
			return default
		elif isinstance(position,gtk.TextIter):
			return position
		else:
			return self.buffer.get_iter_at_offset(
				self.start.get_offset()+position)

	def __bounds(self,start,end):
		"""
		Returns a (start,end) tuple of gtk.TextIter from start and
		end, depending on their vale.
		"""
		return (self.__transform(start,self.start),
			self.__transform(end,self.end))

	def apply_tag(self, tag, start=None, end=None):
		"""
		Applies a gtk.TextTag to the contents of the region.
		"""
		assert isinstance(tag, gtk.TextTag) or isinstance(tag, dict)
		if isinstance(tag, dict):
			tag = self.buffer.create_tag(**tag)
		self.buffer.apply_tag(tag, *self.__bounds(start, end))

	def remove_tag(self, tag, start=None, end=None):
		"""
		Removes the given gtk.TextTag from the full region.
		"""
		assert isinstance(tag, gtk.TextTag)
		self.buffer.remove_tag(tag, *self.__bounds(start, end))

	def remove_all_tags(self, start=None, end=None):
		"""
		Removes all tags from the Segment.
		"""
		self.buffer.remove_all_tags(*self.__bounds(start, end))

	def insert(self, text, pos, tag=None):
		"""
		Inserts text into the region.
		
		text: is the string to insert. pos should be a gtk.TextIter
		between (start,end) for the position of where to insert the
		text.
		
		Applies the segment's default tag to the inserted text.
		If specified, also applies gtk.TextTag 'tag' to the inserted
		text.
		"""
		if type(pos) == int:
			newpos = self.start
			newpos.forward_chars(pos)
			pos = newpos
			del newpos
		assert isinstance(pos, gtk.TextIter)
		
		pos = self.__transform(pos,self.start)
		
		old_offset = pos.get_offset()
		self.buffer.insert(pos,text)
		start = self.buffer.get_iter_at_offset(old_offset)
		end = self.buffer.get_iter_at_offset(old_offset+len(text))
		self.buffer.remove_all_tags(start, end)
		
		if tag is not None:
			self.buffer.apply_tag(tag, start, end)

	def append(self, text):
		"""
		Appends text to the region.
		"""
		self.insert(text, self.end)

	def delete(self, start, end):
		"""
		Deletes the given range of text.
		"""
		self.buffer.delete(*self.__bounds(start,end))

	def shiftMark(self,mark,amount):
		"""
		Shifts 'mark' by 'amount' chars (can be negative).
		"""
		pos = self.buffer.get_iter_at_mark(mark).get_offset()+amount
		self.buffer.move_mark(mark,
			self.buffer.get_iter_at_offset(pos))

	def capEnd(self,separator=None,tag=None,shift=None):
		"""
		Caps the end of the segment by shifting the end marker
		to the left.
		
		If 'shift' is specified, it is the number of characters
		to shift over.
		
		If 'separator' is specified, it is a string to insert
		before shifting the mark.  That string may optionally be
		tagged with the given 'tag'.  
		
		If 'shift' is None and 'separator' is specified, shifts
		by the length of separator.
		
		Returns a gtk.TextIter pointing to just after the given
		separator.
		"""
		assert shift is not None or separator is not None
		
		if shift is not None:
			_shift = shift
		elif type(separator) == str:
			_shift = len(separator)
			self.buffer.insert(self.end, separator)
		elif isinstance(separator,gtk.gdk.Pixbuf):
			_shift = 1
			self.buffer.insert_pixbuf(self.end, separator)
		else:
			raise "Unknown separator type."

		self.shiftMark(self._end, -_shift)
		
#		if len(self._subsegments):
#			self._subsegments[-1].capEnd(shift=_shift)

		pos = self.buffer.get_iter_at_offset(
				self.end.get_offset()+_shift)

		self.buffer.remove_all_tags(self.end, pos)
		if tag is not None:
			self.buffer.apply_tag(tag, self.end, pos)
		
		return pos

	def capStart(self,separator=None,tag=None,shift=None):
		"""
		Just like capEnd, but modifies the start mark.
		"""
		assert shift is not None or separator is not None
		
		if shift is not None:
			_shift = shift
		elif type(separator) == str:
			_shift = len(separator)
			self.buffer.insert(self.start, separator)
		elif isinstance(separator,gtk.gdk.Pixbuf):
			_shift = 1
			self.buffer.insert_pixbuf(self.start, separator)
		else:
			raise "Unknown separator type."

		self.shiftMark(self._start, _shift)
		
#		if len(self._subsegments):
#			self._subsegments[0].capStart(shift=_shift)

		pos = self.buffer.get_iter_at_offset(
				self.start.get_offset()-_shift)

		self.buffer.remove_all_tags(self.start, pos)
		if tag is not None:
			self.buffer.apply_tag(tag, self.start, pos)
		
		return pos

#	def getLineIter(self, line=0):
#		"""
#		Returns a gtk.TextIter at the start of the requested line index.
#		
#		Line numberes start from 0 as the first line in the segment.
#		Lines are added to the segment as necessary if not enough lines
#		are already there.
#		"""
#		if line < 0: line = 0
#		if line > self.end.get_line() - self.start.get_line():
#			lines = line - (self.end.get_line() - self.start.get_line())
#			self.append("\n"*lines)
#		textiter = self.start
#		textiter.forward_lines(line)
#		return textiter
#	
#	def getLineText(self, line):
#		linestart = self.getLineIter(line)
#		return self.buffer.get_text(
#			linestart,
#			self.buffer.get_iter_at_offset(linestart.get_offset(
#				+ self.getLineLength(line))))
#	
#	def getLineLength(self, line):
#		"The length of the line at the given offset into the region."
#		return self.getLineIter(line).get_chars_in_line()-1

	def grabFocus(self):
		"""
		Moves the SegmentedTextView's focus to this segment's cursor.
		"""
		self.buffer.move_mark_by_name("insert", self.cursor)
		self.buffer.move_mark_by_name("selection_bound", self.cursor)
		self.scrollToCursor()

	def scrollToCursor(self):
		"""
		Causes the SegmentedTextView to scroll so that our cursor is
		visible.
		"""
		if self.focused:
			self.stv.scroll_mark_onscreen(self._cursor)

	def setBorderWidget(self, widget):
		"""
		Sets the gtk.Widget to display in the left border region of this
		segment.
		"""
		self.stv.setBorderWidget(self, widget)
		
	##
	## properties
	##

	@property
	def buffer():
		""" gtk.TextBuffer containing segment text. """
		def fget(self):
			return self._buffer

	@property
	def start():
		""" gtk.TextIter at the start of the region. """
		def fget(self):
			return self._buffer.get_iter_at_mark(self._start)
	
	@property
	def end():
		""" gtk.TextIter at the end of the region. """
		def fget(self):
			return self._buffer.get_iter_at_mark(self._end)

	@property
	def cursor():
		""" gtk.TextIter at the current cursor position. """
		def fget(self):
			return self._buffer.get_iter_at_mark(self._cursor)
		def fset(self, textiter):
			self.buffer.move_mark(self._cursor, textiter)
			if self.focused:
				self.buffer.place_cursor(textiter)

	@property
	def anchor():
		""" gtk.TextIter where Segment was created. """
		def fget(self):
			return self._buffer.get_iter_at_mark(self._start)

	@property
	def focused():
		""" True if we are in control the currently focused segment. """
		def fget(self):
			return self.stv.focused == self
	
	@property
	def lines():
		""" The number of lines in the buffer. """
		def fget(self):
			return self.end.get_line() - self.start.get_line() + 1

	@property
	def text():
		""" Contents of the gtk.TextBuffer in this region. """
		def fget(self):
			return self.buffer.get_text(self.start,self.end)
		def fset(self,text):
			self.buffer.delete(self.start,self.end)
			self.buffer.insert(self.start,text)
			self.remove_all_tags()

	def __len__(self):
		return self.end.get_offset()-self.start.get_offset()

	def __str__(self):
		return self.text

# deprecated: gobject.type_register(Segment)

##
## ExpandableSegment implementation
##

class Expander(gtk.EventBox):
	"""
	Little triangle widget for use in the ExpandableSegmentHandler.
	"""
	def __init__(self, region, expanded):
		gtk.EventBox.__init__(self)
		
		self._region = region
		self._arrow = None
		self.setExpanded(expanded)
		self.connect("button-release-event", self.clicked)

	def setExpanded(self, expanded):
		if self._arrow is not None:
			self.remove(self._arrow)
		
		if expanded:
			self._arrow = gtk.Arrow(gtk.ARROW_DOWN,gtk.SHADOW_OUT)
		else:
			self._arrow = gtk.Arrow(gtk.ARROW_RIGHT,gtk.SHADOW_OUT)
		
		self.add(self._arrow)
		self.show_all()

	def clicked(self,widget,args):
		self._region.visible = not self._region.visible

#class ExpandableSegment(Segment):
#	"""
#	A region in a SegmentedTextView with an expander triangle.
#	"""
#	def __init__(self, text="", visible=True):
#		Segment.__init__(self)
#		
#		self._visible = visible
#		self._contents = text
#
#	def register(self, stv, *args):
#		Segment.register(self, stv, *args)
#
#		# buffer for holding the contents when the segment is hidden
#		self._hidden = gtk.TextBuffer(table=self.buffer.get_tag_table())
#						
#		# expander widget
#		self._widget = Expander(self,self._visible)
#		self.stv.setBorderWidget(self, self._widget)
#		
#		self.text = self._contents
#		del self._contents
#
#	##
#	## properties
#	##
#
#	def buffer():
#		doc="gtk.TextBuffer holding segment contents"
#		def fget(self):
#			if self._visible:
#				return Segment.buffer.fget(self)
#			else:
#				return self._hidden
#		return locals()
#	buffer = property(**buffer())
#
#	def start():
#		doc="gtk.TextIter at start of Segment."
#		def fget(self):
#			if self._visible:
#				return Segment.start.fget(self)
#			else:
#				return self._hidden.get_start_iter()
#		return locals()
#	start = property(**start())
#	
#	def end():
#		doc="gtk.TextIter at end of Segment."
#		def fget(self):
#			if self._visible:
#				return Segment.end.fget(self)
#			else:
#				return self._hidden.get_end_iter()
#		return locals()
#	end = property(**end())
#
#	def visible():
#		doc="Wether or not the region is visible"
#		def fget(self):
#			return self._visible
#		def fset(self,visible):
#			if visible == self._visible:
#				return
#			
#			if visible:
#				start = self._hidden.get_start_iter()
#				end   = self._hidden.get_end_iter()
#				source = self._hidden
#				target = self._buffer
#				dest   = Segment.start.fget(self)
#			else:
#				start = Segment.start.fget(self)
#				end   = Segment.end.fget(self)
#				source = self._buffer
#				target = self._hidden
#				dest   = self._hidden.get_start_iter()
#			
#			target.insert_range(dest, start, end)
#			source.delete(start,end)
#			
#			self._widget.setExpanded(visible)
#			self._visibile = visible
#			self._visible = visible
#		return locals()
#	visible = property(**visible())
#
##
## Test-hook main
##
#
#if __name__ == '__main__':
#	window = gtk.Window()
#	window.set_title("gtk.TextView Advanced Feature Test")
#	window.connect("delete-event", lambda self,args: sys.exit())
#
#	text = SegmentedTextView()
#	text.set_size_request(440,200)
#
#	tag = text.buffer.create_tag(weight=800,underline=2)
#	text.addSegment(ExpandableSegment("hii\nyup", True))
#	text.addSegment(ExpandableSegment("Multiple lines:\n1\n2\n3"))
#	text.addSegment(ExpandableSegment("Multiple lines:\n1\n2\n3"))
#	text.addSegment(ExpandableSegment("Multiple lines:\n1\n2\n3"))
#	text.addSegment(ExpandableSegment("Multiple lines:\n1\n2\n3"))
#	text.addSegment(ExpandableSegment("Multiple lines:\n1\n2\n3"))
#	text.addSegment(ExpandableSegment("middle1\nwoo"),1)
#	text.addSegment(ExpandableSegment("middle2"),1)
#	text.segments[0].append(" there")
#	text.segments[3].append("\n4")
#
#	text.setBorderWidget(text.segments[2], gtk.Label("?"))
#
#	s = gtk.ScrolledWindow()
#	s.add(text)
#	window.add(s)
#	window.show_all()
#
#	from time import sleep
#
#	while True:
#		if gtk.main_iteration(0):
#			sleep(.01)
#		text.updateBorderWidgetPositions()
#

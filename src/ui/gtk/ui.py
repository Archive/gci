# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
GTK implementation of the Terminal and Window kit-specific bindings.

(c) Daniel Ramage 2005
"""

import sys
import gtk
import pango
import gobject

from decorators import *

import callback
import Runtime.Parser
import Runtime.Viewers

from OPEP.OPEP import *
from OPEP.AST import *
from OPEP.EBNF import *

from SegmentedTextView import SegmentedTextView,Segment

def poll_interface():
	""" Polls the GTK main loop - called by main loop in gsh.py. """
	while gtk.events_pending():
		gtk.main_iteration(False)

class CompletionsPopup(object):
	""" Popup window for preventing completion options. """
	@takes("CompletionsPopup", "GrammaticalTextSegment", str)
	def __init__(self, segment, font=None):
		self.model = gtk.ListStore(gobject.TYPE_STRING)
		
		self.treeview = gtk.TreeView(self.model)
		self.treeview.set_headers_visible(False)
		cell = gtk.CellRendererText()
		if font is not None: cell.set_property("font", font)
		self.treeview.append_column(gtk.TreeViewColumn("Completion", cell, text=0))
		
		scrolled = gtk.ScrolledWindow()
		scrolled.add(self.treeview)
		scrolled.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		scrolled.set_size_request(240,200)
		scrolled.set_border_width(0)
		
		self.window = gtk.Window(gtk.WINDOW_POPUP)
		self.window.add(scrolled)
		self.window.set_border_width(2)
	
	@takes("CompletionsPopup", [Completion], int, int)
	def show(self, completions, x, y):
		""" Shows the list of completions, with the popup below and to the
			right of the given x, y coordinates. """
		self.model.clear()
		for completion in completions:
			self.model.append([str(completion)])
		self.window.move(x,y)
		self.window.show_all()
		self.treeview.grab_focus()
		self.window.activate_focus()
	
	@takes("CompletionsPopup")
	def hide(self):
		self.window.hide()
	
	@property
	def visible():
		def fget(self):
			return self.window.get_property('visible')

class GrammaticalTextSegment(object):
	""" A TextSegment that auto-validates according to a grammar. """
	@takes("GrammaticalTextSegment", Segment, Parser, anything)
	def __init__(self, segment, parser, *arguments):
		""" Initializes the handler to use the given parser for validating
			input in the given segment.  If specified, arguments are provided
			to the parser during parsing. """
		self.segment = segment
		self.indicator = GTKStatusIndicator()
		self.parser = parser
		self.arguments = arguments
		
		self._tags = {
			Status.Accept:
				segment.buffer.create_tag(),
			Status.Reject:
				segment.buffer.create_tag(underline=pango.UNDERLINE_ERROR),
			Status.Warning:
				segment.buffer.create_tag(foreground="orange",
					underline=pango.UNDERLINE_SINGLE)
		}
		
		self.segment.setBorderWidget(self.indicator)
		font = 'default font'
		if hasattr(self.segment.stv, 'font'):
			font = self.segment.stv.font
		self.popup = CompletionsPopup(self, font)

		## connections

		def cursor_moved(segment, textiter):
			self.popup.hide()
		self.segment.connect('cursor-moved', cursor_moved)
		
		def text_changed(text):
			self.parse()
			if self.popup.visible:
				self.showCompletions(segment.cursor)
		self.segment.connect('text-changed', text_changed)
		
		self.segment.connect("key-press-event", self.key_press_event)
		
		self.parse()

	
	# map from Status to icon for that status
	ICONS = { Status.Accept:	gtk.STOCK_APPLY,
	          Status.Reject:	gtk.STOCK_DIALOG_ERROR,
	          Status.Warning:	gtk.STOCK_DIALOG_WARNING }
		
	def parse(self):
		""" Parse the current region contents using the parser, updating
			the StatusIndicator. """
		self.parsed = None
		self.indicator.setImage(self.ICONS[Status.Accept])
		self.segment.remove_all_tags()

		tips = {Status.Accept: [], Status.Warning: [], Status.Reject: []}

		if self.parser is None:
			# no grammar defined
			self.indicator.status = Status.Warning
			
		else:
			self.parsed = self.parser.parse(self.segment.text, *self.arguments)
			self.indicator.setImage(self.ICONS[self.parsed.status])
			for tag in self.parsed.ast.tags:
				self.segment.apply_tag(self._tags[tag.status],
					start=tag.match.start, end=tag.match.end)
				if tag.message is not None:
					tips[tag.status].append(tag.message)
		
		messages = []
		for k in (Status.Reject, Status.Warning, Status.Accept):
			if len(tips[k]) == 0: continue
			message = "%s:\n  - %s" % (str(k), "\n  - ".join(tips[k]))
			messages.append(message)
		
		if len(messages):
			message = "\n".join(messages)
		else:
			message = None
		
		self.indicator.setTip(message)

	@takes("GrammaticalTextSegment", gtk.TextIter)
	def showCompletions(self, textiter):
		offset = textiter.get_offset() - self.segment.start.get_offset()
		completions = self.parsed.completions(offset)
		
		if len(completions) == 0 and self.popup.visible:
			self.popup.hide()
		
		elif len(completions) == 1 and not self.popup.visible:
			# just one completion - replace
			self.popup.hide()
			completion = completions[0]
			self.segment.delete(completion.match.start, completion.match.end)
			self.segment.insert(completion.string, completion.match.start)
		
		elif len(completions) != 0:
			# all of this jazz is to compute pixel coordinates of one line down
			# from start of first completion.
			start = self.segment.start
			start.forward_chars(min([completion.match.start for completion in completions]))
			rect = self.segment.stv.get_iter_location(start)
			y, height = self.segment.stv.get_line_yrange(start)
			x, y = self.segment.stv.buffer_to_window_coords(
				gtk.TEXT_WINDOW_TEXT, rect.x, rect.y)
			wX, wY = self.segment.stv.get_window(gtk.TEXT_WINDOW_TEXT).get_origin()
			x += wX
			y += wY
			self.popup.show(completions, x-6, y+height)

	@takes("GrammaticalTextSegment", Segment, gtk.gdk.Event)
	def key_press_event(self, segment, event):
		""" Keypress listener for completions """
		if (gtk.gdk.keyval_name(event.keyval) == "space" and \
		    event.state & gtk.gdk.CONTROL_MASK) or \
		   (gtk.gdk.keyval_name(event.keyval) == "Tab"):
		   
			segment.stop_emission("key-press-event")
			
			# if currently visible and they request a completion, hide
			# the window (so that if we have length one, we can complete it)
			if self.popup.visible:
				self.popup.hide()
			self.showCompletions(segment.cursor)
			
		if self.popup.visible:
			if gtk.gdk.keyval_name(event.keyval) in ('Down','Up'):
				# to do: get those events recognized
				event.window = self.popup.treeview.get_bin_window()
				print event.window, self.popup.treeview.get_bin_window()
				self.popup.treeview.emit("key-press-event", event)
				segment.stop_emission("key-press-event")
			elif gtk.gdk.keyval_name(event.keyval) == 'Escape':
				self.popup.hide()
				segment.stop_emission("key-press-event")

	@property
	def value():
		def fget(self):
			""" Returns the value of the parse. """
			return self.parsed.ast.value

	@property
	def status():
		def fget(self):
			""" Returns the status of the parse. """
			return self.parsed.ast.status

class GTKStatusIndicator(gtk.EventBox):
	""" A status indicator for grammar segments that uses GTK stock icons. """
	tooltips = gtk.Tooltips()
	
	@takes("GTKStatusIndicator")
	def __init__(self):
		gtk.EventBox.__init__(self)
		
		self._image = gtk.image_new_from_stock(gtk.STOCK_APPLY, gtk.ICON_SIZE_MENU)
		self._image.set_size_request(16,16)
		self.add(self._image)
		self.set_size_request(18,18)

	@takes("GTKStatusIndicator", str)	
	def setImage(self, image):
		""" Sets the image of the status indicator from the gtk.stock value """
		self._image.set_from_stock(image, gtk.ICON_SIZE_MENU)
	
	@takes("GTKStatusIndicator", str)
	def setTip(self, message):
		self.tooltips.set_tip(self, message)

import schema
class UISchema(schema.Schema):
	""" Default (empty) schema for gtk UI. """
	@takes("UISchema", "GTKTerminal")
	def __init__(self, terminal):
		self.terminal = terminal

class GTKTerminal(SegmentedTextView):
	"""	A Terminal implementation based on a single GrammaticalTextSegment
		for receiving typed input and a set of TerminalDisplaySegmentHandlers
		for embedded virtual terminals in a single SegmentedTextView. """
	
	@takes("GTKTerminal", "GTKWindow", GTKStatusIndicator)
	def __init__(self, window):
		SegmentedTextView.__init__(self)
		
		# font
		self.font = "Courier 10 Pitch" # read by popup
		self.modify_font(pango.FontDescription(self.font))
		
		self._window = window

		# tag for the command before its value
		self._commandTag = self.buffer.create_tag(
			style=pango.STYLE_ITALIC, weight=pango.WEIGHT_BOLD, editable=False)
		
		# title segment
		segment = self.newSegment()
		segment.text = "GNOME Command Interface 0.1\n"
		segment.apply_tag(self.buffer.create_tag(
			justification=gtk.JUSTIFY_CENTER,
			weight=pango.WEIGHT_LIGHT,
			editable=False
		))
		
		# runtime context
		self._context = schema.Context({'ui':UISchema(self)})

		# command line editor
		self._cli = GrammaticalTextSegment(self.newSegment(),
			Runtime.Parser.RuntimeParser(self._context))
		self._cli.segment.connect("key-press-event", self.cli_key_press_event)
		
		# command line history
		self.history = [""]
		self.historyPos = -1
		
		# should we follow the bottom of the screen on output?
		self.follow = True
		
		def scrolled(widget, adjustment):
			""" Updates self.follow according to wether or not the user has
				scrolled to the bottom. """
			self.follow = (adjustment.value == adjustment.upper - adjustment.page_size)
		self.connect("scrolled", scrolled)
		
		def changed(widget):
			""" If the contents change and follow is on, follow the screen bottom """
			if self.follow: self.scroll_to_mark(self._cli.segment._end, 0)
		self.connect("changed", changed)

	@takes("GTKTerminal", Segment, gtk.gdk.Event)
	def cli_key_press_event(self, segment, event):
		""" Keypress listener for evaluation on Return """
		# only continue if we are unshifted-return
		if gtk.gdk.keyval_name(event.keyval) == "Return" and not \
		   event.state & gtk.gdk.SHIFT_MASK:
		
			segment.stop_emission("key-press-event")

			# grammatical parse, so we have something to execute
			if self._cli.status is not Status.Reject and self._cli.value:
				assert callable(self._cli.value), \
					"Parsed value must be callable"
			
				cseg = self.newSegment(len(self)-1)
				cseg.text = self._cli.segment.text
				cseg.apply_tag(self._commandTag)
				cstatus = GTKStatusIndicator()
				cseg.setBorderWidget(cstatus)
				
				# TODO: display for all returned values, not just last line
				value = self._cli.value()[-1]
				Viewer = Runtime.Viewers.viewer("gtk", value)

				# use Viewer to display value if available
				if value is not None:
					vseg = self.newSegment(len(self)-1)
					if Viewer:	Viewer((vseg, cstatus), value)
					else:		vseg.text = str(value)	# fallback
				
				# save entry in history
				self.history[-1] = self._cli.segment.text
				if len(self.history)>1 and self.history[-1] == self.history[-2]:
					self.history[-1] = ""		# remove duplicates
				else:
					self.history.append("")
				self.historyPos = -1
				
				# clear command line
				self._cli.segment.text = ""
		
		elif gtk.gdk.keyval_name(event.keyval) == 'Down':
			# only intercept key if we are already at the bottom of the input
			if self._cli.segment.cursor.get_line()-self._cli.segment.end.get_line():
				return
		
			# cycle down through history
			segment.stop_emission("key-press-event")
			
			if self.historyPos < 0:
				return
			
			self.historyPos += 1
			if self.historyPos >= len(self.history):
				self.historyPos = -1
			
			self._cli.segment.text = self.history[self.historyPos]
			
			# cursor to end of first line
			textiter = self._cli.segment.start
			textiter.forward_to_line_end()
			self._cli.segment.cursor = textiter
			
		elif gtk.gdk.keyval_name(event.keyval) == 'Up':
			# only intercept key if we are already at the top of the input
			if self._cli.segment.cursor.get_line()-self._cli.segment.start.get_line():
				return
		
			# cycle up through history
			segment.stop_emission("key-press-event")
			
			if self.historyPos == 0:
				return
			elif self.historyPos == -1:
				self.historyPos = len(self.history)-1
			
			self.historyPos -= 1
			if self.historyPos < 0:
				self.historyPos = 0
			
			self._cli.segment.text = self.history[self.historyPos]
			
			# cursor to end of last line
			self._cli.segment.cursor = self._cli.segment.end
	
	##
	## callback methods
	##
	## these methods may be called by other schemas to interact with the terminal
	##
	
	def closeTerminal(self):
		"""	Removes this terminal from the window. """
		self._window.closeTerminal(self)
	
	def setTerminalTitle(self, title):
		""" The title displayed for this terminal. """
		self._window.setTerminalTitle(self, title)
	
	def newTerminalTab(self):
		""" Creates a new tab in the current window. """
		return self._window.newTerminal()
	
	def newTerminalWindow(self):
		""" Creates a new terminal window. """
		return GTKWindow()

class GTKGrammarWindow(gtk.Window):
	""" Fun with OPEP grammars. """
	def __init__(self):
		gtk.Window.__init__(self)
		self.set_title("OPEP Grammar Window")
		
		grammarSTV = SegmentedTextView()
		grammarEdit = GrammaticalTextSegment(grammarSTV.newSegment(),
			EBNFParser, Runtime.Parser.RuntimeEBNFContext())
		subjectSTV = SegmentedTextView()
		subjectEdit = GrammaticalTextSegment(subjectSTV.newSegment(), None)

		def change_grammar(a):
			if grammarEdit.parsed.ast.status != Status.Reject:
				subjectEdit.parser = grammarEdit.parsed.value
			else:
				subjectEdit.parser = None
			subjectEdit.parse()
		
		grammarEdit.segment.connect("text-changed", change_grammar)
		import schema
		try:
			grammarEdit.segment.text = schema.app.AppSchema.AppRegistry['nautilus'].grammar
		except:
			grammarEdit.segment.text = Runtime.Parser.RuntimeGrammar.strip()
		
		grammarScrolled = gtk.ScrolledWindow()
		grammarScrolled.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		grammarScrolled.add(grammarSTV)
		grammarFrame = gtk.Frame("Grammar")
		grammarFrame.add(grammarScrolled)
		grammarScrolled.set_size_request(540,240)
		
		subjectScrolled = gtk.ScrolledWindow()
		subjectScrolled.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		subjectScrolled.add(subjectSTV)
		subjectFrame = gtk.Frame("Subject")
		subjectFrame.add(subjectScrolled)
		
		box = gtk.VBox()
		box.add(grammarFrame)
		box.add(subjectFrame)
		
		self.add(box)
		self.show_all()

class GTKWindow(gtk.Window):
	""" A GTK Window that owns a tabbed set of GTKTerminals. """

	## static list of open windows 
	windows = []

	def __init__(self):
		""" Initializes the window with a single default terminal. """
		gtk.Window.__init__(self)
		self.set_title("GCI")
		
		GTKWindow.windows.append(self)

		##
		## connections
		##
		
		self.connect("delete-event", GTKWindow.closeWindow)
		
		##
		## properties
		##
		
		# Terminals added to the window
		self.terminals = []
		
		# Widgets in the notebook holding those terminals
		self.widgets = []
		
		# Tooltips
		self.tooltips = gtk.Tooltips()
		self.tooltips.enable()

		self.notebook = gtk.Notebook()
		self.add(self.notebook)
		self.newTerminal()
		
		self.tooltips.set_tip(self.notebook, "HIIII")
		
		self.show_all()

	def closeWindow(self, args = {}):
		"""
		Closes this window, freeing all terminals, and exiting
		the app if no windows remain.
		"""
		self.hide()
		self.destroy()
		
		GTKWindow.windows.remove(self)
		if len(GTKWindow.windows) == 0:
			sys.exit()

	def newTerminal(self):
		""" Create a terminal as a tab in the window. """
		terminal = GTKTerminal(self)
		
		termbox = gtk.ScrolledWindow()
		termbox.add(terminal)
		termbox.set_policy(gtk.POLICY_NEVER, gtk.POLICY_ALWAYS)
		termbox.set_size_request(680,400)

#		terminal = VTETerminal(self, status)
#		textview = GTKCommandTextView(status, terminal)
#		terminal.registerTextView(textview)
#	
#		editbox = gtk.HBox(spacing=2)
#		editbox.pack_start(status, expand=False)
#		editbox.pack_end(textview, expand=True)
#	
#		scrollbar = gtk.VScrollbar()
#		scrollbar.set_adjustment(terminal.get_adjustment())
#		termbox = gtk.HBox()
#		termbox.pack_start(terminal, expand=True)
#		termbox.pack_start(scrollbar, expand=False)
#	
#		box = gtk.VBox()
#		box.pack_start(termbox, expand=True)
#		box.pack_end(editbox, expand=False)
#	
#		textview.grab_focus()
		
		self.terminals.append(terminal)
		self.widgets.append(termbox)
		
		if len(self.terminals) == 1:
			self.notebook.set_show_tabs(False)
		else:
			self.notebook.set_show_tabs(True)
		
		termbox.show_all()
		self.notebook.append_page(termbox)
		
		self.setTerminalTitle(terminal, str(len(self.terminals)))
		#  terminal.namespace['env.cwd'])

	def closeTerminal(self, terminal):
		"""
		Closes the given terminal.
		"""
		assert terminal in self.terminals
			
		index = self.terminals.index(terminal)
		self.terminals.pop(index)
		self.widgets.pop(index)
		
		if len(self.terminals) == 0:
			self.closeWindow()
		else:
			self.notebook.remove_page(index)
			if len(self.terminals) == 1:
				self.notebook.set_show_tabs(False)
	
	def setTerminalTitle(self, terminal, title):
		"""
		Sets the title for the given terminal.
		"""
		assert terminal in self.terminals
		assert type(title) == str
		
		widget = self.widgets[self.terminals.index(terminal)]
		
		button = gtk.Button()
		button.add(gtk.image_new_from_stock(
				gtk.STOCK_CLOSE,
				gtk.ICON_SIZE_MENU))
		button.set_relief(gtk.RELIEF_NONE)
		button.set_focus_on_click(False)
		button.set_size_request(20,18)
		button.connect("pressed", lambda button: self.closeTerminal(terminal))
		
		hbox = gtk.HBox(False, 0)
		hbox.pack_start(gtk.Label(title), True, True, 0)
		hbox.pack_end(button, False, False, 0)
		hbox.show_all()
		
		ebox = gtk.EventBox()
		ebox.add(hbox)
		
		self.tooltips.set_tip(ebox, title)
		
		self.notebook.set_tab_label_packing(widget,
			False, True, gtk.PACK_START)
		self.notebook.set_tab_label(widget, ebox)

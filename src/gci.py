# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
gci - GNOME Command Interface

(c) Daniel Ramage 2005
"""

VERBOSE = __debug__

import time

import ui
import schema
import callback

##
## main loop
##

def main():
	"""
	Main loop for the shell.  Polls all modules that need polling.  Main loop
	runs at most 20 times per second.
	"""
	while (1):
		now = time.time()
		end = now + .05
		
		ui.poll()
		callback.poll()
		
		now = time.time()
		if end > now:
			time.sleep(end-now)

if __name__ == '__main__':
	ui.Window()
	main()

# This file is part of GCI.
#
# GCI is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# GCI is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GCI; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
"""
GCIAppService for talking to nautilus.

This service currently provides no real integration with nautilus beyond what
can already be done with shell commands.  It does serve as an example of how
to write a GCIAppService.

(c) Daniel Ramage 2005
"""

import os
import dbus
import dbus.service
if getattr(dbus, 'version', (0,0,0)) >= (0,41,0): import dbus.glib

grammar = """
@nautilus := @show | @browse | @plus

@show := [cmd]show [arg]@sh.directory+
@browse := [cmd]browse [arg]@sh.directory+
@plus := [arg]@gci.value.int [cmd]plus [arg]@gci.value.int
"""

def system(command, *args):
	""" Forks the system command with the given arguments. """
	argv = [command]
	argv.extend(args)
	os.spawnvp(os.P_NOWAIT, command, argv)

dbusSessionBus = dbus.SessionBus()
dbusExportedBus = dbus.service.BusName('org.gnome.nautilus.GCIAppService', bus=dbusSessionBus)
dbusExportedIFace = 'org.gnome.GCI.AppService'

class GCIAppService(dbus.service.Object):
	def __init__(self):
		dbus.service.Object.__init__(self, dbusExportedBus, "/GCIAppService")
	
	@dbus.service.method(dbusExportedIFace)
	def GetGrammar(self):
		"""	Called by GCI to request the grammar this service. """
		print "GCIAppSerivce.GetGrammar"
		return grammar
	
	@dbus.service.method(dbusExportedIFace)
	def Invoke(self, command, args):
		""" Called by GCI to invoke a command marked with [cmd] on args
			as marked with [arg]. """
		print "GCIAppService.Invoke %s on %s" % (command, args)
		return getattr(self,command)(args)
	
	def show(self, args):
		""" Shows the given directories in nautilus. """
		for directory in args:
			system("nautilus", directory)
		return "nautilus: ok"
	
	def browse(self, args):
		"""	Browses the given directories in nautilus. """
		for directory in args:
			system("nautilus", "--browser", directory)
		return "nautilus: ok"
	
	def plus(self, args):
		""" Adds two numbers ... for no apparent reason. """
		return args[0]+args[1]

app = GCIAppService()

print __doc__
print "org.gnome.nautilus.GCIAppService ready"

import gtk
gtk.main()
